# FGTraceability Attachments #

This Repository contains all the attachements and aditional resources mentioned in my thesis work:

"Recovering fine grained traceability links between software mandatory constraints and source code"

### Author information ###

* Sebastian Alejandro Velasco Dimate
* savelascod@unal.edu.co
* Universidad Nacional de Colombia

### How this repo is organized? ###

* FGTHResults: Contains the execution results for all the four systems analysed by FGTHunter tool: iTrust TAPAS OSCAR OPENMRS
* HCSystems: Contains the extracted high level requirements, source code mappings (GOLDSET) and input files for FGTHunter tool: iTrust TAPAS OSCAR OPENMRS
* HIPAA: Contains the taxonomy of HIPAA statutes and mappings patterns that were defined prior to the FGTHunter tool implementation.
* FGTHunter: Contains the fgthunter.jar tool with installation instructions and example parameters files.  