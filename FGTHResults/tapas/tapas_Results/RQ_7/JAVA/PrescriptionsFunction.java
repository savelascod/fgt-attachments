currentSan=cloneSan((SubstanceAdministration) substanceAdministrations[index], false)section.addAct(currentSan)currentSan=(SubstanceAdministration)pendingSubstanceAdministrations[index]currentSan=cloneSan((SubstanceAdministration) favourites[index], false)section.addAct(currentSan)panel.setNameTextField(currentSan.getConsumable().getManufacturedProduct().getManufacturedLabeledDrug().getDrugProduct().getDisplayName())panel.setAmtLowTextField(currentSan.getAmount_low().toString())panel.setAmtHighTextField(currentSan.getAmount_high().toString())panel.setComments(currentSan.getText().getText())if (currentSan.getCode3() != null) {                panel.setUnit(currentSan.getCode3().getCode());            } else {                panel.setUnitNull();            }if (currentSan.getCode2() != null) {                panel.setRoute(currentSan.getCode2().getCode());            } else {                panel.setRouteNull();            }if (currentSan.getCode4() != null) {                panel.setFrequency(currentSan.getCode4().getCode());            } else {                panel.setFrequencyNull();            }panel.setOngoing(currentSan.getOngoing())panel.setPRN(currentSan.getPrn())if (currentSan.getQcode() != null) {                panel.setDuration(currentSan.getQcode().getValue().intValue() + "");            } else {                panel.setDurationNull();            }if (currentSan.getQcode().getUnit() != null) {                panel.setPeriod(currentSan.getQcode().getUnit().getCode());            } else {                panel.setPeriodNull();            }if (currentSan.getRepeatNumber() != null) {                panel.setRepeats(currentSan.getRepeatNumber().getValue().toString());            } else {                panel.setRepeatsNull();            }if (mode == 'p' && currentSan.getStartDate() != null) {                panel.setStartDate(currentSan.getStartDate());            }currentSan.setStartDate(panel.getStartDate())currentSan.setEndDate(panel.getEndDate())addToPending(currentSan)SubstanceAdministration[]dummy= new SubstanceAdministration[1]dummy[0]=sanSystem.arraycopy(dummy, 0, pendingTemp, 0, 1)currentSan=(SubstanceAdministration)pendingSubstanceAdministrations[index]SubstanceAdministrationnewFavourite= cloneSan(currentSan, true)private SubstanceAdministrationcloneSan(SubstanceAdministration favSan, boolean fromFavourite){        SubstanceAdministration newSan = new SubstanceAdministration();        newSan.setDced(favSan.getDced());        newSan.setPending(true);        newSan.setClassCode(favSan.getClassCode());        newSan.setMoodCode(favSan.getMoodCode());        newSan.setText(new ST());        newSan.getText().setText(favSan.getText().getText());        newSan.setAmount_low(new Float(favSan.getAmount_low()));        newSan.setAmount_high(new Float(favSan.getAmount_high()));        newSan.setCode3(favSan.getCode3());        newSan.setCode2(favSan.getCode2());        newSan.setCode4(favSan.getCode4());        newSan.setOngoing((fromFavourite || favSan.getOngoing() == null) ? false : favSan.getOngoing());        newSan.setPrn((fromFavourite || favSan.getPrn() == null) ? false : favSan.getPrn());        newSan.setQcode(new QCE());        newSan.getQcode().setValue(favSan.getQcode().getValue());        newSan.getQcode().setUnit(favSan.getQcode().getUnit());        newSan.setRepeatNumber(new IVL_INT());        newSan.getRepeatNumber().setValue(favSan.getRepeatNumber().getValue());        newSan.setConsumable(new Consumable());        newSan.getConsumable().setManufacturedProduct(new ManufacturedProduct());        newSan.getConsumable().getManufacturedProduct().setManufacturedLabeledDrug(new LabeledDrug());        newSan.getConsumable().getManufacturedProduct().getManufacturedLabeledDrug().setDrugProduct(favSan.getConsumable().getManufacturedProduct().getManufacturedLabeledDrug().getDrugProduct());        return newSan ;    }currentSan=(SubstanceAdministration)pendingSubstanceAdministrations[panel.getSelectedPendingTableRow()]currentSan.getConsumable().getManufacturedProduct().getManufacturedLabeledDrug().setDrugProduct(drugProduct)currentSan=add(drugProduct)section.addAct(currentSan)addToPending(currentSan)
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasSurgical.java">
{public String getStartDate() {	return this.startDate;}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/TEL.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/Consumable.java">
{public ManufacturedProduct getManufacturedProduct() {        return manufacturedProduct;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/QCE.java">
{public CE getUnit() {        return unit;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerting/AlertingFunctionPanel.java">
{protected java.util.Date getStartDate(){        return startDateChooser.getDate();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/immunizations/ImmunizationsFunctionPanel.java">
{protected void setRouteNull(){        routeComboBox.setSelectedIndex(0);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerts/AlertsFunctionPanel.java">
{protected java.util.Date getStartDate(){        return startDateChooser.getDate();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public ST getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Material.java">
{public String getManufacturedLabeledDrug() {        return manufacturedLabeledDrug;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public IVL_INT getRepeatNumber() {        return repeatNumber;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasProblem.java">
{public String getStartDate() {        return startDate;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ManufacturedProduct.java">
{public org.opentapas.commons.rim.ems.LabeledDrug getManufacturedLabeledDrug() {        return manufacturedLabeledDrug;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/SubstanceAdministration.java">
{public Consumable getConsumable() {        return consumable;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/prescriptions/PrescriptionsFunctionPanel.java">
{protected int getSelectedPendingTableRow(){        return pendingTable.getSelectedRow();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasMedicalHX.java">
{public String getStartDate() {      return startDate;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.ST getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasMedication.java">
{public String getStartDate() {	return startDate;}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasImmunization.java">
{public String getStartDate(){      return this.startDate;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/ED.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/ST.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasSocial.java">
{public String getStartDate() {      return startDate;   }}
