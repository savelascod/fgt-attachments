Setsections= cda.getActs()for (Object obj : sections) {         Section section = (Section) obj;         ArrayList acts = new ArrayList(section.getActs());         String secCode = section.getCode().getCode();         ArrayList actBeans = new ArrayList();         if (secCode.equalsIgnoreCase(ToolKit.PROBLEMHX)) {            this.problems = actBeans;            for (Object a : acts) {               TapasProblem prob = new TapasProblem( (Observation) a);               actBeans.add(prob);            }            this.setProblems(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.MEDICALHX)) {            this.medicalHx = actBeans;            for (Object a : acts) {               TapasMedicalHX prob = new TapasMedicalHX( (Observation) a);               actBeans.add(prob);            }            this.setMedicalHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.PROCHX)) {            this.procedures = actBeans;            for (Object a : acts) {               TapasSurgical surg = new TapasSurgical( (Procedure) a);               actBeans.add(surg);            }            this.setProcedures(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.                                           PRESCRIPTIONS)) {            this.ongoingMeds = actBeans;            /**            * @todo Filter out past drugs             */            for (Object a : acts) {               SubstanceAdministration san = (SubstanceAdministration) a;               if (!san.getPending()) {                  TapasMedication surg = new TapasMedication(san);                  actBeans.add(surg);               }            }            this.setOngoingMeds(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.ALLERGYHX)) {            this.allergies = actBeans;            for (Object a : acts) {               TapasAllergyHX surg = new TapasAllergyHX( (Observation) a);               actBeans.add(surg);            }            this.setAllergies(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.FAMILYHX)) {            this.familyHx = actBeans;            for (Object a : acts) {               TapasFamilyHx surg = new TapasFamilyHx( (Observation) a);               actBeans.add(surg);            }            this.setFamilyHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.IMMUNIZATIONSHX)) {            this.immunizations = actBeans;            for (Object a : acts) {               TapasImmunization surg = new TapasImmunization( (Procedure) a);               actBeans.add(surg);            }            this.setImmunizations(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.RISKS)) {            this.risks = actBeans;            for (Object a : acts) {               TapasSocial surg = new TapasSocial( (Observation) a);               actBeans.add(surg);            }            this.setRisks(actBeans);         }         /**            * @todo Complete Alerts section of Patient Bean          */         else if (secCode.equalsIgnoreCase(ToolKit.ALERTS)) {            this.alerts = actBeans;            for (Object a : acts) {               TapasAlert alert = new TapasAlert( (Observation) a, this.patientID);               actBeans.add(alert);            }            this.setAlerts(actBeans);         }      }this.specialistFirstName=specialistFirstNamepublic voidsetReferallDate(Date referallDate){      this.referallDate = referallDate;   }public StringextractProviderName(Author provider){      String ret = "";      if (provider != null) {         String firstName = "";         String lastName = "";         try {            if (provider.getAssignedAuthor() != null &&                provider.getAssignedAuthor().getAssignedPerson() != null &&                provider.getAssignedAuthor().getAssignedPerson().getName() != null) {               firstName = provider.getAssignedAuthor().getAssignedPerson().getName().               getPreferredName();               lastName = provider.getAssignedAuthor().getAssignedPerson().getName().                  getFamilyName();            }            ret = firstName + " " + lastName;         }         catch (NullPointerException e) {            e.printStackTrace();         }      }      return ret;   }public voidsetProblems(List problems){      this.problems = problems;   }public voidsetMedicalHx(List medicalHx){      this.medicalHx = medicalHx;   }public voidsetProcedures(List procedures){      this.procedures = procedures;   }for (Object obj : problems) {            TapasProblem activeProblem = (TapasProblem) obj;            if (activeProblem != null){               stringBuffer.append(activeProblem.getTitle() + "\n");               stringBuffer.append("- " + activeProblem.getStartDate() + "\n\n");            }            else {               stringBuffer.append("activeProblem object = null\n\n");            }         }for (Object obj : medicalHx) {            TapasMedicalHX medicalHistory = (TapasMedicalHX) obj;            if (medicalHistory != null){               stringBuffer.append(medicalHistory.getTitle() + "\n");               stringBuffer.append("- " +                                   medicalHistory.getStartDate() + " - " +                                   medicalHistory.getStartDate() + "\n\n");            }            else {               stringBuffer.append("medicalHistory object = null\n\n");            }         }for (Object obj : procedures) {            TapasSurgical surgicalHistory = (TapasSurgical) obj;            if (surgicalHistory != null){               stringBuffer.append(surgicalHistory.getTitle() + "\n");               stringBuffer.append("- " + surgicalHistory.getStartDate() + "\n\n");            }            else{               stringBuffer.append("surgicalHistory object = null\n\n");            }         }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/commandImpl/GetDrugProductCommand.java">
{public String getCode() {    return code;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/CE.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/PN.java">
{public String getFamilyName() {        return familyName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasFunction.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Person.java">
{public String getFamilyName() {        PN pn = this.getName();        String ret = "";        if (pn != null) {            ret = pn.getFamilyName();        }        return ret;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/Insurer.java">
{public String getName() {    return name;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/Author.java">
{public AssignedAuthor getAssignedAuthor() {    return assignedAuthor;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/AssignedAuthor.java">
{public Person getAssignedPerson() {        return assignedPerson;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Code.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasUser.java">
{public String getName() {        return userName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunctionPanel.java">
{protected String getFamilyName() {      return familyNameTextField.getText();  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/SubstanceAdministration.java">
{public Boolean getPending() {        return pending;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.PN getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasRole.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasGroup.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Drug.java">
{public String getCode() {        return code;    }}
