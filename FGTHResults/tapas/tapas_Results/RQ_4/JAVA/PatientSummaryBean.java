Patientpatient= role.getPatient()patientID=patient.getPkId()patientFirstName=patient.getGivenName()patientLastName=patient.getFamilyName()this.patientPrefix=patient.getPrefix()DatebirthTime= patient.getBirthTime()this.patientDOB=patient.getBirthTime().toString()this.patientSex=patient.getSex()STpatientSt= patient.getText()if (patientSt != null) {         this.note = patientSt.getText();      }TapasAlertalert= new TapasAlert( (Observation) a, this.patientID)patientSummary=createPDASummary()/* added by DD */public voidsetPatientAge(String patientAge){      this.patientAge = patientAge;   }public voidsetPatientDOB(String patientDOB){      this.patientDOB = patientDOB;   }public voidsetPatientHIN(String patientHIN){      this.patientHIN = patientHIN;   }public voidsetPatientProviderName(String patientProviderName){      this.patientProviderName = patientProviderName;   }public voidsetPatientStreet(String patientStreet){      this.patientStreet = patientStreet;   }public voidsetPatientAddress(String patientAddress){      this.patientAddress = patientAddress;   }public voidsetPatientPhone(String patientPhone){      this.patientPhone = patientPhone;   }public voidsetPatientSex(String patientSex){      this.patientSex = patientSex;   }public voidsetPatientFullName(String patientFullName){      this.patientFullName = patientFullName;   }this.modificationTime=modificationTimepublic voidsetPatientEmailAddress(String patientEmailAddress){      this.patientEmailAddress = patientEmailAddress;   }public voidsetPatientFirstName(String patientFirstName){      this.patientFirstName = patientFirstName;   }public voidsetPatientHomePhoneNumber(String patientHomePhoneNumber){      this.patientHomePhoneNumber = patientHomePhoneNumber;   }public voidsetPatientID(int patientID){      this.patientID = patientID;   }public voidsetPatientLastName(String patientLastName){      this.patientLastName = patientLastName;   }public voidsetPatientSummary(String patientSummary){      this.patientSummary = patientSummary;   }public voidsetPatientWorkPhoneNumber(String patientWorkPhoneNumber){      this.patientWorkPhoneNumber = patientWorkPhoneNumber;   }public voidsetPatientPrefix(String patientPrefix){      this.patientPrefix = patientPrefix;   }public voidsetPatientNote(String patientNote){      this.patientNote = patientNote;   }public voidsetPatientProviderMSPNo(String patientProviderMSPNo){      this.patientProviderMSPNo = patientProviderMSPNo;   }public voidsetPatientProviderStreet(String patientProviderStreet){      this.patientProviderStreet = patientProviderStreet;   }public voidsetPatientProviderAddress(String patientProviderAddress){      this.patientProviderAddress = patientProviderAddress;   }public voidsetPatientProviderWkPhone(String patientProviderWkPhone){      this.patientProviderWkPhone = patientProviderWkPhone;   }public voidsetPatientProviderFax(String patientProviderFax){      this.patientProviderFax = patientProviderFax;   }TapasMedicationcurrentMedications= (TapasMedication) objif (currentMedications != null){               stringBuffer.append(currentMedications.getDrugName() + "\n");               stringBuffer.append(                                   currentMedications.getAmount() + " " +                                   currentMedications.getType() + " " +                                   currentMedications.getRoute() + " " +                                   currentMedications.getFrequency() + " " +                                   currentMedications.isPrnStatus() + "\n");               stringBuffer.append("- " +                                   currentMedications.getStartDate() + " - " +                                   currentMedications.getEndDate() + "\n\n");            }            else {               stringBuffer.append("currentMedications object = null\n\n");            }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/TEL.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerts/AlertsFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/PatientRole.java">
{public Patient getPatient() {        return patient;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public ST getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/PatientSummaryBean.java">
{private String createPDASummary() {      StringBuffer stringBuffer = new StringBuffer();      if (problems != null) {         stringBuffer.append("Active Problems:\n\n");         for (Object obj : problems) {            TapasProblem activeProblem = (TapasProblem) obj;            if (activeProblem != null){               stringBuffer.append(activeProblem.getTitle() + "\n");               stringBuffer.append("- " + activeProblem.getStartDate() + "\n\n");            }            else {               stringBuffer.append("activeProblem object = null\n\n");            }         } // end-for      } // end-if      if (medicalHx != null) {         stringBuffer.append("Medical History:\n\n");         for (Object obj : medicalHx) {            TapasMedicalHX medicalHistory = (TapasMedicalHX) obj;            if (medicalHistory != null){               stringBuffer.append(medicalHistory.getTitle() + "\n");               stringBuffer.append("- " +                                   medicalHistory.getStartDate() + " - " +                                   medicalHistory.getStartDate() + "\n\n");            }            else {               stringBuffer.append("medicalHistory object = null\n\n");            }         } // end-for      } // end-if      if (procedures != null) {         stringBuffer.append("Surgical History:\n\n");         for (Object obj : procedures) {            TapasSurgical surgicalHistory = (TapasSurgical) obj;            if (surgicalHistory != null){               stringBuffer.append(surgicalHistory.getTitle() + "\n");               stringBuffer.append("- " + surgicalHistory.getStartDate() + "\n\n");            }            else{               stringBuffer.append("surgicalHistory object = null\n\n");            }         } // end-for      } // end-if      if (ongoingMeds != null) {         stringBuffer.append("Current Medications:\n\n");         for (Object obj : ongoingMeds) {            TapasMedication currentMedications = (TapasMedication) obj;            if (currentMedications != null){               stringBuffer.append(currentMedications.getDrugName() + "\n");               stringBuffer.append(                                   currentMedications.getAmount() + " " +                                   currentMedications.getType() + " " +                                   currentMedications.getRoute() + " " +                                   currentMedications.getFrequency() + " " +                                   currentMedications.isPrnStatus() + "\n");               stringBuffer.append("- " +                                   currentMedications.getStartDate() + " - " +                                   currentMedications.getEndDate() + "\n\n");            }            else {               stringBuffer.append("currentMedications object = null\n\n");            }         } // end-for      } // end-if      if (allergies != null) {         stringBuffer.append("Allergies:\n\n");         for (Object obj : allergies) {            TapasAllergyHX allergy = (TapasAllergyHX) obj;            if (allergy != null){               stringBuffer.append(allergy.getSubstance() + "\n\n");            }            else {               stringBuffer.append("allergy object = null\n\n");            }         } // end-for      } // end-if      if (familyHx != null) {         stringBuffer.append("Family History:\n\n");         for (Object obj : familyHx) {            TapasFamilyHx familyHistory = (TapasFamilyHx) obj;            if (familyHistory != null){               stringBuffer.append(familyHistory.getTitle() + "\n");               stringBuffer.append("- " + familyHistory.getFamilyMember() + "\n\n");            }            else {               stringBuffer.append("familyHistory object = null\n\n");            }         } // end-for      } // end-if      if (risks != null) {         stringBuffer.append("Social History:\n\n");         for (Object obj : risks) {            TapasSocial socialHistory = (TapasSocial) obj;            if (socialHistory != null){               stringBuffer.append(socialHistory.getTitle() + "\n");               stringBuffer.append("- " + socialHistory.getStartDate() + " - ");               stringBuffer.append("- " + socialHistory.getEndDate() + "\n\n");            }            else {               stringBuffer.append("socialHistory object = null\n\n");            }         } // end-for      } // end-if      if (immunizations != null) {         stringBuffer.append("Immunizations:\n\n");         for (Object obj : immunizations) {            TapasImmunization immunization = (TapasImmunization) obj;            if (immunization != null){               stringBuffer.append(immunization.getTitle() + "\n");               stringBuffer.append("- " + immunization.getStartDate() + "\n\n");            }            else {               stringBuffer.append("immunization object = null\n\n");            }         } // end-for      } // end-if      return stringBuffer.toString();   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasProblem.java">
{public String toString() {        return reason + " - " + relationship + ",age " + this.ageOfOnSet.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasUser.java">
{public String toString() {        return String.valueOf(this.getPkId());    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/user/UserFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunctionPanel.java">
{public String toString() {              return item;          }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasFamilyHx.java">
{public String toString() {    return reason + " - " + relationship + ",age " + this.ageOfOnSet.toString();  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.ST getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/ED.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/allergy/AllergyFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Any.java">
{public Integer getPkId(){        return this.pkId == null ? Integer.MAX_VALUE : this.pkId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/problemhx/ProblemHxFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/risks/RisksFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasSurgical.java">
{public String toString() {    return date + " " + procedureName;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referring/ReferringFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/familyhx/FamilyHxFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/PN.java">
{public String toString(){      return "TAPAS SYSTEM";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referrals/ReferralsFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Person.java">
{public String getSex() {        CE code = this.administrativeGenderCode;        String ret = "";        if (code != null) {            ret = code.getDisplayName();        }        return ret;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/DateTime.java">
{public String toString(){      return getStringRepresentation();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/Insurer.java">
{public String toString(){    return this.name + " - " + this.number;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/immunizations/ImmunizationsFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/specialist/SpecialistFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public ST getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/ClinicalDocument.java">
{public Patient getPatient() {    return this.getRecordTarget().getPatientRole().getPatient();  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/TapearPanel.java">
{public Patient getPatient(){        return clinicalDocument.getRecordTarget().getPatientRole().getPatient();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasMedicalHX.java">
{public String toString() {      return startDate +      " - " + endDate + " " +      title;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/prescriptions/PrescriptionsFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/EffectiveTime.java">
{public String toString(){    return this.value;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasAllergyHX.java">
{public String toString() {      return title;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasMedication.java">
{public String toString() {    return this.drugName + " " + this.amount + " " + this.route + " " +        this.frequency;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/procedurehx/ProcedureHxFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/ST.java">
{public String getText() {        return text;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasSocial.java">
{public String toString() {      /**      * @todo Fix formatting       */      return startDate +      " - " + endDate + " " +      title;         }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/medicalhx/MedicalHxFunctionPanel.java">
{public String toString() { return item; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/setting/SettingFunctionPanel.java">
{public String toString() { return item; }}
