public voidpopulate(TapasUser currentUser){        if (!isActive()){            this.currentUser = currentUser;            observations.clear();            acts = currentUser.getManagedActs().toArray();                        for (Object tgo : getGroupArrayForUser(currentUser)) {                TapasGroup tg = (TapasGroup) tgo ;                Object[] dummy1 = tg.getManagedActs().toArray() ;                Object[] dummy2 = new Object[acts.length + dummy1.length] ;                System.arraycopy(acts, 0, dummy2, 0, acts.length);                System.arraycopy(dummy1, 0, dummy2, acts.length, dummy1.length);                acts = dummy2 ;            }                        // only ACTS that have code OBS and are Alerts with active timespan            List activeActs = new ArrayList() ;            for (Object obj : acts){                Act act = (Act) obj ;                if (act.getClassCode().equalsIgnoreCase("OBS") &&                        ((act.getStartDate() == null ? true : act.getStartDate().before(new java.util.Date())) &&                        (act.getEndDate() == null ? true : act.getEndDate().after(new java.util.Date())))                        ) {                    activeActs.add(obj);                }            }                        acts = activeActs.toArray();                        if (panel.getFindTextField().length() != 0){                List filteredActs = new ArrayList();                for (Object obj : acts){                    Act act = (Act) obj ;                    if ((panel.getFilterComboBox() == 0 && lookupPatientName((Observation)act).toUpperCase().indexOf(panel.getFindTextField().toUpperCase()) != -1) ||                            (panel.getFilterComboBox() == 1 && act.getTitle().getText().toUpperCase().indexOf(panel.getFindTextField().toUpperCase()) != -1) ||                            (panel.getFilterComboBox() == 2 && (act.getManagingTapasUser() != null ? act.getManagingTapasUser().getName() :                                (act.getManagingTapasGroup() != null ? act.getManagingTapasGroup().getName() : "")).toUpperCase().indexOf(panel.getFindTextField().toUpperCase()) != -1)                                ){                        filteredActs.add(obj);                    }                }                acts = filteredActs.toArray();            }                        Arrays.sort(acts, actComparator);                        for(Object obj : acts){                observations.add(obj);            }                        list();        }    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referrals/ReferralsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/SwingWorker.java">
{synchronized void clear() { thread = null; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/allergies/AllergiesFocusPanel.java">
{public void clear() {        ((DefaultTableModel) table.getModel()).setRowCount(0); //new    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/PatientCentricFunction.java">
{boolean isActive();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/risks/RisksFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/problemhx/ProblemHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/UserCentricFunction.java">
{boolean isActive();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/user/UserFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerter/AlerterSidePanel.java">
{public void clear() {        ((DefaultTableModel) table.getModel()).setRowCount(0);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouping/GroupingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/patientflags/PatientFlagsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/immunizations/ImmunizationsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/summary/SummaryFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouper/GrouperFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/roler/RolerFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/user/UserFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) addressTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) telecomTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) groupsTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) rolesTable.getModel()).setRowCount(0); //new        userNameTextField.setText("");        passwordField.setText("");        passwordAgainField.setText("");        prefixComboBox.setSelectedIndex(0);        givenNamesTextField.setText("");        familyNameTextField.setText("");        suffixTextField.setText("");        collegeIdTextField.setText("");        billingTextField.setText("");        payeeTextField.setText("");        givenNames = "";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasUser.java">
{public Set getManagedActs() {        return managedActs;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referring/ReferringFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunctionPanel.java">
{protected void clear() {      ((DefaultTableModel) addressTable.getModel()).setRowCount(0); //new      ((DefaultTableModel) healthNumberTable.getModel()).setRowCount(0); //new      ((DefaultTableModel) telecomTable.getModel()).setRowCount(0); //new      ((DefaultTableModel) providerTable.getModel()).setRowCount(0); //new      prefixComboBox.setSelectedIndex(0);      givenNamesTextField.setText("");      preferredNameTextField.setText("");      familyNameTextField.setText("");      suffixTextField.setText("");      genderComboBox.setSelectedIndex(0);      birthDateChooser.setDate(Controller.getInstance().getNow());      languageComboBox.setSelectedIndex(0);      confidentialityComboBox.setSelectedIndex(0);      sendToPDACheckBox.setSelected(false);      statusComboBox.setSelectedIndex(0);  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerts/AlertsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/DummyFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/roler/RolerFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) availableTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) currentTable.getModel()).setRowCount(0); //new        setNameTextField("") ;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasGroup.java">
{public Set getManagedActs() {        return managedActs;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/setting/SettingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/specialist/SpecialistFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerting/AlertingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/messaging/MessagingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/messages/MessagesSideFunction.java">
{public void clear() {        panel.clear();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouper/GrouperFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) availableTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) currentTable.getModel()).setRowCount(0); //new        setNameTextField("") ;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/specialist/SpecialistFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) addressTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) telecomTable.getModel()).setRowCount(0); //new        prefixComboBox.setSelectedIndex(0);        statusComboBox.setSelectedIndex(0);        specialityComboBox.setSelectedIndex(0);        givenNamesTextField.setText("");        preferredNameTextField.setText("");        familyNameTextField.setText("");        suffixTextField.setText("");        givenNames = "";        extraTextArea.setText("");    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/prescriptions/PrescriptionsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/allergy/AllergyFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouping/GroupingFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) availableTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) currentTable.getModel()).setRowCount(0); //new        setGroupNameLabel("Group Name:") ;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/familyhx/FamilyHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/messages/MessagesSidePanel.java">
{public void clear() {        ((DefaultTableModel) table.getModel()).setRowCount(0); //new        label.setText("No new messages");    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/procedurehx/ProcedureHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/medicalhx/MedicalHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/setting/SettingFunctionPanel.java">
{protected void clear(){        ((DefaultTableModel) addressTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) telecomTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) groupsTable.getModel()).setRowCount(0); //new        ((DefaultTableModel) rolesTable.getModel()).setRowCount(0); //new        userNameTextField.setText("");        passwordField.setText("");        prefixComboBox.setSelectedIndex(0);        givenNamesTextField.setText("");        familyNameTextField.setText("");        suffixTextField.setText("");        collegeIdTextField.setText("");        billingTextField.setText("");        payeeTextField.setText("");    }}
