tapasUsers=Controller.getInstance().getTapasUsers().toArray()Arrays.sort(tapasUsers, userComparator)panel=newMessagingFunctionPanel(this, tapasUsers)public voidpopulate(TapasUser currentUser){        if (!isActive()){            this.cu = currentUser;            //cu = Controller.getInstance().getCurrentUser();            receivedUserMessages = cu.getReceivedUserMessages().toArray();                        // all this is to filter out the archived messages in a neat way            Object[] dummyReceived = new Object[receivedUserMessages.length];            j = 0 ;            k = 0 ;            for(Object obj : receivedUserMessages){                if (!((TapasUserMessage)obj).getArchived()){                    System.arraycopy(receivedUserMessages, j, dummyReceived, k, 1);                    k++ ;                }                j++ ;            }                        receivedUserMessages = new Object[k];            for(j = 0 ; j < k ; j++){                System.arraycopy(dummyReceived, j, receivedUserMessages, j, 1);            }            Arrays.sort(receivedUserMessages, userMessageComparator);                        sentMessages = cu.getSentMessages().toArray();            Object[] dummySent = new Object[sentMessages.length];            j = 0 ;            k = 0 ;            for(Object obj : sentMessages){                if (!((TapasMessage)obj).getArchived()){                    System.arraycopy(sentMessages, j, dummySent, k, 1);                    k++ ;                }                j++ ;            }                        sentMessages = new Object[k];            for(j = 0 ; j < k ; j++){                System.arraycopy(dummySent, j, sentMessages, j, 1);            }            Arrays.sort(sentMessages, messageComparator);            list();        }    }ListfilteredReceivedUserMessages= new ArrayList()for (Object obj : receivedUserMessages) {                    TapasUserMessage tum = (TapasUserMessage) obj ;                    if ( (panel.getFilterComboBox() == 0 && tum.getReceivingMessage().getSentByUser().getUserName().toUpperCase().indexOf(panel.getFindTextField().toUpperCase()) != -1) ||                            (panel.getFilterComboBox() == 1 && tum.getReceivingMessage().getSubject().toUpperCase().indexOf(panel.getFindTextField().toUpperCase()) != -1)                            )                        filteredReceivedUserMessages.add(tum);                }receivedUserMessages=filteredReceivedUserMessages.toArray()panel.setTableRows(receivedUserMessages.length)//newfor (Object obj : receivedUserMessages) {                tum = (TapasUserMessage) obj ;                panel.addRow(                        (ToolKit.getInstance().isSentToday(tum.getReceivingMessage().getDateSent()) ? todaysSdf.format(tum.getReceivingMessage().getDateSent()) : sdf.format(tum.getReceivingMessage().getDateSent()))                        , tum.getReceivingMessage().getSentByUser().getUserName()                        , tum.getReceivingMessage().getSubject()                        , i);                if (!tum.getRead()){                    Controller.getInstance().addMessageAlert(tum.getReceivingMessage().getSentByUser().getUserName()                    , tum.getReceivingMessage().getSubject()                    , msgAlerts);                    msgAlerts++;                }                i++;            }protected voidasterisker(boolean sendButtonEnabled, boolean previous){        Controller.getInstance().asterisker(sendButtonEnabled, previous);    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referrals/ReferralsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/PatientCentricFunction.java">
{boolean isActive();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/risks/RisksFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/problemhx/ProblemHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/UserCentricFunction.java">
{boolean isActive();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/user/UserFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouping/GroupingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/patientflags/PatientFlagsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/immunizations/ImmunizationsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/summary/SummaryFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/grouper/GrouperFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/Controller.java">
{public List getTapasUsers() {        return tapasUsersList;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/util/ToolKit.java">
{public static ToolKit getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/persistence/DocumentManager.java">
{public List getTapasUsers() {    GetTapasUsersCommand cmd = (GetTapasUsersCommand) factory.getCommand(        GetTapasUsersCommand.class);    return cmd.getTapasUsers("");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/roler/RolerFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/referring/ReferringFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerts/AlertsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/DummyFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasRole.java">
{public Set getTapasUsers() {        return tapasUsers;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasGroup.java">
{public Set getTapasUsers() {        return tapasUsers;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/remote/RemoteFacade.java">
{public static RemoteFacade getInstance() {    if (fac == null) {      fac = new RemoteFacade();    }    return fac;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/setting/SettingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/specialist/SpecialistFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/alerting/AlertingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/messaging/MessagingFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/prescriptions/PrescriptionsFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/allergy/AllergyFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/familyhx/FamilyHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/procedurehx/ProcedureHxFunction.java">
{public boolean isActive() {        return active;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/medicalhx/MedicalHxFunction.java">
{public boolean isActive() {        return active;    }}
