Setsections= cda.getActs()Setaddresses= role.getAddresses()if (addresses != null) {         for (Object elem : addresses) {            AD ad = (AD) elem;            if (ad.getIsMain()) {               this.patientStreet = ad.getType() == null ? "" :               ad.getType().getDisplayName() + " " +               ad.getStreetAddressLine();               ad.getStreetNameType();               this.patientAddress = ad.getCity() + "," +                  (ad.getCountry()!=null?ad.getCountry().getDisplayName():"") +                  " " + ad.getPostalCode();            }         }      }for (Object obj : sections) {         Section section = (Section) obj;         ArrayList acts = new ArrayList(section.getActs());         String secCode = section.getCode().getCode();         ArrayList actBeans = new ArrayList();         if (secCode.equalsIgnoreCase(ToolKit.PROBLEMHX)) {            this.problems = actBeans;            for (Object a : acts) {               TapasProblem prob = new TapasProblem( (Observation) a);               actBeans.add(prob);            }            this.setProblems(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.MEDICALHX)) {            this.medicalHx = actBeans;            for (Object a : acts) {               TapasMedicalHX prob = new TapasMedicalHX( (Observation) a);               actBeans.add(prob);            }            this.setMedicalHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.PROCHX)) {            this.procedures = actBeans;            for (Object a : acts) {               TapasSurgical surg = new TapasSurgical( (Procedure) a);               actBeans.add(surg);            }            this.setProcedures(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.                                           PRESCRIPTIONS)) {            this.ongoingMeds = actBeans;            /**            * @todo Filter out past drugs             */            for (Object a : acts) {               SubstanceAdministration san = (SubstanceAdministration) a;               if (!san.getPending()) {                  TapasMedication surg = new TapasMedication(san);                  actBeans.add(surg);               }            }            this.setOngoingMeds(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.ALLERGYHX)) {            this.allergies = actBeans;            for (Object a : acts) {               TapasAllergyHX surg = new TapasAllergyHX( (Observation) a);               actBeans.add(surg);            }            this.setAllergies(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.FAMILYHX)) {            this.familyHx = actBeans;            for (Object a : acts) {               TapasFamilyHx surg = new TapasFamilyHx( (Observation) a);               actBeans.add(surg);            }            this.setFamilyHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.IMMUNIZATIONSHX)) {            this.immunizations = actBeans;            for (Object a : acts) {               TapasImmunization surg = new TapasImmunization( (Procedure) a);               actBeans.add(surg);            }            this.setImmunizations(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.RISKS)) {            this.risks = actBeans;            for (Object a : acts) {               TapasSocial surg = new TapasSocial( (Observation) a);               actBeans.add(surg);            }            this.setRisks(actBeans);         }         /**            * @todo Complete Alerts section of Patient Bean          */         else if (secCode.equalsIgnoreCase(ToolKit.ALERTS)) {            this.alerts = actBeans;            for (Object a : acts) {               TapasAlert alert = new TapasAlert( (Observation) a, this.patientID);               actBeans.add(alert);            }            this.setAlerts(actBeans);         }      }public StringextractProviderName(Author provider){      String ret = "";      if (provider != null) {         String firstName = "";         String lastName = "";         try {            if (provider.getAssignedAuthor() != null &&                provider.getAssignedAuthor().getAssignedPerson() != null &&                provider.getAssignedAuthor().getAssignedPerson().getName() != null) {               firstName = provider.getAssignedAuthor().getAssignedPerson().getName().               getPreferredName();               lastName = provider.getAssignedAuthor().getAssignedPerson().getName().                  getFamilyName();            }            ret = firstName + " " + lastName;         }         catch (NullPointerException e) {            e.printStackTrace();         }      }      return ret;   }ADaddress= person.getAddress()this.specialistAddress=address.getHouseNumber()+" "+address.getStreetName()+","+address.getCity()+","+address.getProvince()+","+address.getCountry()public voidsetFamilyHx(List familyHx){      this.familyHx = familyHx;   }for (Object obj : familyHx) {            TapasFamilyHx familyHistory = (TapasFamilyHx) obj;            if (familyHistory != null){               stringBuffer.append(familyHistory.getTitle() + "\n");               stringBuffer.append("- " + familyHistory.getFamilyMember() + "\n\n");            }            else {               stringBuffer.append("familyHistory object = null\n\n");            }         }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/CE.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/TEL.java">
{public CE getType() {        return type;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/Author.java">
{public AssignedAuthor getAssignedAuthor() {    return assignedAuthor;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/AssignedAuthor.java">
{public Person getAssignedPerson() {        return assignedPerson;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Code.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasUser.java">
{public String getName() {        return userName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/demographics/DemographicsFunctionPanel.java">
{protected String getFamilyName() {      return familyNameTextField.getText();  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/SubstanceAdministration.java">
{public Boolean getPending() {        return pending;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.AD getAddress() {        return address;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasRole.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/AuthorClinicalDoc.java">
{public Boolean getIsMain() {        return isMain == null ? false : isMain ;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasGroup.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Drug.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/allergy/AllergyFunctionPanel.java">
{protected CE getType(){        return (CE)types[typeComboBox.getSelectedIndex()];    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/AD.java">
{public CE getCountry() {        return country;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/commandImpl/GetDrugProductCommand.java">
{public String getCode() {    return code;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/PN.java">
{public String getFamilyName() {        return familyName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasFunction.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Person.java">
{public String getFamilyName() {        PN pn = this.getName();        String ret = "";        if (pn != null) {            ret = pn.getFamilyName();        }        return ret;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/commandImpl/GetCECommand.java">
{public String getDisplayName() {    return displayName;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/Insurer.java">
{public String getName() {    return name;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Participation.java">
{public Boolean getIsMain() {    return isMain;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasAllergyHX.java">
{public String getType() {      return type;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/TapasMedication.java">
{public String getType() {	return type;}}
