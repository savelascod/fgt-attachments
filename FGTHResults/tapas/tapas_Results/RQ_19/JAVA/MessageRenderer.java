javax.swing.JTabletableObjectvaluebooleanisSelectedbooleanhasFocusintrowintcolumn{        function = (MessagingFunction) Controller.getInstance().getMainFrame().getTabAt(Controller.getInstance().getAdministrator() ? 1 : 0).getUserCentricFunctions(0);                if (function.isNotRead(row)) {            setFont(bold);            if (function.isUrgent(row)) {                setForeground(java.awt.Color.red);            } else {                setForeground(ToolKit.LETTERDARKGREY);            }                                    if (isSelected) {                setBackground(ToolKit.HIGHRED);            } else {                setBackground(java.awt.Color.white);            }        } else {            setFont(plain);                        if (function.isUrgent(row)) {                setForeground(java.awt.Color.red);            } else {                setForeground(ToolKit.LETTERDARKGREY);            }                                    if (isSelected) {                setBackground(ToolKit.EDITBLUE);            } else {                setBackground(java.awt.Color.white);            }        }                super.setValue(value);        return this;    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/Controller.java">
{public MainFrame getMainFrame() {        return mainFrame;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/util/ToolKit.java">
{public static ToolKit getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/remote/RemoteFacade.java">
{public static RemoteFacade getInstance() {    if (fac == null) {      fac = new RemoteFacade();    }    return fac;  }}
