public voidlistClinicalDocs(List clinicalDocList){        listWhat = 'd';        panel.setSearchTitleText("select Patient");        panel.setGreen();        clinicalDocs = clinicalDocList.toArray();        panel.clearSearchTable();        panel.setRowNumber(clinicalDocs.length);        String patientMiddleName = "";        String patientLastName = "";        RecordTarget rtt;        PatientRole pre;        Patient ptt;        PN patientName;        String patientFirstNameString = "";        Object[] patientFirstNames = null;        for (int i = 0; i < (clinicalDocs.length > 30 ? 30 : clinicalDocs.length); i++) {            ClinicalDocument cdt = (ClinicalDocument) clinicalDocs[i];            if (cdt.getRecordTarget() != null &&                    cdt.getRecordTarget().getPatientRole() != null &&                    cdt.getRecordTarget().getPatientRole().getPatient() != null &&                    cdt.getRecordTarget().getPatientRole().getPatient().getName() != null) {                patientFirstNameString = "";                patientMiddleName = "";                patientLastName = "";                rtt = cdt.getRecordTarget();                pre = rtt.getPatientRole();                ptt = pre.getPatient();                patientName = ptt.getName();                panel.addItem(patientName.getPreferredName() + " " + patientName.getFamilyName(), i);            }        }    }public voidlistMgrs(List users, List groups, String text, int functionIndex){        numberOfUsers = 0 ;        int numberOfGroups = 0 ;        this.functionIndex = functionIndex;        listWhat = 'm';        panel.setSearchTitleText("select Referral Manager");        panel.setBlue();        tapasUsers = users.toArray();        tapasGroups = groups.toArray();        panel.clearSearchTable();        Vector mgrs = new Vector();        for (int i = 0; i < tapasUsers.length; i++){            TapasUser user = (TapasUser)tapasUsers[i];            if (user.getName().toUpperCase().indexOf(text.toUpperCase()) != -1) {                numberOfUsers++ ;                panel.setRowNumber(numberOfUsers);                panel.addItem(user.getName(), numberOfUsers - 1);                mgrs.add(user);            }        }        for (int i = 0; i < tapasGroups.length; i++){            TapasGroup group = (TapasGroup)tapasGroups[i];            if (group.getName().toUpperCase().indexOf(text.toUpperCase()) != -1) {                numberOfGroups++ ;                panel.setRowNumber(numberOfGroups + numberOfUsers);                panel.addItem(group.getName(), numberOfGroups + numberOfUsers - 1);                mgrs.add(group);            }        }        currentUsers = mgrs.toArray();    }public voidlistUsers(List users){        listWhat = 'u';        panel.setSearchTitleText("select User");        panel.setGreen();        tapasUsers = users.toArray();        panel.clearSearchTable();        panel.setRowNumber(tapasUsers.length);        for (int i = 0; i < tapasUsers.length; i++) {            TapasUser tapasUser = (TapasUser) tapasUsers[i];            panel.addItem(tapasUser.getName(), i);        }    }public voidtableClicked(JTable table){        int index = table.getSelectedRow();        try {            if (listWhat == 'c') {                Controller.getInstance().changeCode( (CE) ces[index], functionIndex);            } else if (listWhat == 'p' || listWhat == 'q' || listWhat == 'n') {                Controller.getInstance().addProvider((Author) providers[index], functionIndex, listWhat);            } else if (listWhat == 'd') {                Patient patient = (Patient)table.getModel().getValueAt(index, 0);                Integer id = patient.getPkId();                List docs = Controller.getInstance().getCDAByPatientId(id.toString());                doc = (ClinicalDocument) docs.get(0);            } else if (listWhat == 'x') {                Controller.getInstance().changeDrug( (Drug) drugs[index]);            } else if (listWhat == 'r') {                Controller.getInstance().addRole((TapasRole) tapasRoles[index]);            } else if (listWhat == 'g') {                Controller.getInstance().addGroup((TapasGroup) tapasGroups[index]);            } else if (listWhat == 'u') {                Controller.getInstance().addUser((TapasUser) tapasUsers[index]);            } else if (listWhat == 'm' && index >= numberOfUsers) {                Controller.getInstance().setMgrGroup((TapasGroup) currentUsers[index], functionIndex);            } else if (listWhat == 'm' && index < numberOfUsers) {                Controller.getInstance().setMgrUser((TapasUser) currentUsers[index], functionIndex);            }            panel.setReady(true); //stop timer            index = 0; // neutral state        } catch (java.lang.ArrayIndexOutOfBoundsException e) {org.opentapas.commons.util.Logger.debug(e.getMessage() + "\nIN: SearchSideFunction - tableClicked() - ArrayIndexOutOfBoundsException");        } catch (java.lang.NullPointerException e) { org.opentapas.commons.util.Logger.debug(e.getMessage() + "\nIN: SearchSideFunction - tableClicked() - NullPointerException");        }    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/RecordTarget.java">
{public PatientRole getPatientRole() {        return this.patientRole;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasFunction.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/tapasdomain/Insurer.java">
{public String getName() {    return name;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/PatientRole.java">
{public Patient getPatient() {        return patient;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/SearchSidePanel.java">
{public void clearSearchTable(){        ((DefaultTableModel) table.getModel()).setRowCount(0);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/Controller.java">
{public static Controller getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/util/ToolKit.java">
{public static ToolKit getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/ClinicalDocument.java">
{public Patient getPatient() {    return this.getRecordTarget().getPatientRole().getPatient();  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasUser.java">
{public String getName() {        return userName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/TapearPanel.java">
{public Patient getPatient(){        return clinicalDocument.getRecordTarget().getPatientRole().getPatient();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.PN getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasRole.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/security/TapasGroup.java">
{public String getName() {        return name;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/remote/RemoteFacade.java">
{public static RemoteFacade getInstance() {    if (fac == null) {      fac = new RemoteFacade();    }    return fac;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Any.java">
{public Integer getPkId(){        return this.pkId == null ? Integer.MAX_VALUE : this.pkId;    }}
