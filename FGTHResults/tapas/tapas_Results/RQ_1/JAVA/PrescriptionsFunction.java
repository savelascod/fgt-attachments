public voidsetAvailable(boolean isAvailable){        available = isAvailable;    }public voidsetClinicalDocument(ClinicalDocument clinicalDocument){        this.clinicalDocument = clinicalDocument;    }this.section=sectionconsumable=tempSan.getConsumable()if (consumable.getManufacturedProduct() == null) {                        consumable.setManufacturedProduct(new ManufacturedProduct());                    }manufacturedProduct=consumable.getManufacturedProduct()protected voidlistPending(int selectedPendingTableRow){        panel.clearPendingTable();        if (pendingSubstanceAdministrations != null) {            int i = 0 ;            for (Object obj : pendingSubstanceAdministrations){                tempSan = (SubstanceAdministration) obj ;                panel.setPendingTableRowNumber(pendingSubstanceAdministrations.length);                panel.addPendingRow(                        (tempSan.getStartDate() == null ? "" : sdf.format(tempSan.getStartDate()))                        + " - " +                        (tempSan.getEndDate() == null ? "" : sdf.format(tempSan.getEndDate())),                        tempSan.getConsumable().getManufacturedProduct().getManufacturedLabeledDrug().getDrugProduct().getDisplayName()                        + " " +                        getShortData(tempSan),                        String.valueOf(panel.getRepeats()),                        i);                i++ ;            }            if (pendingSubstanceAdministrations.length > 0) {                panel.enablePendingTable(true);                panel.setSelectedPendingTableRow(selectedPendingTableRow);            }        } else {            panel.enablePendingTable(false);        }        Controller.getInstance().addNumberToPatientTabTitle(getNumberOfPendingSans(), functionName);    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/framework/Controller.java">
{public static Controller getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/util/ToolKit.java">
{public static ToolKit getInstance() {        return cINSTANCE;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/SubstanceAdministration.java">
{public Consumable getConsumable() {        return consumable;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/tapear/prescriptions/PrescriptionsFunctionPanel.java">
{protected void clearPendingTable(){        ((DefaultTableModel) pendingTable.getModel()).setRowCount(0);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/ems/Consumable.java">
{public ManufacturedProduct getManufacturedProduct() {        return manufacturedProduct;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/remote/RemoteFacade.java">
{public static RemoteFacade getInstance() {    if (fac == null) {      fac = new RemoteFacade();    }    return fac;  }}
