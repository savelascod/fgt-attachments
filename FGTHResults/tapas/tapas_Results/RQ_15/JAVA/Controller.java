Setsections= clinicalDocument.getActs()IteratorsectionsIterator= sections.iterator()while (sectionsIterator.hasNext()) {            Section section = (Section) sectionsIterator.next();            if (section.getCode().getCode().equalsIgnoreCase(ToolKit.PROBLEMHX) && hasAccessToPatientFunction(2)) {                ( (ProblemHxFunction) patientCentricFunctions[locatePatientFunction(2)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.MEDICALHX) && hasAccessToPatientFunction(3)) {                ( (MedicalHxFunction) patientCentricFunctions[locatePatientFunction(3)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.PROCHX) && hasAccessToPatientFunction(4)) {                ( (ProcedureHxFunction) patientCentricFunctions[locatePatientFunction(4)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.PRESCRIPTIONS) && hasAccessToPatientFunction(5)) {                ( (PrescriptionsFunction) patientCentricFunctions[locatePatientFunction(5)]).populate(section, documentManager);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.ALLERGYHX) && hasAccessToPatientFunction(6)) {                ( (AllergyFunction) patientCentricFunctions[locatePatientFunction(6)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.FAMILYHX) && hasAccessToPatientFunction(7)) {                ( (FamilyHxFunction) patientCentricFunctions[locatePatientFunction(7)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.RISKS) && hasAccessToPatientFunction(8)) {                ( (RisksFunction) patientCentricFunctions[locatePatientFunction(8)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.IMMUNIZATIONSHX) && hasAccessToPatientFunction(9)) {                ( (ImmunizationsFunction) patientCentricFunctions[locatePatientFunction(9)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.REFERRALHX) && hasAccessToPatientFunction(10)) {                ( (ReferralsFunction) patientCentricFunctions[locatePatientFunction(10)]).populate(section);            } else if (section.getCode().getCode().equalsIgnoreCase(ToolKit.ALERTS) && hasAccessToPatientFunction(11)) {                ( (AlertsFunction) patientCentricFunctions[locatePatientFunction(11)]).populate(section);                //This section is where the alerts get loaded into their respective panels                List recs = clinicalDocument.getRecommendations();                if (recs != null) {                    for (Object elem : recs) {                        ClinicalRecommendation rec = (ClinicalRecommendation) elem;                        ( (AlerterSideFunction) patientCentricSidePanels[0]).addAlertRow(                                rec);                    }                }            }        }public voidrefreshMedicalHx(Section section){        ( (MedicalHxFunction) patientCentricFunctions[locatePatientFunction(3)]).                populate(section);    }public voidasterisker(boolean saveButtonEnabled, boolean previous){        mainFrame.getCurrentTab().asteriskCurrentFunctionTabName(saveButtonEnabled,                previous);    }Sectionsection= (Section) objif ((section.getCode().getCode().equalsIgnoreCase(ToolKit.ALERTS) && hasAccessToPatientFunction(11)) & pcf instanceof AlertsFunction) {                            ((AlertsFunction) pcf).populate(section);                                                } else if ((section.getCode().getCode().equalsIgnoreCase(ToolKit.REFERRALHX) && hasAccessToPatientFunction(10)) & pcf instanceof ReferralsFunction) {                            ((ReferralsFunction) pcf).populate(section);                                                                        }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/commandImpl/GetDrugProductCommand.java">
{public String getCode() {    return code;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/CE.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Code.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Drug.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public CE getCode() {        return code;    }}
