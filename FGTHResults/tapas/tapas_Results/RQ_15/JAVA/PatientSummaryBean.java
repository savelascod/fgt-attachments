Setsections= cda.getActs()for (Object obj : sections) {         Section section = (Section) obj;         ArrayList acts = new ArrayList(section.getActs());         String secCode = section.getCode().getCode();         ArrayList actBeans = new ArrayList();         if (secCode.equalsIgnoreCase(ToolKit.PROBLEMHX)) {            this.problems = actBeans;            for (Object a : acts) {               TapasProblem prob = new TapasProblem( (Observation) a);               actBeans.add(prob);            }            this.setProblems(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.MEDICALHX)) {            this.medicalHx = actBeans;            for (Object a : acts) {               TapasMedicalHX prob = new TapasMedicalHX( (Observation) a);               actBeans.add(prob);            }            this.setMedicalHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.PROCHX)) {            this.procedures = actBeans;            for (Object a : acts) {               TapasSurgical surg = new TapasSurgical( (Procedure) a);               actBeans.add(surg);            }            this.setProcedures(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.                                           PRESCRIPTIONS)) {            this.ongoingMeds = actBeans;            /**            * @todo Filter out past drugs             */            for (Object a : acts) {               SubstanceAdministration san = (SubstanceAdministration) a;               if (!san.getPending()) {                  TapasMedication surg = new TapasMedication(san);                  actBeans.add(surg);               }            }            this.setOngoingMeds(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.ALLERGYHX)) {            this.allergies = actBeans;            for (Object a : acts) {               TapasAllergyHX surg = new TapasAllergyHX( (Observation) a);               actBeans.add(surg);            }            this.setAllergies(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.FAMILYHX)) {            this.familyHx = actBeans;            for (Object a : acts) {               TapasFamilyHx surg = new TapasFamilyHx( (Observation) a);               actBeans.add(surg);            }            this.setFamilyHx(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.IMMUNIZATIONSHX)) {            this.immunizations = actBeans;            for (Object a : acts) {               TapasImmunization surg = new TapasImmunization( (Procedure) a);               actBeans.add(surg);            }            this.setImmunizations(actBeans);         }         else if (secCode.equalsIgnoreCase(ToolKit.RISKS)) {            this.risks = actBeans;            for (Object a : acts) {               TapasSocial surg = new TapasSocial( (Observation) a);               actBeans.add(surg);            }            this.setRisks(actBeans);         }         /**            * @todo Complete Alerts section of Patient Bean          */         else if (secCode.equalsIgnoreCase(ToolKit.ALERTS)) {            this.alerts = actBeans;            for (Object a : acts) {               TapasAlert alert = new TapasAlert( (Observation) a, this.patientID);               actBeans.add(alert);            }            this.setAlerts(actBeans);         }      }public voidsetAllergies(List allergies){      this.allergies = allergies;   }for (Object obj : allergies) {            TapasAllergyHX allergy = (TapasAllergyHX) obj;            if (allergy != null){               stringBuffer.append(allergy.getSubstance() + "\n\n");            }            else {               stringBuffer.append("allergy object = null\n\n");            }         }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/commandImpl/GetDrugProductCommand.java">
{public String getCode() {    return code;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/CE.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Act.java">
{public CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Code.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/SubstanceAdministration.java">
{public Boolean getPending() {        return pending;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Entity.java">
{public org.opentapas.commons.rim.datatype.CE getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/datatype/Drug.java">
{public String getCode() {        return code;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/tapasFGTH//src/org/opentapas/commons/rim/Role.java">
{public CE getCode() {        return code;    }}
