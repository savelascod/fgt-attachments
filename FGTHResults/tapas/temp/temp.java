// --------------------------------------------------------------------------

// The OpenTAPAS Project -- Source File.
// Copyright (c) 2004 - 2006 The OpenTAPAS Team et al.
// --------------------------------------------------------------------------

// OSI Certified Open Source Software
// --------------------------------------------------------------------------

//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation; either version 2 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along 
// with this program; if not, write to the Free Software Foundation, Inc., 
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
/*
 * PatientFlagsFunction.java
 *
 * Created on May 19, 2005, 12:01 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package org.opentapas.tapear.patientflags;

import org.opentapas.tapear.framework.Controller;
import org.opentapas.tapear.framework.SidePanel;
import org.opentapas.tapear.framework.UserCentricFunction;

import javax.swing.*;


/**
 * <p>This function shows the patients' flags
 * NOT IMPLEMENTED IN THIS RELEASE</p>
 * 
 * Copyright (c) 2005. Department of Family Practice, University of British Columbia All Rights Reserved. 
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * * You should have received a copy of the GNU General Public License along with this program; 
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 *
 * TAPAS TEAM
 *
 * This software was written for the
 * Department of Family Practice
 * University of British Columbia
 * Vancouver,BC Canada
 *
 * @author TAPAS TEAM
 * @version $Revision: 250 $
 *
 */
public class PatientFlagsFunction implements UserCentricFunction {
    
    /**
     * Boolean to determine whether the funtion is active
     */
    private boolean active = false;

    /**
     * Name of the function
     */
    private String functionName = "Patient Flags";

    /**
     * Short name of the function this panel performs (i.e. for use on tabs)
     */
    private String functionShortName = "Flags";

    /**
     * Array of Sidepanels holding the Sidepanels for this function (not used at the moment)
     */
    private SidePanel[] SidePanels = null;

    /**
     * The GUI part of this function
     */
    private PatientFlagsFunctionPanel panel;


    /** Creates a new instance of NewClass */
    public PatientFlagsFunction() {
        panel = new PatientFlagsFunctionPanel();
    }

    /**
     * Returns whether function is active (not used now)
     * @return true / yes or false / no
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets function active or not (not used now)
     * @param isActive true / yes or false / no
     */
    public void setActive(boolean isActive) {
        active = isActive;
    }

    /**
     * Returns the name of the function
     * @return Name of the function
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Sets the name of the function
     * @param isFunctionName name of the function
     */
    public void setFunctionName(String isFunctionName) {
        functionName = isFunctionName;
    }

    /**
     * Gets the name of the function
     * @return short name of the function
     */
    public String getFunctionShortName() {
        return functionShortName;
    }

    /**
     * Sets the short name of the function
     * @param isFunctionShortName short name of the function
     */
    public void setFunctionShortName(String isFunctionShortName) {
        functionShortName = isFunctionShortName;
    }

    /**
     * returns the side panel at [index]
     * @param index the index nr of the side panel
     * @return the Sidepanel at [index]
     */
    public SidePanel getSidePanelAt(int index) {
        return SidePanels[index];
    }

    /**
     * Set sidepanel at [index]
     * @param isSidePanel Sidepanel
     * @param index where to put the Sidepanel
     */
    public void setSidePanel(SidePanel isSidePanel, int index) {
        SidePanels[index] = isSidePanel;
    }

    /**
     * Returns the panel / GUI part of the function
     * @return the panel / GUI part of the function
     */
    public PatientFlagsFunctionPanel getPanel() {
        return panel;
    }

    /**
     * Sets the panel / GUI part of the function
     * @param panel the panel / GUI part of the function
     */
    public void setPanel(JPanel panel) {
        this.panel = (PatientFlagsFunctionPanel) panel;
    }
}