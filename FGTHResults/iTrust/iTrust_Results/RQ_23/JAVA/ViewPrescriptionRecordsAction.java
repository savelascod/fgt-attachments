public PatientBeangetPatient(long patientID)throws ITrustException{		return patientDAO.getPatient(patientID);	}public List<PrescriptionBean>getPrescriptionsForPatient(long patientID)throws ITrustException{		PatientBean patient = patientDAO.getPatient(patientID);		if (loggedInMID == patientID) {			return patientDAO.getPrescriptions(patientID);		}				List<String> toList = new ArrayList<String>();		toList.add(patient.getEmail());				List<PatientBean> representatives = patientDAO.getRepresenting(patientID);		for(PatientBean representative : representatives) {			if (loggedInMID == representative.getMID()) {				return patientDAO.getPrescriptions(patientID);			}			toList.add(representative.getEmail());		}				List<PersonnelBean> dlhcps = patientDAO.getDeclaredHCPs(patientID);		for(PersonnelBean dlhcp : dlhcps) {			if (loggedInMID == dlhcp.getMID()) {				return patientDAO.getPrescriptions(patientID);			}			List<PersonnelBean> uaps = personnelDAO.getUAPsForHCP(dlhcp.getMID());			for(PersonnelBean uap : uaps) {				if (loggedInMID == uap.getMID()) {					return patientDAO.getPrescriptions(patientID);				}			}		}				Email email = new Email();		email.setToList(toList);		email.setFrom("noreply@itrust.com"); //$NON-NLS-1$		email.setSubject(Messages.getString("ViewPrescriptionRecordsAction.1")); //$NON-NLS-1$		email.setBody(Messages.getString("ViewPrescriptionRecordsAction.2")); //$NON-NLS-1$		emailer.sendEmail(email);		return patientDAO.getPrescriptions(patientID);	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ReviewsBean.java">
{public long getMID() 	{		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AdverseEventBean.java">
{public String getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PatientBean.java">
{public long getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PersonnelBean.java">
{public long getMID() {		return MID;	}}
