LabProcedureBeanlp2= new LabProcedureBean()lp2.setLoinc("00000-0")lp2.setCommentary("This is a routine procedure")lp2.setOvID(953)lp2.setPid(2L)lp2.setResults("")lp2.allow()lp2.statusPending()lpDAO.addLabProcedure(lp2)LabProcedureBeanlp3= new LabProcedureBean()lp3.setLoinc("10543-5")lp3.setCommentary("This is a routine procedure")lp3.setOvID(954)lp3.setPid(2L)lp3.setResults("")lp3.allow()lp3.statusPending()lpDAO.addLabProcedure(lp3)action2=newLabProcHCPAction(factory, 9000000003L)List<LabProcedureBean>procedures= action2.getLabProcForNextMonth()
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/action/LabProcHCPAction.java">
{public List<LabProcedureBean> getLabProcForNextMonth() throws DBException {		List<LabProcedureBean> listLabProc = new ArrayList<LabProcedureBean>(0);		List<OfficeVisitBean> listOV = ovDAO.getAllOfficeVisitsForLHCP(loggedInMID);		for (OfficeVisitBean ov : listOV) {			if (listLabProc.isEmpty() == true) {				listLabProc = lpDAO.getLabProceduresForLHCPForNextMonth(ov.getID());			}			else {				for (LabProcedureBean lb : lpDAO.getLabProceduresForLHCPForNextMonth(ov.getID())) {					listLabProc.add(lb);				}			}		}		return listLabProc;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/LabProcedureBean.java">
{public void statusPending(){		 this.status = Pending;	 }}
