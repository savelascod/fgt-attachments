public ViewVisitedHCPsAction(DAOFactory factory, long loggedInMID){		patientMID = loggedInMID;		docDAO = factory.getPersonnelDAO();		visitDAO = factory.getOfficeVisitDAO();		patientDAO = factory.getPatientDAO();				declareAction = new DeclareHCPAction(factory, loggedInMID);	}for (HCPVisitBean bean : getVisitedHCPs()) {			if (name.equals(bean.getHCPName())) {				r = bean;				break;			}		}for (HCPVisitBean visit: getVisitedHCPs()) {			if (0 == visit.getHCPName().toLowerCase().compareTo(name.toLowerCase())) {				Long mid = Long.valueOf(visit.getHCPMID());				declareAction.undeclareHCP(mid.toString());				visit.setDesignated(false);			}		}for (HCPVisitBean visit: getVisitedHCPs()) {			if (0 == visit.getHCPName().toLowerCase().compareTo(name.toLowerCase())) {				match = true;				Long mid = Long.valueOf(visit.getHCPMID());				if (!patientDAO.checkDeclaredHCP(patientMID, visit.getHCPMID())) {					declareAction.declareHCP(mid.toString());				}				visit.setDesignated(true);			}		}visits=getAllVisitedHCPs(lastName, specialty, zip)
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public PatientDAO getPatientDAO() {		return new PatientDAO(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/action/ViewVisitedHCPsAction.java">
{public List<HCPVisitBean> getVisitedHCPs() {		List<HCPVisitBean> visits;		try {			visits = getAllVisitedHCPs(null, null, null);		}		catch (ITrustException ie) {			visits = new ArrayList<HCPVisitBean>();		}					return visits;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/HCPVisitBean.java">
{public String getHCPName() {		return HCPName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/HCPDiagnosisBean.java">
{public String getHCPName() {		return HCPname;	}}
