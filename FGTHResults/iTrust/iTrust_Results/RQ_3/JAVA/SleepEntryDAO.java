public List<SleepEntryBean>getPatientSleepDiary(long patientMID)pstring.setLong(1, patientMID)public List<SleepEntryBean>getPatientSleepDiaryTotals(long patientMID)public intdeleteSleepEntry(long entryID, long patientMID)pstmt.setLong(2, patientMID)public intupdateSleepEntry(long entryID, long patientMID,			SleepEntryBean sleepEntry)pstmt.setLong(6, patientMID)DateupperlongpatientMIDthrows DBException{		Connection conn = null;		PreparedStatement pstring = null;		try {			conn = factory.getConnection();			pstring = conn.prepareStatement("SELECT * FROM sleepEntry "					+ " WHERE PatientID = ? " + " AND Date BETWEEN ? AND ? "					+ " ORDER BY Date DESC");			pstring.setLong(1, patientMID);			// Convert java.util.Date to java.sql.Date			java.sql.Date lowerSQLDate = new java.sql.Date(lower.getTime());			java.sql.Date upperSQLDate = new java.sql.Date(upper.getTime());			pstring.setDate(2, lowerSQLDate);			pstring.setDate(3, upperSQLDate);			final  ResultSet results = pstring.executeQuery();			final  List<SleepEntryBean> diaryList = this.sleepLoader					.loadList(results);			results.close();			pstring.close();			return diaryList;		} catch (SQLException e) {			throw new DBException(e);		} finally {			DBUtil.closeConnection(conn, pstring);		}	}DateupperlongpatientMIDthrows DBException{		Connection conn = null;		PreparedStatement pstring = null;		try {			conn = factory.getConnection();			pstring = conn.prepareStatement(" SELECT EntryID AS EntryID, Date AS Date, SleepType AS SleepType, "					+ " SUM(Hours) AS Hours, PatientID AS PatientID, LabelID AS LabelID  "					+ " FROM sleepEntry "					+ " WHERE PatientID = ? " + " AND Date BETWEEN ? AND ? "					+ " GROUP BY Date " + " ORDER BY Date DESC");			// Add params			pstring.setLong(1, patientMID);			// Convert java.util.Date to java.sql.Date			java.sql.Date lowerSQLDate = new java.sql.Date(lower.getTime());			java.sql.Date upperSQLDate = new java.sql.Date(upper.getTime());			pstring.setDate(2, lowerSQLDate);			pstring.setDate(3, upperSQLDate);			final  ResultSet results = pstring.executeQuery();			final  List<SleepEntryBean> diaryList = this.sleepLoader					.loadList(results);			results.close();			pstring.close();			return diaryList;		} catch (SQLException e) {			throw new DBException(e);		} finally {			DBUtil.closeConnection(conn, pstring);		}	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public Connection getConnection() throws SQLException {		return driver.getConnection();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/IConnectionDriver.java">
{public Connection getConnection() throws SQLException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/ProductionConnectionDriver.java">
{public Connection getConnection() throws SQLException {		try {			if (initialContext == null)				initialContext = new InitialContext();			return ((DataSource) (((Context) initialContext.lookup("java:comp/env"))).lookup("jdbc/itrust"))					.getConnection();		} catch (NamingException e) {			throw new SQLException(("Context Lookup Naming Exception: " + e.getMessage()));		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//test/edu/ncsu/csc/itrust/unit/testutils/EvilDAOFactory.java">
{@Override	public Connection getConnection() throws SQLException {		if (numCorrect-- > 0) //check THEN decrement			return driver.getConnection();		else			throw new SQLException(MESSAGE);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//test/edu/ncsu/csc/itrust/unit/testutils/TestDAOFactory.java">
{@Override	public Connection getConnection() throws SQLException {		return dataSource.getConnection();	}}
