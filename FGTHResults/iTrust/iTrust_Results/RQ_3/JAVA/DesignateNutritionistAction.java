patientDAO=factory.getPatientDAO()public intupdateNutritionist(long HCPID)throws ITrustException{		/* If the HCPID = -1 (HCPID can never be -1), then the		 * patient wishes to have 'None' as his selected hcp and 		 * we will delete it.		 * if not -1, we will try first to just update it, and assume that 		 * the patient already has a designated nutritionist, if not		 * we will add 1 for him		 */		try {			int updated = 0;			PersonnelBean personnel = personnelDAO.getPersonnel(HCPID);			PersonnelBean currentHCP = getDesignatedNutritionist();			if (personnel != null && currentHCP != null && 					(personnel.getMID() == currentHCP.getMID())) {				//the patient wants to update it to his current nutritionist				updated = -1;			} else if (HCPID == -1) {				updated = patientDAO.deleteDesignatedNutritionist(loggedInMID);							} else { //must want to change/add his designated nutritionist				//must first make sure the hcp in question has 				//specialty of nutritionist				boolean isNutritionist = (personnel != null &&						personnel.getSpecialty().equalsIgnoreCase("Nutritionist"));				if (isNutritionist) {					updated = patientDAO.							updateDesignatedNutritionist(loggedInMID, HCPID);					if (updated == 0) { //didn't update anything, so have to add						updated = patientDAO.addDesignatedNutritionist(loggedInMID, HCPID);					}				} else {					updated = -2;				}			}			return updated;		} catch (DBException d) {			throw new ITrustException("Error retrieving information");		}	}longHCPID= patientDAO.getDesignatedNutritionist(loggedInMID)PersonnelBeanHCP= personnelDAO.getPersonnel(HCPID)return HCP;
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public PatientDAO getPatientDAO() {		return new PatientDAO(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ReviewsBean.java">
{public long getMID() 	{		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AdverseEventBean.java">
{public String getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PatientBean.java">
{public long getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/action/DesignateNutritionistAction.java">
{public PersonnelBean getDesignatedNutritionist() throws ITrustException {		try {			long HCPID = patientDAO.getDesignatedNutritionist(loggedInMID);			PersonnelBean HCP = personnelDAO.getPersonnel(HCPID);			return HCP;		} catch (DBException d) {			throw new ITrustException("Error retrieving information");		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PersonnelBean.java">
{public long getMID() {		return MID;	}}
