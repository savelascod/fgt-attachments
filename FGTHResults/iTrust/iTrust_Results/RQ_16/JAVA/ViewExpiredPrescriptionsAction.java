public PatientBeangetPatient(long patientID)throws ITrustException{		return patientDAO.getPatient(patientID);	}public PersonnelBeangetPrescribingDoctor(PrescriptionBean prescription)throws ITrustException{		return personnelDAO.getPrescribingDoctor(prescription);	}public List<PrescriptionBean>getPrescriptionsForPatient(long patientID)throws ITrustException{		PatientBean patient = patientDAO.getPatient(patientID);		if (loggedInMID == patientID) {			return patientDAO.getExpiredPrescriptions(patientID);		}				List<String> toList = new ArrayList<String>();		toList.add(patient.getEmail());				List<PatientBean> representatives = patientDAO.getRepresenting(patientID);		for(PatientBean representative : representatives) {			if (loggedInMID == representative.getMID()) {				return patientDAO.getExpiredPrescriptions(patientID);			}			toList.add(representative.getEmail());		}				List<PersonnelBean> dlhcps = patientDAO.getDeclaredHCPs(patientID);		for(PersonnelBean dlhcp : dlhcps) {			if (loggedInMID == dlhcp.getMID()) {				return patientDAO.getExpiredPrescriptions(patientID);			}			List<PersonnelBean> uaps = personnelDAO.getUAPsForHCP(dlhcp.getMID());			for(PersonnelBean uap : uaps) {				if (loggedInMID == uap.getMID()) {					return patientDAO.getPrescriptions(patientID);				}			}		}				Email email = new Email();		email.setToList(toList);		email.setFrom("noreply@itrust.com");		email.setSubject("Undesignated Personnel Have Accessed Your Prescription Records");		email.setBody("An undesignated HCP or UAP has accessed your prescription records. For more information, please log in to iTrust.");		emailer.sendEmail(email);		return patientDAO.getPrescriptions(patientID);	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ReviewsBean.java">
{public long getMID() 	{		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AdverseEventBean.java">
{public String getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PatientBean.java">
{public long getMID() {		return MID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PersonnelBean.java">
{public long getMID() {		return MID;	}}
