public ActivityFeedAction(DAOFactory factory, long loggedInMID){		this.loggedInMID = loggedInMID;		this.transDAO = factory.getTransactionDAO();		this.authDAO = factory.getAuthDAO();		this.patientDAO = factory.getPatientDAO();	}public StringgetMessageAsSentence(String actor, Timestamp timestamp, TransactionType code){		String result = actor + " ";		StringBuffer buf = new StringBuffer();			for (TransactionType type : TransactionType.values()) {			if (code.getCode() == type.getCode() && type.isPatientViewable())				buf.append(type.getActionPhrase());		}		result += buf.toString();				SimpleDateFormat formatter = new SimpleDateFormat("h:mma.");		switch(recent(new Date(timestamp.getTime()))) {		case 0:			result += " today";			break;		case 1:			result += " yesterday";			break;		case 2:			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");			result += " on " + sdf.format(new Date(timestamp.getTime()));			break;		default:			break;		}				result += " at " + formatter.format(timestamp);								return replaceNameWithYou(result);	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/ExerciseType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/exception/ErrorList.java">
{@Override	public String toString() {		return errorList.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/FlagValue.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/Ethnicity.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/MealType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/State.java">
{@Override	public String toString() {		return super.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/PersonnelReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/PersonnelReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AllergyBean.java">
{@Override	public String toString() {		return this.description;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/Email.java">
{@Override	public String toString() {		return "FROM: " + from + " TO: " + toList.toString() + " SUBJECT: " + subject + " BODY: " + body;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologyScheduleOVRecordBean.java">
{@Override	public String toString() {		return "OphthalmologyScheduleOVRecordBean [patientmid=" + patientmid				+ ", doctormid=" + doctormid + ", oid=" + oid + ", date="				+ date + ", docLastName=" + docLastName + ", docFirstName="				+ docFirstName + ", comment=" + comment + ", status=" + status				+ "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/SleepType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ObstetricsRecordBean.java">
{@Override	public String toString() {		return "ObstetricsRecordBean [mid=" + mid + ", oid=" + oid + ", pid="				+ pregId + ", lmp=" + lmp + ", edd=" + edd + ", weeksPregnant="				+ weeksPregnant + ", dateVisit=" + dateVisit				+ ", yearConception=" + yearConception + ", hoursInLabor="				+ hoursInLabor + ", deliveryType=" + deliveryType				+ ", pregnancyStatus=" + pregnancyStatus + ", weight=" + weight				+ ", bloodPressureS=" + bloodPressureS + ", bloodPressureD="				+ bloodPressureD + ", fhr=" + fhr + ", fhu=" + fhu + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/PregnancyStatus.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologySurgeryRecordBean.java">
{@Override	public String toString() {		return "OphthalmologySurgeryRecordBean [mid=" + mid + ", oid=" + oid				+ ", visitDate=" + visitDate + ", docLastName=" + docLastName				+ ", docFirstName=" + docFirstName + ", vaNumOD=" + vaNumOD				+ ", vaDenOD=" + vaDenOD + ", vaNumOS=" + vaNumOS				+ ", vaDenOS=" + vaDenOS + ", sphereOD=" + sphereOD				+ ", sphereOS=" + sphereOS + ", cylinderOD=" + cylinderOD				+ ", cylinderOS=" + cylinderOS + ", axisOD=" + axisOD				+ ", axisOS=" + axisOS + ", addOD=" + addOD + ", addOS="				+ addOS + ", surgery=" + surgery + ",surgeryNotes=" 				+ surgeryNotes + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/DemographicReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/DemographicReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/ReportFilter.java">
{public abstract String toString();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/DeliveryType.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/tags/ICD9CMLink.java">
{public String getCode() {		return code;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AdverseEventBean.java">
{public String getCode() {		return code;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/HCPLinkBean.java">
{public String getCode() {		return code;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologyOVRecordBean.java">
{@Override	public String toString() {		return "OphthalmologyOVRecordBean [mid=" + mid + ", oid=" + oid				+ ", visitDate=" + visitDate + ", docLastName=" + docLastName				+ ", docFirstName=" + docFirstName + ", vaNumOD=" + vaNumOD				+ ", vaDenOD=" + vaDenOD + ", vaNumOS=" + vaNumOS				+ ", vaDenOS=" + vaDenOS + ", sphereOD=" + sphereOD				+ ", sphereOS=" + sphereOS + ", cylinderOD=" + cylinderOD				+ ", cylinderOS=" + cylinderOS + ", axisOD=" + axisOD				+ ", axisOS=" + axisOS + ", addOD=" + addOD + ", addOS="				+ addOS + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/TransactionType.java">
{public boolean isPatientViewable(){		return patientView;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/MedicalReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/MedicalReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public PatientDAO getPatientDAO() {		return new PatientDAO(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/BloodType.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/Gender.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/SortDirection.java">
{public String toString() {		return dirString;	}}
