public SetSecurityQuestionAction(DAOFactory factory, long rLoggedInMID)throws ITrustException{		this.authDAO = factory.getAuthDAO();		loggedInMID = checkMID(rLoggedInMID);	}private longcheckMID(long mid)throws ITrustException{		if (!authDAO.checkUserExists(mid))			throw new ITrustException("MID " + mid + " is not a user!");		return mid;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public AuthDAO getAuthDAO() {		return new AuthDAO(this);	}}
