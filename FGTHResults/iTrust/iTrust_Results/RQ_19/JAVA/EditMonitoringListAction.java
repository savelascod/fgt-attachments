public EditMonitoringListAction(DAOFactory factory, long loggedInMID){		this.loggedInMID = loggedInMID;		this.rmDAO = factory.getRemoteMonitoringDAO();		this.authDAO = factory.getAuthDAO();	}public booleanaddToList(long patientMID, TelemedicineBean tBean)throws DBException{		return rmDAO.addPatientToList(patientMID, loggedInMID, tBean);	}public booleanremoveFromList(long patientMID)throws DBException{		return rmDAO.removePatientFromList(patientMID, loggedInMID);	}public booleanisPatientInList(long patientMID)throws DBException{		List<RemoteMonitoringDataBean> dataset = rmDAO.getPatientsData(loggedInMID);		for(RemoteMonitoringDataBean d: dataset) {			if(d.getPatientMID() == patientMID)				return true;		}		return false;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/DAOFactory.java">
{public AuthDAO getAuthDAO() {		return new AuthDAO(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/RemoteMonitoringDataBean.java">
{public long getPatientMID() {		return patientMID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ReportRequestBean.java">
{public long getPatientMID() {		return patientMID;	}}
