public voidsetPatient(long patient){		this.patient = patient;	}public voidsetDate(Timestamp date){		this.date = (Timestamp) date.clone();	}@Overridepublic booleanequals(Object other){	   	    if ( this == other ){	    	return true;	    }	    if ( !(other instanceof ApptBean) ){	    	return false;	    }	    	    ApptBean otherAppt = (ApptBean)other;		return otherAppt.getApptID() == getApptID();	    	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ApptBean.java">
{public int getApptID() {		return apptID;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/BillingBean.java">
{public int getApptID() {		return apptID;	}}
