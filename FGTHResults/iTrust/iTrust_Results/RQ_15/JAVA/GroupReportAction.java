List<PatientBean>patientspatients=getAllPatients()patients=filter.filter(patients)return new GroupReportBean(patients, filters);public StringgetComprehensiveDemographicInfo(PatientBean patient, DemographicReportFilterType filterType){		switch (filterType) {		case GENDER:			return patient.getGender().toString();		case LAST_NAME:			return patient.getLastName();		case FIRST_NAME:			return patient.getFirstName();		case CONTACT_EMAIL:			return patient.getEmail();		case STREET_ADDR:			return patient.getStreetAddress1() + " " + patient.getStreetAddress2();		case CITY:			return patient.getCity();		case STATE:			return patient.getState();		case ZIP:			return patient.getZip();		case PHONE:			return patient.getPhone();		case EMER_CONTACT_NAME:			return patient.getEmergencyName();		case EMER_CONTACT_PHONE:			return patient.getEmergencyPhone();		case INSURE_NAME:			return patient.getIcName();		case INSURE_ADDR:			return patient.getIcAddress1() + " " + patient.getIcAddress2();		case INSURE_CITY:			return patient.getIcCity();		case INSURE_STATE:			return patient.getIcState();		case INSURE_ZIP:			return patient.getIcZip();		case INSURE_PHONE:			return patient.getIcPhone();		case MID:			return Long.toString(patient.getMID());		case INSURE_ID:			return patient.getIcID();		case PARENT_FIRST_NAME:			try {				List<FamilyMemberBean> parents = fDAO.getParents(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean parent : parents) {					buff.append(parent.getFirstName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case PARENT_LAST_NAME:			try {				List<FamilyMemberBean> parents = fDAO.getParents(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean parent : parents) {					buff.append(parent.getLastName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case CHILD_FIRST_NAME:			try {				List<FamilyMemberBean> children = fDAO.getChildren(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean child : children) {					buff.append(child.getFirstName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case CHILD_LAST_NAME:			try {				List<FamilyMemberBean> children = fDAO.getChildren(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean child : children) {					buff.append(child.getLastName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case SIBLING_FIRST_NAME:			try {				List<FamilyMemberBean> siblings = fDAO.getSiblings(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean sibling : siblings) {					buff.append(sibling.getFirstName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case SIBLING_LAST_NAME:			try {				List<FamilyMemberBean> siblings = fDAO.getSiblings(patient.getMID());				StringBuffer buff = new StringBuffer();				for (FamilyMemberBean sibling : siblings) {					buff.append(sibling.getLastName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		case DEACTIVATED:			return patient.getDateOfDeactivationStr();		default:			break;		}		return null;	}public StringgetComprehensiveMedicalInfo(PatientBean patient, MedicalReportFilterType filterType){		try {			String out;			StringBuffer buff = new StringBuffer();			List<PrescriptionBean> prescriptions;			switch (filterType) {			case ALLERGY:				List<AllergyBean> allergies = aDAO.getAllergies(patient.getMID());				for (AllergyBean allergy : allergies) {					buff.append(allergy.getNDCode());					buff.append("\n");				}				out = buff.toString();				return out;			case CURRENT_PRESCRIPTIONS:				prescriptions = pDAO.getCurrentPrescriptions(patient.getMID());				for (PrescriptionBean prescription : prescriptions) {					buff.append(prescription.getMedication().getNDCodeFormatted());					buff.append("\n");				}				out = buff.toString();				return out;			case DIAGNOSIS_ICD_CODE:				List<DiagnosisBean> diagnoses = pDAO.getDiagnoses(patient.getMID());				for (DiagnosisBean diagnosis : diagnoses) {					buff.append(diagnosis.getICDCode());					buff.append("\n");				}				out = buff.toString();				return out;			case LOWER_OFFICE_VISIT_DATE:				List<OfficeVisitBean> visits = oDAO.getAllOfficeVisits(patient.getMID());				for (OfficeVisitBean visit : visits) {					buff.append(visit.getVisitDateStr());					buff.append("\n");				}				out = buff.toString();				return out;			case PASTCURRENT_PRESCRIPTIONS:				prescriptions = pDAO.getPrescriptions(patient.getMID());				for (PrescriptionBean prescription : prescriptions) {					buff.append(prescription.getMedication().getNDCodeFormatted());					buff.append("\n");				}				out = buff.toString();				return out;			case PROCEDURE:				List<ProcedureBean> procedures = pDAO.getProcedures(patient.getMID());				for (ProcedureBean procedure : procedures) {					buff.append(procedure.getCPTCode());					buff.append("\n");				}				out = buff.toString();				return out;			default:				break;			}		} catch(Exception e) {			return null;		}		return null;	}public StringgetComprehensivePersonnelInfo(PatientBean patient, PersonnelReportFilterType filterType){		switch (filterType) {		case DLHCP:			try {				List<PersonnelBean> dlhcps = pDAO.getDeclaredHCPs(patient.getMID());				StringBuffer buff = new StringBuffer();				for (PersonnelBean dlhcp : dlhcps) {					buff.append(dlhcp.getFullName());					buff.append("\n");				}				String out = buff.toString();				return out;			} catch (Exception e) {				break;			}		default:			break;		}		return null;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/ExerciseType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/exception/ErrorList.java">
{@Override	public String toString() {		return errorList.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/FlagValue.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/Ethnicity.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/MealType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/State.java">
{@Override	public String toString() {		return super.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/PersonnelReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/PersonnelReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/AllergyBean.java">
{@Override	public String toString() {		return this.description;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/Email.java">
{@Override	public String toString() {		return "FROM: " + from + " TO: " + toList.toString() + " SUBJECT: " + subject + " BODY: " + body;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologyScheduleOVRecordBean.java">
{@Override	public String toString() {		return "OphthalmologyScheduleOVRecordBean [patientmid=" + patientmid				+ ", doctormid=" + doctormid + ", oid=" + oid + ", date="				+ date + ", docLastName=" + docLastName + ", docFirstName="				+ docFirstName + ", comment=" + comment + ", status=" + status				+ "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/SleepType.java">
{public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ZipCodeBean.java">
{public String getZip() 	{		return zip;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/ObstetricsRecordBean.java">
{@Override	public String toString() {		return "ObstetricsRecordBean [mid=" + mid + ", oid=" + oid + ", pid="				+ pregId + ", lmp=" + lmp + ", edd=" + edd + ", weeksPregnant="				+ weeksPregnant + ", dateVisit=" + dateVisit				+ ", yearConception=" + yearConception + ", hoursInLabor="				+ hoursInLabor + ", deliveryType=" + deliveryType				+ ", pregnancyStatus=" + pregnancyStatus + ", weight=" + weight				+ ", bloodPressureS=" + bloodPressureS + ", bloodPressureD="				+ bloodPressureD + ", fhr=" + fhr + ", fhu=" + fhu + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/PregnancyStatus.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologySurgeryRecordBean.java">
{@Override	public String toString() {		return "OphthalmologySurgeryRecordBean [mid=" + mid + ", oid=" + oid				+ ", visitDate=" + visitDate + ", docLastName=" + docLastName				+ ", docFirstName=" + docFirstName + ", vaNumOD=" + vaNumOD				+ ", vaDenOD=" + vaDenOD + ", vaNumOS=" + vaNumOS				+ ", vaDenOS=" + vaDenOS + ", sphereOD=" + sphereOD				+ ", sphereOS=" + sphereOS + ", cylinderOD=" + cylinderOD				+ ", cylinderOS=" + cylinderOS + ", axisOD=" + axisOD				+ ", axisOS=" + axisOS + ", addOD=" + addOD + ", addOS="				+ addOS + ", surgery=" + surgery + ",surgeryNotes=" 				+ surgeryNotes + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/DemographicReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/DemographicReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/ReportFilter.java">
{public abstract String toString();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/DeliveryType.java">
{@Override	public String toString() {		if (name == null)			return "";		return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/forms/VisitReminderReturnForm.java">
{public String getFirstName() {		return firstName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PatientBean.java">
{public String getDateOfDeactivationStr() {		return dateOfDeactivationStr;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/OphthalmologyOVRecordBean.java">
{@Override	public String toString() {		return "OphthalmologyOVRecordBean [mid=" + mid + ", oid=" + oid				+ ", visitDate=" + visitDate + ", docLastName=" + docLastName				+ ", docFirstName=" + docFirstName + ", vaNumOD=" + vaNumOD				+ ", vaDenOD=" + vaDenOD + ", vaNumOS=" + vaNumOS				+ ", vaDenOS=" + vaDenOS + ", sphereOD=" + sphereOD				+ ", sphereOS=" + sphereOS + ", cylinderOD=" + cylinderOD				+ ", cylinderOS=" + cylinderOS + ", axisOD=" + axisOD				+ ", axisOS=" + axisOS + ", addOD=" + addOD + ", addOS="				+ addOS + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/FamilyMemberBean.java">
{public String getFirstName() {		return firstName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/MedicalReportFilter.java">
{@Override		public String toString() {			return this.name;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/report/MedicalReportFilter.java">
{@Override	public String toString() {		String out = "Filter by " + filterType.toString() + " with value " + filterValue;		return out;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/beans/PersonnelBean.java">
{public String getPhone() {		return phone;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/action/GroupReportAction.java">
{private List<PatientBean> getAllPatients() throws DBException {		return pDAO.getAllPatients();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/BloodType.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/Gender.java">
{@Override	public String toString() {		return getName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/enums/SortDirection.java">
{public String toString() {		return dirString;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/iTrust//src/edu/ncsu/csc/itrust/dao/mysql/PatientDAO.java">
{public List<PatientBean> getAllPatients() throws DBException {		Connection conn = null;		PreparedStatement ps = null;		try {			conn = factory.getConnection();			ps = conn.prepareStatement("SELECT * FROM patients ");			ResultSet rs = ps.executeQuery();			List<PatientBean> loadlist = patientLoader.loadList(rs);			rs.close();			ps.close();			return loadlist;		} catch (SQLException e) {						throw new DBException(e);		} finally {			DBUtil.closeConnection(conn, ps);		}	}}
