UsercreatedUser= us.saveUser(u, "Openmr5xy")assertTrue("The user returned by the create user method should equal the passed in user", createdUser.equals(u))createdUser=us.getUserByUsername("bwolfe")assertTrue("The created user should equal the passed in user", createdUser.equals(u))UserpreliminaryFetchedUser= userService.getUser(2)assertNull(preliminaryFetchedUser)PersonpersonToMakeUser= Context.getPersonService().getPerson(2)((Patient)personToMakeUser).getIdentifiers().size()Useruser= new User(personToMakeUser)IntegershouldCreateUserWhoIsPatientAlreadyTestUserIdCreated= user.getUserId()UserfetchedUser= userService.getUser(shouldCreateUserWhoIsPatientAlreadyTestUserIdCreated)UserfetchedUser3= userService.getUser(3)if (fetchedUser3 != null)			throw new Exception("There is a user with id #3");assertNotNull("Uh oh, the user object was not created", fetchedUser)assertNotNull("Uh oh, the username was not saved", fetchedUser.getUsername())assertTrue("Uh oh, the username was not saved", fetchedUser.getUsername().equals("bwolfe"))assertTrue("Uh oh, the role was not assigned", fetchedUser.hasRole("Some Role"))List<User>allUsers= userService.getAllUsers()assertEquals(11, allUsers.size())Stringusername= "admin"Useruser= us.getUserByUsername(username)assertNotNull("username not found " + username, user)List<User>users= Context.getUserService().getUsers("Johnson", null, false)Assert.assertEquals(3, users.size())Assert.assertTrue(containsId(users, 2))Assert.assertTrue(containsId(users, 4))Assert.assertTrue(containsId(users, 5))Personperson= Context.getPersonService().getPerson(1)person.getNames().size()Stringhash= Security.encodeString("new password" + salt)userService.changeHashedPassword(user, hash, salt)List<User>users= Context.getUserService().getAllUsers()Assert.assertEquals(4, users.size())List<User>users= Context.getUserService().getAllUsers()Assert.assertEquals(12, users.size())List<User>users= userService.getUsersByName("Susy", "Kingman", false)assertEquals(1, users.size())UservoidedUser= userService.getUser(501)List<User>users= userService.getUsersByName("Bruno", "Otterbourg", true)assertTrue(users.contains(voidedUser))UservoidedUser= userService.getUser(501)List<User>users= userService.getUsersByName("Bruno", "Otterbourg", false)assertFalse(users.contains(voidedUser))List<User>users= userService.getUsersByName("John", "Doe", false)assertEquals(1, users.size())List<User>users= Context.getUserService().getUsers("John Doe", null, false)Assert.assertEquals(1, users.size())Personperson= new Person(5508)List<User>users= Context.getUserService().getUsersByPerson(person, true)Assert.assertEquals(3, users.size())Personperson= new Person(5508)List<User>users= Context.getUserService().getUsersByPerson(person, false)Assert.assertEquals(2, users.size())final intnumberOfUserProperties= user.getUserProperties().size()UserupdatedUser= userService.saveUserProperty(USER_PROPERTY_KEY, USER_PROPERTY_VALUE)assertNotNull(updatedUser.getUserProperty(USER_PROPERTY_KEY))assertEquals(USER_PROPERTY_VALUE, updatedUser.getUserProperty(USER_PROPERTY_KEY))assertEquals((numberOfUserProperties + 1), updatedUser.getUserProperties().size())UserupdatedUser= userService.saveUserProperties(propertiesMap)assertEquals(2, updatedUser.getUserProperties().size())assertEquals(USER_PROPERTY_VALUE_1, updatedUser.getUserProperty(USER_PROPERTY_KEY_1))assertEquals(USER_PROPERTY_VALUE_2, updatedUser.getUserProperty(USER_PROPERTY_KEY_2))
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/UserDAO.java">
{public List<User> getAllUsers() throws DAOException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/LoginCredential.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/hibernate/HibernateUserDAO.java">
{@SuppressWarnings("unchecked")	public List<User> getAllUsers() throws DAOException {		return sessionFactory.getCurrentSession().createQuery("from User u order by u.userId").list();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{public List<PatientIdentifier> getIdentifiers() {		return identifiers;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/UserService.java">
{@Authorized( { PrivilegeConstants.VIEW_USERS })	public List<User> getAllUsers() throws APIException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Concept.java">
{@ElementList	public Collection<ConceptName> getNames() {		return getNames(false);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static UserService getUserService() {		return getServiceContext().getUserService();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Cohort.java">
{public int size() {		return getMemberIds() == null ? 0 : getMemberIds().size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{@ElementList	public Set<PersonName> getNames() {		if (names == null) {			names = new TreeSet<PersonName>();		}		return this.names;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public Set<PatientIdentifier> getIdentifiers() {		if (identifiers == null) {			identifiers = new LinkedHashSet<PatientIdentifier>();		}		return this.identifiers;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/FormatTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/cohort/CohortSearchHistory.java">
{public int size() {		return searchHistory.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/UserListItem.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public UserService getUserService() {		return getService(UserService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/messagesource/PresentationMessageMap.java">
{public int size() {		return internalMap.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public Map<String, String> getUserProperties() {		if (userProperties == null) {			userProperties = new HashMap<String, String>();		}		return userProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/impl/UserServiceImpl.java">
{@Transactional(readOnly = true)	public List<User> getAllUsers() throws APIException {		return dao.getAllUsers();	}}
