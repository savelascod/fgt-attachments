public UserformBackingObject(WebRequest request, @RequestParam(required = false, value = "person_id") Integer personId){		String userId = request.getParameter("userId");		User u = null;		try {			u = Context.getUserService().getUser(Integer.valueOf(userId));		}		catch (Exception ex) {}		if (u == null) {			u = new User();		}		if (personId != null) {			u.setPerson(Context.getPersonService().getPerson(personId));		} else if (u.getPerson() == null) {			Person p = new Person();			p.addName(new PersonName());			u.setPerson(p);		}		return u;	}@RequestParam(required = false, value = "createNewPerson")StringcreateNewPersonif (createNewPerson != null) {			model.addAttribute("createNewPerson", createNewPerson);		}@RequestParam(required = false, value = "userFormPassword")Stringpassword@RequestParam(required = false, value = "createNewPerson")StringcreateNewPersonreturn showForm(user.getUserId(), createNewPerson, user, model);if (password == null || password.equals("XXXXXXXXXXXXXXX")) {				password = "";			}if (!password.equals(confirm)) {				errors.reject("error.password.match");			}if (password.length() == 0 && isNewUser(user)) {				errors.reject("options.login.password.null");			}if (password.length() > 0) {				try {					OpenmrsUtil.validatePassword(user.getUsername(), password, user.getSystemId());				}				catch (PasswordException e) {					errors.reject(e.getMessage());				}			}String[]keys= request.getParameterValues("property")if (keys != null && values != null) {				for (int x = 0; x < keys.length; x++) {					String key = keys[x];					String val = values[x];					user.setUserProperty(key, val);				}			}return showForm(user.getUserId(), createNewPerson, user, model);us.saveUser(user, password)if (!password.equals("") && Context.hasPrivilege(PrivilegeConstants.EDIT_USER_PASSWORDS)) {					if (log.isDebugEnabled()) {						log.debug("calling changePassword for user " + user + " by user " + Context.getAuthenticatedUser());					}					us.changePassword(user, password);				}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/LoginCredential.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAddress.java">
{@Element(required = true)	public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/observation/PersonObsFormController.java">
{public Person getPerson() {			return person;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/RelationshipPatientFilter.java">
{public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/module/web/FormEntryContext.java">
{public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonName.java">
{@Element(required = true)	public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/activelist/ActiveListItem.java">
{public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static UserService getUserService() {		return getServiceContext().getUserService();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public Person getPerson() {		return this;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Provider.java">
{public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Obs.java">
{public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/FormatTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAttribute.java">
{@Element(required = true)	public Person getPerson() {		return person;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/UserListItem.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public UserService getUserService() {		return getService(UserService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{@Attribute(required = true)	public Integer getUserId() {		return userId;	}}
