public Userauthenticate(String username, String password, ContextDAO contextDAO)throws ContextAuthenticationException{		if (log.isDebugEnabled()) {			log.debug("Authenticating with username: " + username);		}				this.user = contextDAO.authenticate(username, password);		setUserLocation();		if (log.isDebugEnabled()) {			log.debug("Authenticated as: " + this.user);		}				return this.user;	}public UserbecomeUser(String systemId)throws ContextAuthenticationException{		if (!Context.getAuthenticatedUser().isSuperUser()) {			throw new APIAuthenticationException("You must be a superuser to assume another user's identity");		}				if (log.isDebugEnabled()) {			log.debug("Turning the authenticated user into user with systemId: " + systemId);		}				User userToBecome = Context.getUserService().getUserByUsername(systemId);				if (userToBecome == null) {			throw new ContextAuthenticationException("User not found with systemId: " + systemId);		}				// hydrate the user object		if (userToBecome.getAllRoles() != null) {			userToBecome.getAllRoles().size();		}		if (userToBecome.getUserProperties() != null) {			userToBecome.getUserProperties().size();		}		if (userToBecome.getPrivileges() != null) {			userToBecome.getPrivileges().size();		}				this.user = userToBecome;		//update the user's location		setUserLocation();				if (log.isDebugEnabled()) {			log.debug("Becoming user: " + user);		}				return userToBecome;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public UserService getUserService() {		return getService(UserService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/UserContext.java">
{public User getAuthenticatedUser() {		return user;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public boolean isSuperUser() {		return containsRole(RoleConstants.SUPERUSER);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static UserService getUserService() {		return getServiceContext().getUserService();	}}
