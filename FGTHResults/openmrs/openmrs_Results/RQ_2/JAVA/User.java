public User(Person person){		this.person = person;	}public booleanhasRole(String r, boolean ignoreSuperUser){		if (ignoreSuperUser == false) {			if (isSuperUser()) {				return true;			}		}				if (roles == null) {			return false;		}				Set<Role> tmproles = getAllRoles();				if (log.isDebugEnabled()) {			log.debug("User #" + userId + " has roles: " + tmproles);		}				return containsRole(r);	}public voidsetSystemId(String systemId){		this.systemId = systemId;	}public voidsetPersonId(Integer personId){		throw new APIException("You need to call setPerson(Person)");	}person=newPerson()return person;public voidsetPerson(Person person){		this.person = person;	}public voidsetUsername(String username){		this.username = username;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/UserDAO.java">
{public List<Role> getAllRoles() throws DAOException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/hibernate/HibernateUserDAO.java">
{@SuppressWarnings("unchecked")	public List<Role> getAllRoles() throws DAOException {		return sessionFactory.getCurrentSession().createQuery("from Role r order by r.role").list();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/UserService.java">
{public List<Role> getAllRoles() throws APIException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public Set<Role> getAllRoles() {		// the user's immediate roles		Set<Role> baseRoles = new HashSet<Role>();				// the user's complete list of roles including		// the parent roles of their immediate roles		Set<Role> totalRoles = new HashSet<Role>();		if (getRoles() != null) {			baseRoles.addAll(getRoles());			totalRoles.addAll(getRoles());		}				if (log.isDebugEnabled()) {			log.debug("User's base roles: " + baseRoles);		}				try {			for (Role r : baseRoles) {				totalRoles.addAll(r.getAllParentRoles());			}		}		catch (ClassCastException e) {			log.error("Error converting roles for user: " + this);			log.error("baseRoles.class: " + baseRoles.getClass().getName());			log.error("baseRoles: " + baseRoles.toString());			Iterator<Role> iter = baseRoles.iterator();			while (iter.hasNext()) {				log.error("baseRole: '" + iter.next() + "'");			}		}		return totalRoles;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/UserContext.java">
{public Set<Role> getAllRoles() throws Exception {		return getAllRoles(getAuthenticatedUser());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/impl/UserServiceImpl.java">
{@Transactional(readOnly = true)	public List<Role> getAllRoles() throws APIException {		return dao.getAllRoles();	}}
