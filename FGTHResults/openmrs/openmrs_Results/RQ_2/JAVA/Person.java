public Person(Person person){		if (person == null) {			return;		}				personId = person.getPersonId();		setUuid(person.getUuid());		addresses = person.getAddresses();		names = person.getNames();		attributes = person.getAttributes();				gender = person.getGender();		birthdate = person.getBirthdate();		birthtime = person.getBirthDateTime();		birthdateEstimated = person.getBirthdateEstimated();		deathdateEstimated = person.getDeathdateEstimated();		dead = person.isDead();		deathDate = person.getDeathDate();		causeOfDeath = person.getCauseOfDeath();				// base creator/voidedBy/changedBy info is not copied here		// because that is specific to and will be recreated		// by the subobject upon save				setPersonCreator(person.getPersonCreator());		setPersonDateCreated(person.getPersonDateCreated());		setPersonChangedBy(person.getPersonChangedBy());		setPersonDateChanged(person.getPersonDateChanged());		setPersonVoided(person.isPersonVoided());		setPersonVoidedBy(person.getPersonVoidedBy());		setPersonDateVoided(person.getPersonDateVoided());		setPersonVoidReason(person.getPersonVoidReason());	}public Person(Integer personId){		this.personId = personId;	}public voidsetPersonId(Integer personId){		this.personId = personId;	}public List<PersonAttribute>getAttributes(PersonAttributeType personAttributeType){		List<PersonAttribute> ret = new Vector<PersonAttribute>();		for (PersonAttribute attribute : getAttributes()) {			if (personAttributeType.equals(attribute.getAttributeType()) && !attribute.isVoided()) {				ret.add(attribute);			}		}		return ret;	}PersonNamepersonName= getPersonName()if (personName == null) {			return "";		} else {			return personName.getGivenName();		}PersonNamepersonName= getPersonName()if (personName == null) {			return "";		} else {			return personName.getMiddleName();		}PersonNamepersonName= getPersonName()if (personName == null) {			return "";		} else {			return personName.getFamilyName();		}this.personChangedBy=changedBythis.personDateChanged=dateChangedthis.personCreator=creatorthis.personDateCreated=dateCreatedthis.personDateVoided=dateVoidedthis.personVoidedBy=voidedBythis.personVoidReason=voidReason
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/customdatatype/Customizable.java">
{Collection<A> getAttributes();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Obs.java">
{public Integer getPersonId() {		return personId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/util/databasechange/Database1_9_7UpgradeIT.java">
{Integer getPersonId() {			return personId;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PersonListItem.java">
{public Boolean getDeathdateEstimated() {		return deathdateEstimated;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/FormatTag.java">
{public Integer getPersonId() {		return personId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/PatientCharacteristicFilter.java">
{public String getGender() {		return gender;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Concept.java">
{@ElementList	public Collection<ConceptName> getNames() {		return getNames(false);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseCustomizableMetadata.java">
{@Override	public Set<A> getAttributes() {		return attributes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public Integer getPersonId() {		return personId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public Set<PersonName> getNames() {		return person.getNames();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseCustomizableData.java">
{@Override	public Set<A> getAttributes() {		return attributes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{@Element(required = false)	public Concept getCauseOfDeath() {		return this.causeOfDeath;	}}
