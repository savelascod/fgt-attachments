public UsercreateUser(User user, String password)throws APIException{		return Context.getUserService().saveUser(user, password);	}public UsersaveUser(User user, String password)throws APIException{		if (user.getUserId() == null) {			Context.requirePrivilege(PrivilegeConstants.ADD_USERS);		} else {			Context.requirePrivilege(PrivilegeConstants.EDIT_USERS);		}				checkPrivileges(user);				// if we're creating a user and a password wasn't supplied, throw an		// error		if (user.getUserId() == null && (password == null || password.length() < 1)) {			throw new APIException("A password is required when creating a user");		}				if (hasDuplicateUsername(user)) {			throw new DAOException("Username " + user.getUsername() + " or system id " + user.getSystemId()			        + " is already in use.");		}				// TODO Check required fields for user!!				if (user.getUserId() == null && password != null) {			OpenmrsUtil.validatePassword(user.getUsername(), password, user.getSystemId());		}				return dao.saveUser(user, password);	}public UsergetUserByUsername(String username)throws APIException{		return dao.getUserByUsername(username);	}public voidchangeHashedPassword(User user, String hashedPassword, String salt)throws APIException{		dao.changeHashedPassword(user, hashedPassword, salt);	}public List<User>findUsers(String givenName, String familyName, boolean includeVoided){		return Context.getUserService().getUsersByName(givenName, familyName, includeVoided);	}public List<User>getUsersByName(String givenName, String familyName, boolean includeVoided)throws APIException{		return dao.getUsersByName(givenName, familyName, includeVoided);	}public List<User>getUsersByPerson(Person person, boolean includeRetired)throws APIException{		return dao.getUsersByPerson(person, includeRetired);	}public UsersetUserProperty(User user, String key, String value){		if (user != null) {			if (!Context.hasPrivilege(PrivilegeConstants.EDIT_USERS) && !user.equals(Context.getAuthenticatedUser())) {				throw new APIException("You are not authorized to change " + user.getUserId() + "'s properties");			}						user.setUserProperty(key, value);			try {				Context.addProxyPrivilege(PrivilegeConstants.EDIT_USERS);				Context.getUserService().saveUser(user, null);			}			finally {				Context.removeProxyPrivilege(PrivilegeConstants.EDIT_USERS);			}		}				return user;	}public UserremoveUserProperty(User user, String key){		if (user != null) {						// if the current user isn't allowed to edit users and			// the user being edited is not the current user, throw an			// exception			if (!Context.hasPrivilege(PrivilegeConstants.EDIT_USERS) && !user.equals(Context.getAuthenticatedUser())) {				throw new APIException("You are not authorized to change " + user.getUserId() + "'s properties");			}						user.removeUserProperty(key);						try {				Context.addProxyPrivilege(PrivilegeConstants.EDIT_USERS);				Context.getUserService().saveUser(user, null);			}			finally {				Context.removeProxyPrivilege(PrivilegeConstants.EDIT_USERS);			}		}				return user;	}StringsystemIdsystemId=generatedId.toString()systemId=liv.getValidIdentifier(systemId)return systemId;(dao.hasDuplicateUsername(null, systemId, null))return systemId;public voidnotifyPrivilegeListeners(User user, String privilege, boolean hasPrivilege){		if (privilegeListeners != null) {			for (PrivilegeListener privilegeListener : privilegeListeners) {				try {					privilegeListener.privilegeChecked(user, privilege, hasPrivilege);				}				catch (Exception e) {					log.error("Privilege listener has failed", e);				}			}		}	}public UsersaveUserProperty(String key, String value){		User user = Context.getAuthenticatedUser();		if (user == null) {			throw new APIException("No Authenticated user found");		}		user.setUserProperty(key, value);		return dao.saveUser(user, null);	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/LoginCredential.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/FormatTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/UserListItem.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public UserService getUserService() {		return getService(UserService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{@Attribute(required = true)	public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public Integer getUserId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static UserService getUserService() {		return getServiceContext().getUserService();	}}
