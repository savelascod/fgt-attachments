UserevictedUser= Context.getAuthenticatedUser()Context.evictFromSession(evictedUser)UserfetchedUser= Context.getUserService().getUser(evictedUser.getUserId())fetchedUser.getPersonName().setGivenName("new username")Context.getUserService().saveUser(fetchedUser, null)Assert.assertNotSame(Context.getAuthenticatedUser().getGivenName(), fetchedUser.getGivenName())EncounterVisitHandlerregisteredComponent= Context.getRegisteredComponent("existingOrNewVisitAssignmentHandler",		    EncounterVisitHandler.class)Assert.assertTrue(registeredComponent instanceof ExistingOrNewVisitAssignmentHandler)
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PersonListItem.java">
{public String getPersonName() {		String name = "";				if (!StringUtils.isBlank(givenName)) {			name = givenName;		}				if (!StringUtils.isBlank(middleName)) {			name = name + (name.length() > 0 ? " " : "") + middleName;		}				if (!StringUtils.isBlank(familyName)) {			name = name + (name.length() > 0 ? " " : "") + familyName;		}				return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{public PersonName getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public UserService getUserService() {		return getService(UserService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/VisitListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/ObsListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EncounterListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/UserContext.java">
{public User getAuthenticatedUser() {		return user;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public PersonName getPersonName() {		return getPerson() == null ? null : getPerson().getPersonName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/OptionsForm.java">
{public PersonName getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static UserService getUserService() {		return getServiceContext().getUserService();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{public PersonName getPersonName() {		// normally the DAO layer returns these in the correct order, i.e. preferred and non-voided first, but it's possible that someone		// has fetched a Person, changed their names around, and then calls this method, so we have to be careful.		if (getNames() != null && getNames().size() > 0) {			for (PersonName name : getNames()) {				if (name.isPreferred() && !name.isVoided()) {					return name;				}			}			for (PersonName name : getNames()) {				if (!name.isVoided()) {					return name;				}			}						if (isVoided() && !getNames().isEmpty()) {				return getNames().iterator().next();			}		}		return null;	}}
