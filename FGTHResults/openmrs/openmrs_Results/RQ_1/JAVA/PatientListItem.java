public PatientListItem(Patient patient){		this(patient, null);	}public PatientListItem(Patient patient, String searchName){		super(patient, searchName);				if (patient != null) {						patientId = patient.getPatientId();						// get patient's identifiers			boolean first = true;			for (PatientIdentifier pi : patient.getIdentifiers()) {				if (first) {					identifier = pi.getIdentifier();					identifierCheckDigit = pi.getIdentifierType().hasCheckDigit();					first = false;				} else {					if (!"".equals(otherIdentifiers)) {						otherIdentifiers += ",";					}					otherIdentifiers += " " + pi.getIdentifier();				}			}					}	}this.identifier=identifierpublic voidsetOtherIdentifiers(String otherIdentifiers){		this.otherIdentifiers = otherIdentifiers;	}public voidsetPatientId(Integer patientId){		this.patientId = patientId;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public Integer getPatientId() {		return this.patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PatientWidgetTag.java">
{public Integer getPatientId() {		return patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/export/DataExportFunctions.java">
{public Integer getPatientId() {		return patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PatientListItem.java">
{public Integer getPatientId() {		return patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/ShowGraphTag.java">
{public Integer getPatientId() {		return this.patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PatientProgramItem.java">
{public Integer getPatientId() {		return patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public Integer getPatientId() {		return patientId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Encounter.java">
{@Deprecated	public Integer getPatientId() {		return patientId;	}}
