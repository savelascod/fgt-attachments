ShortPatientModelshortPatientModel= (ShortPatientModel) objPersonNamepersonName= shortPatientModel.getPersonName()for (PersonName possibleDuplicate : shortPatientModel.getPatient().getNames()) {			//don't compare the name to itself			if (OpenmrsUtil.nullSafeEquals(possibleDuplicate.getId(), personName.getId())) {				continue;			}						if (OpenmrsUtil.nullSafeEqualsIgnoreCase(possibleDuplicate.getGivenName(), personName.getGivenName())			        && OpenmrsUtil.nullSafeEqualsIgnoreCase(possibleDuplicate.getMiddleName(), personName.getMiddleName())			        && OpenmrsUtil.nullSafeEqualsIgnoreCase(possibleDuplicate.getFamilyName(), personName.getFamilyName())) {				errors.reject("Patient.duplicateName", new Object[] { personName.getFullName() }, personName.getFullName()				        + " is a duplicate name for the same patient");			}		}PersonAddresspersonAddress= shortPatientModel.getPersonAddress()for (PersonAddress possibleDuplicate : shortPatientModel.getPatient().getAddresses()) {			//don't compare the address to itself			if (OpenmrsUtil.nullSafeEquals(possibleDuplicate.getId(), personAddress.getId())) {				continue;			}						if (!possibleDuplicate.isBlank() && !personAddress.isBlank()			        && possibleDuplicate.toString().equalsIgnoreCase(personAddress.toString())) {				errors.reject("Patient.duplicateAddress", new Object[] { personAddress.toString() }, personAddress				        .toString()				        + " is a duplicate address for the same patient");			}		}if (CollectionUtils.isEmpty(shortPatientModel.getIdentifiers())) {			errors.reject("PatientIdentifier.error.insufficientIdentifiers");		} else {			boolean nonVoidedIdentifierFound = false;			for (PatientIdentifier pId : shortPatientModel.getIdentifiers()) {				//no need to validate unsaved identifiers that have been removed				if (pId.getPatientIdentifierId() == null && pId.isVoided()) {					continue;				}								if (!pId.isVoided()) {					nonVoidedIdentifierFound = true;				}								new PatientIdentifierValidator().validate(pId, errors);			}			// if all the names are voided			if (!nonVoidedIdentifierFound) {				errors.reject("PatientIdentifier.error.insufficientIdentifiers");			}					}if (StringUtils.isBlank(shortPatientModel.getPatient().getGender())) {			errors.rejectValue("patient.gender", "Person.gender.required");		}if (shortPatientModel.getPatient().getBirthdate() != null) {			if (shortPatientModel.getPatient().getBirthdate().after(new Date())) {				errors.rejectValue("patient.birthdate", "error.date.future");			} else {				Calendar c = Calendar.getInstance();				c.setTime(new Date());				c.add(Calendar.YEAR, -120); // patient cannot be older than 120				// years old				if (shortPatientModel.getPatient().getBirthdate().before(c.getTime())) {					errors.rejectValue("patient.birthdate", "error.date.nonsensical");				}			}		} else {			errors.rejectValue("patient.birthdate", "error.required", new Object[] { Context.getMessageSourceService()			        .getMessage("Person.birthdate") }, "");		}if (shortPatientModel.getPersonAddress() != null) {			try {				errors.pushNestedPath("personAddress");				ValidationUtils.invokeValidator(new PersonAddressValidator(), shortPatientModel.getPersonAddress(), errors);			}			finally {				errors.popNestedPath();			}		}if (shortPatientModel.getPatient().getDead()) {			if (shortPatientModel.getPatient().getCauseOfDeath() == null) {				errors.rejectValue("patient.causeOfDeath", "Person.dead.causeOfDeathNull");			}						if (shortPatientModel.getPatient().getDeathDate() != null) {				if (shortPatientModel.getPatient().getDeathDate().after(new Date())) {					errors.rejectValue("patient.deathDate", "error.date.future");				}				// death date has to be after birthdate if both are specified				if (shortPatientModel.getPatient().getBirthdate() != null				        && shortPatientModel.getPatient().getDeathDate().before(				            shortPatientModel.getPatient().getBirthdate())) {					errors.rejectValue("patient.deathDate", "error.deathdate.before.birthdate");				}			}		}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Order.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/VisitListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientIdentifier.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Concept.java">
{@ElementList	public Collection<ConceptName> getNames() {		return getNames(false);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/ObsListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/export/DataExportFunctions.java">
{public Patient getPatient() {		if (patient == null) {			patient = patientService.getPatient(patientId);		}				return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EncounterListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderGroup.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{@ElementList	public Set<PersonName> getNames() {		if (names == null) {			names = new TreeSet<PersonName>();		}		return this.names;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Encounter.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientProgram.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Visit.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PersonListItem.java">
{public String getPersonName() {		String name = "";				if (!StringUtils.isBlank(givenName)) {			name = givenName;		}				if (!StringUtils.isBlank(middleName)) {			name = name + (name.length() > 0 ? " " : "") + middleName;		}				if (!StringUtils.isBlank(familyName)) {			name = name + (name.length() > 0 ? " " : "") + familyName;		}				return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/Note.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Obs.java">
{public Patient getPatient() {		return (Patient) getPerson();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/arden/ArdenValue.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public Set<PersonName> getNames() {		return person.getNames();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/OptionsForm.java">
{public PersonName getPersonName() {		return personName;	}}
