Patientpatient= (Patient) objBooleanpreferredIdentifierChosen= falseCollection<PatientIdentifier>identifiers= patient.isVoided() ? patient.getIdentifiers() : patient		        .getActiveIdentifiers()for (PatientIdentifier pi : identifiers) {			if (pi.isPreferred()) {				preferredIdentifierChosen = true;			}		}if (!preferredIdentifierChosen && identifiers.size() != 1) {			errors.reject("error.preferredIdentifier");		}if (patient.getIdentifiers() != null) {				for (PatientIdentifier identifier : patient.getIdentifiers()) {					patientIdentifierValidator.validate(identifier, errors);				}			}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAddress.java">
{public Boolean isPreferred() {		if (preferred == null) {			return new Boolean(false);		}		return preferred;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{public List<PatientIdentifier> getIdentifiers() {		return identifiers;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/VisitListItem.java">
{public boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientIdentifier.java">
{public Boolean isPreferred() {		return preferred;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Voidable.java">
{public Boolean isVoided();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseOpenmrsData.java">
{public Boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonName.java">
{public Boolean isPreferred() {		if (preferred == null) {			return Boolean.FALSE;		}		return preferred;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EncounterListItem.java">
{public boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/RelationshipType.java">
{public Boolean isPreferred() {		return preferred;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Cohort.java">
{public int size() {		return getMemberIds() == null ? 0 : getMemberIds().size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public Set<PatientIdentifier> getIdentifiers() {		if (identifiers == null) {			identifiers = new LinkedHashSet<PatientIdentifier>();		}		return this.identifiers;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/cohort/CohortSearchHistory.java">
{public int size() {		return searchHistory.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptName.java">
{public Boolean isPreferred() {		return isLocalePreferred();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/messagesource/PresentationMessageMap.java">
{public int size() {		return internalMap.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptNameTag.java">
{public Boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptSource.java">
{@Deprecated	public Boolean isVoided() {		return isRetired();	}}
