patientService=Context.getPatientService()private voidvoidOrders(Collection<Patient> patientsWithOrders){		OrderService os = Context.getOrderService();		for (Patient p : patientsWithOrders) {			List<Order> orders = os.getAllOrdersByPatient(p);			for (Order o : orders) {				o.setVoided(true);			}		}	}Patientpatient= new Patient()patient.addName(pName)Set<PersonAddress>pAddressList= patient.getAddresses()patient.setAddresses(pAddressList)patient.addAddress(pAddress)patient.setDeathDate(new Date())patient.setBirthdate(new Date())patient.setBirthdateEstimated(true)patient.setGender("male")patient.setDeathdateEstimated(true)return patient;Patientpatient= new Patient()patient.addName(pName)Set<PersonAddress>pAddressList= patient.getAddresses()patient.setAddresses(pAddressList)patient.addAddress(pAddress)patient.setDeathDate(new Date())patient.setBirthdateEstimated(true)patient.setBirthdate(new Date())patient.setBirthdateEstimated(true)patient.setGender("male")List<PatientIdentifierType>patientIdTypes= patientService.getAllPatientIdentifierTypes()assertNotNull(patientIdTypes)PatientIdentifierpatientIdentifier= new PatientIdentifier()patientIdentifier.setIdentifier("123-0")patientIdentifier.setIdentifierType(patientIdTypes.get(0))patientIdentifier.setLocation(new Location(1))patientIdentifier.setPreferred(true)Set<PatientIdentifier>patientIdentifiers= new LinkedHashSet<PatientIdentifier>()patientIdentifiers.add(patientIdentifier)patient.setIdentifiers(patientIdentifiers)patientService.savePatient(patient)PatientcreatedPatient= patientService.getPatient(patient.getPatientId())assertNotNull(createdPatient)assertNotNull(createdPatient.getPatientId())PatientcreatedPatientById= patientService.getPatient(createdPatient.getPatientId())assertNotNull(createdPatientById)Patientpatient= createBasicPatient()Patientpatient2= createBasicPatient()patient.addIdentifier(ident1)patientService.savePatient(patient)patient.removeIdentifier(ident1)patient.addIdentifier(ident2)patientService.savePatient(patient)patient.removeIdentifier(ident2)patient.addIdentifier(ident3)patientService.savePatient(patient)patientService.purgePatient(patient)patient.removeIdentifier(ident3)patient2.addIdentifier(ident4)patientService.savePatient(patient2)Collection<Patient>johnPatients= patientService.getPatients("John", null, null, false)assertNotNull("There should be a patient named 'John'", johnPatients)assertFalse("There should be a patient named 'John'", johnPatients.isEmpty())PatientfirstJohnPatient= johnPatients.iterator().next()Stringidentifier= firstJohnPatient.getPatientIdentifier().getIdentifier()List<Patient>patients= patientService.getPatients(null, identifier, null, false)assertTrue("Odd. The firstJohnPatient isn't in the list of patients for this identifier", patients		        .contains(firstJohnPatient))Collection<Patient>patients= patientService.getPatients("John", null, null, false)assertTrue("The patient list size should be restricted to under the max (1000). its " + patients.size(), patients		        .size() == 1000)List<Patient>patients= patientService.getPatients(null, "1234", types, false)assertEquals(1, patients.size())patients=patientService.getPatients(null, "1234", null, false)assertEquals(1, patients.size())patients=patientService.getPatients(null, null, types, false)assertEquals(2, patients.size())patients=patientService.getPatients(null, "00000001234", null, false)assertEquals(1, patients.size())PatientIdentifierTypepatientIdentifierType= new PatientIdentifierType()patientIdentifierType.setName("testing")patientIdentifierType.setDescription("desc")patientIdentifierType.setRequired(false)patientService.savePatientIdentifierType(patientIdentifierType)PatientIdentifierTypetype= patientService.getPatientIdentifierType(patientIdentifierType.getId())assertNull(patientService.getPatientIdentifierType(patientIdentifierType.getId()))PatientIdentifierTypepatientIdentifierType= new PatientIdentifierType()patientIdentifierType.setName("testing")patientIdentifierType.setDescription("desc")patientIdentifierType.setRequired(false)patientService.savePatientIdentifierType(patientIdentifierType)PatientIdentifierTypenewPatientIdentifierType= patientService.getPatientIdentifierType(patientIdentifierType		        .getPatientIdentifierTypeId())assertNotNull(newPatientIdentifierType)PatientIdentifierTypenewerPatientIdentifierType= patientService.getPatientIdentifierType(1)assertEquals("SOME NEW NAME", newerPatientIdentifierType.getName())Patientpatient2= patientService.getPatient(2)assertTrue("When getting a patient, it should be of the class patient, not: " + patient2.getClass(), patient2		        .getClass().equals(Patient.class))Patientpatient3= patientService.getPatient(3)assertTrue("When getting a patient, it should be of the class patient, not: " + patient3.getClass(), patient3		        .getClass().equals(Patient.class))PatientoldPatient= patientService.getPatient(501)Assert.assertNull(oldPatient)Patientpatient= new Patient(existingPerson)PatientIdentifierpatientIdentifier= new PatientIdentifier("some identifier", new PatientIdentifierType(2),		        new Location(1))patientIdentifier.setPreferred(true)patient.addIdentifier(patientIdentifier)patientService.savePatient(patient)Assert.assertEquals(501, patient.getPatientId().intValue())List<Patient>patients= patientService.getPatients("Johnson", null, null, false)Assert.assertEquals(3, patients.size())Assert.assertTrue(TestUtil.containsId(patients, 2))Assert.assertTrue(TestUtil.containsId(patients, 4))Assert.assertTrue(TestUtil.containsId(patients, 5))Patientpatient= new Patient()patient.setGender("M")patient.setPatientId(2)patient.addName(new PersonName("This", "Isa", "Test"))PatientIdentifierpatientIdentifier= new PatientIdentifier("101-6", new PatientIdentifierType(1), new Location(1))patientIdentifier.setPreferred(true)patient.addIdentifier(patientIdentifier)patientService.savePatient(patient)Patientpatient= Context.getPatientService().getPatient(2)patient.getNames().size()Patientpatient= Context.getPatientService().getPatient(2)patient.getAddresses().size()Patientpatient= Context.getPatientService().getPatient(2)patient.getAttributes().size()Patientpatient= new Patient()patient.setGender("M")patient.setPatientId(2)patient.addName(new PersonName("This", "Isa", "Test"))PatientIdentifierpatientIdentifier= new PatientIdentifier("101-6", new PatientIdentifierType(1), new Location(1))patientIdentifier.setPreferred(true)patient.addIdentifier(patientIdentifier)Context.getPatientService().savePatient(patient)PatientIdentifierpatientIdentifier= new PatientIdentifier("ABC123", pit, null)Assert.assertFalse(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))booleanfound= falsefor (PatientIdentifier id : p.getIdentifiers()) {				if (id.getIdentifier().equals("XYZ") && id.getIdentifierType().getId() == 2) {					found = true;					break;				}			}Assert.assertTrue(found)PatientIdentifierpatientIdentifier= new PatientIdentifier("XYZ", pit, null)Assert.assertFalse(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))PatientIdentifierpatientIdentifier= new PatientIdentifier("Nobody could possibly have this identifier", pit, null)patientIdentifier.setPatient(patientService.getPatient(2))Assert.assertFalse(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))PatientIdentifierpatientIdentifier= new PatientIdentifier("Nobody could possibly have this identifier", pit, null)Assert.assertFalse(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))PatientIdentifierpatientIdentifier= new PatientIdentifier("7TU-8", pit, null)patientIdentifier.setPatient(patientService.getPatient(2))Assert.assertTrue(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))PatientIdentifierpatientIdentifier= new PatientIdentifier("7TU-8", pit, null)Assert.assertTrue(patientService.isIdentifierInUseByAnotherPatient(patientIdentifier))Patientpatient= new Patient()PatientIdentifierpatientIdentifier= new PatientIdentifier()patientIdentifier.setIdentifierType(Context.getPatientService().getAllPatientIdentifierTypes(false).get(0))patientIdentifier.setLocation(new Location(1))patientIdentifier.setVoided(true)patientIdentifier.setVoidedBy(Context.getAuthenticatedUser())patientIdentifier.setVoidReason("Testing whether voided identifiers are ignored")patient.addIdentifier(patientIdentifier)patientIdentifier=newPatientIdentifier()patientIdentifier.setIdentifier("a non empty string")patientIdentifier.setIdentifierType(Context.getPatientService().getAllPatientIdentifierTypes(false).get(0))patientIdentifier.setLocation(new Location(1))patientIdentifier.setVoided(false)patientIdentifier.setVoidedBy(Context.getAuthenticatedUser())patientIdentifier.setVoidReason("Testing whether voided identifiers are ignored")patient.addIdentifier(patientIdentifier)Context.getPatientService().checkPatientIdentifiers(patient)Patientpatient= new Patient()PatientIdentifierpatientIdentifier= new PatientIdentifier()patientIdentifier.setIdentifierType(Context.getPatientService().getAllPatientIdentifierTypes(false).get(0))patientIdentifier.setVoided(true)patientIdentifier.setVoidedBy(Context.getAuthenticatedUser())patientIdentifier.setVoidReason("Testing whether voided identifiers are ignored")patient.addIdentifier(patientIdentifier)Context.getPatientService().checkPatientIdentifiers(patient)Patientpatient= new Patient()PatientIdentifierpatientIdentifier= new PatientIdentifier()patientIdentifier.setIdentifierType(Context.getPatientService().getAllPatientIdentifierTypes(false).get(0))patient.addIdentifier(patientIdentifier)Context.getPatientService().checkPatientIdentifiers(patient)Patientpatient= new Patient()patient.setIdentifiers(null)Context.getPatientService().checkPatientIdentifiers(patient)Patientpatient= new Patient()patient.setIdentifiers(new HashSet<PatientIdentifier>())Context.getPatientService().checkPatientIdentifiers(patient)PatientIdentifierTypepatientIdentifierType= Context.getPatientService().getAllPatientIdentifierTypes(false).get(0)Patientpatient= new Patient()PatientIdentifierpatientIdentifier1= new PatientIdentifier()patientIdentifier1.setIdentifier("123456789")patientIdentifier1.setDateCreated(new Date())patientIdentifier1.setIdentifierType(patientIdentifierType)patient.addIdentifier(patientIdentifier1)PatientIdentifierpatientIdentifier2= new PatientIdentifier()patientIdentifier2.setIdentifier("123456789")patientIdentifier2.setIdentifierType(patientIdentifierType)patientIdentifier2.setDateCreated(new Date())patient.addIdentifier(patientIdentifier2)Context.getPatientService().checkPatientIdentifiers(patient)PatientIdentifierTypepatientIdentifierType= Context.getPatientService().getAllPatientIdentifierTypes(false).get(0)log.info(patientIdentifierType.getRequired())Patientpatient= Context.getPatientService().getPatient(2)Assert.assertNotNull(patient)Assert.assertTrue(patient.getClass().isAssignableFrom(Patient.class))Patientpatient= Context.getPatientService().getPatient(10000)Assert.assertNull(patient)PatientexamplePatient= Context.getPatientService().getPatient(6)examplePatient.setId(2)Patientpatient= Context.getPatientService().getPatientByExample(examplePatient)Assert.assertNotNull(patient)Assert.assertTrue(patient.getClass().isAssignableFrom(Patient.class))Assert.assertEquals(new Integer(2), patient.getPatientId())PatientexamplePatient= Context.getPatientService().getPatient(6)examplePatient.setId(null)Patientpatient= Context.getPatientService().getPatientByExample(examplePatient)Assert.assertNull(patient)PatientexamplePatient= Context.getPatientService().getPatient(6)examplePatient.setId(3)Patientpatient= Context.getPatientService().getPatientByExample(examplePatient)Assert.assertNull(patient)List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(		    "Test OpenMRS Identification Number", "java.lang.Integer", null, null)Assert.assertEquals(false, patientIdentifierTypes.isEmpty())for (PatientIdentifierType patientIdentifierType : patientIdentifierTypes) {			Assert.assertEquals("Test OpenMRS Identification Number", patientIdentifierType.getName());			Assert.assertEquals("java.lang.Integer", patientIdentifierType.getFormat());		}List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, true, null)Assert.assertTrue(!patientIdentifierTypes.isEmpty())Assert.assertEquals(1, patientIdentifierTypes.size())for (PatientIdentifierType patientIdentifierType : patientIdentifierTypes) {			Assert.assertTrue(patientIdentifierType.getRequired());		}List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, false, null)Assert.assertTrue(!patientIdentifierTypes.isEmpty())for (PatientIdentifierType patientIdentifierType : patientIdentifierTypes) {			Assert.assertFalse(patientIdentifierType.getRequired());		}List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, null, null)Assert.assertTrue(!patientIdentifierTypes.isEmpty())Assert.assertEquals(5, patientIdentifierTypes.size())List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, null, true)Assert.assertTrue(!patientIdentifierTypes.isEmpty())for (PatientIdentifierType patientIdentifierType : patientIdentifierTypes) {			Assert.assertTrue(patientIdentifierType.hasCheckDigit());		}List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, null, false)Assert.assertTrue(!patientIdentifierTypes.isEmpty())for (PatientIdentifierType patientIdentifierType : patientIdentifierTypes) {			Assert.assertFalse(patientIdentifierType.hasCheckDigit());		}List<PatientIdentifierType>patientIdentifierTypes= Context.getPatientService().getPatientIdentifierTypes(null,		    null, null, null)Assert.assertTrue(!patientIdentifierTypes.isEmpty())Assert.assertEquals(5, patientIdentifierTypes.size())PatientIdentifierTypesavedIdentifierType= patientService.getPatientIdentifierType(identifierType		        .getPatientIdentifierTypeId())assertNotNull(savedIdentifierType)PatientIdentifierTypesavedIdentifierType= patientService.getPatientIdentifierType(2)assertNotNull(savedIdentifierType)assertTrue(savedIdentifierType.equals(identifierType))PatientIdentifierTypeunretiredIdentifierType= Context.getPatientService().unretirePatientIdentifierType(		    identifierType)Assert.assertFalse(unretiredIdentifierType.isRetired())Assert.assertNull(unretiredIdentifierType.getRetiredBy())Assert.assertNull(unretiredIdentifierType.getRetireReason())Assert.assertNull(unretiredIdentifierType.getDateRetired())PatientIdentifierTypeunretiredIdentifierType= Context.getPatientService().unretirePatientIdentifierType(		    identifierType)Assert.assertFalse(unretiredIdentifierType.isRetired())Assert.assertNull(unretiredIdentifierType.getRetiredBy())Assert.assertNull(unretiredIdentifierType.getRetireReason())Assert.assertNull(unretiredIdentifierType.getDateRetired())Patientpatient= Context.getPatientService().getPatient(2)PatientvoidedPatient= Context.getPatientService().voidPatient(patient, "Void for testing")Assert.assertTrue(voidedPatient.isVoided())Assert.assertNotNull(voidedPatient.getVoidedBy())Assert.assertNotNull(voidedPatient.getVoidReason())Assert.assertNotNull(voidedPatient.getDateVoided())PatientunvoidedPatient= Context.getPatientService().unvoidPatient(voidedPatient)Assert.assertFalse(unvoidedPatient.isVoided())Assert.assertNull(unvoidedPatient.getVoidedBy())Assert.assertNull(unvoidedPatient.getVoidReason())Assert.assertNull(unvoidedPatient.getDateVoided())Patientpatient= Context.getPatientService().getPatient(2)PatientvoidedPatient= Context.getPatientService().voidPatient(patient, "Void for testing")Assert.assertTrue(voidedPatient.isVoided())Assert.assertNotNull(voidedPatient.getVoidedBy())Assert.assertNotNull(voidedPatient.getVoidReason())Assert.assertNotNull(voidedPatient.getDateVoided())PatientunvoidedPatient= Context.getPatientService().unvoidPatient(voidedPatient)Assert.assertFalse(unvoidedPatient.isVoided())Assert.assertNull(unvoidedPatient.getVoidedBy())Assert.assertNull(unvoidedPatient.getVoidReason())Assert.assertNull(unvoidedPatient.getDateVoided())Patientpatient= Context.getPatientService().getPatient(2)PatientvoidedPatient= Context.getPatientService().voidPatient(patient, "Void for testing")Assert.assertTrue(voidedPatient.isVoided())Assert.assertEquals("Void for testing", voidedPatient.getVoidReason())Patientpatient= Context.getPatientService().getPatient(2)PatientvoidedPatient= Context.getPatientService().voidPatient(patient, "Void for testing")for (PatientIdentifier patientIdentifier : voidedPatient.getIdentifiers()) {			Assert.assertTrue(patientIdentifier.isVoided());			Assert.assertNotNull(patientIdentifier.getVoidedBy());			Assert.assertNotNull(patientIdentifier.getVoidReason());			Assert.assertNotNull(patientIdentifier.getDateVoided());		}Patientpatient= Context.getPatientService().getPatient(2)PatientvoidedPatient= Context.getPatientService().voidPatient(patient, "Void for testing")Assert.assertTrue(voidedPatient.isVoided())Assert.assertNotNull(voidedPatient.getVoidedBy())Assert.assertNotNull(voidedPatient.getVoidReason())Assert.assertNotNull(voidedPatient.getDateVoided())Assert.assertEquals("Void for testing", voidedPatient.getVoidReason())PatientServicepatientService= Context.getPatientService()PatientvoidedPatient= patientService.voidPatient(null, "No null patient should be voided")Assert.assertNull(voidedPatient)Patientpatient= Context.getPatientService().getPatientByUuid(uuid)Assert.assertEquals(2, (int) patient.getPatientId())PatientIdentifierpatientIdentifier= Context.getPatientService().getPatientIdentifierByUuid(uuid)Assert.assertEquals(1, (int) patientIdentifier.getPatientIdentifierId())PatientIdentifierTypepatientIdentifierType= Context.getPatientService().getPatientIdentifierTypeByUuid(uuid)Assert.assertEquals(1, (int) patientIdentifierType.getPatientIdentifierTypeId())booleanfound= falsefound=trueAssert.assertTrue("odd, user 7 didn't get user 8's address", found)for (PatientIdentifier patientIdentifier : notPreferred.getIdentifiers()) {			if (patientIdentifier.getIdentifier().equals("7TU-8")) {				nonvoidedPI = patientIdentifier;			}			if (patientIdentifier.getIdentifier().equals("ABC123")) {				voidedPI = patientIdentifier;			}		}.assertTrue(contains(new ArrayList<PatientIdentifier>(preferred.getIdentifiers()), nonvoidedPI		                .getIdentifier()))Assert.assertFalse(contains(new ArrayList<PatientIdentifier>(preferred.getIdentifiers()), voidedPI.getIdentifier()))for (PatientIdentifier patientIdentifier : list) {			if (patientIdentifier.getIdentifier().equals(identifier)) {				return true;			}		}booleanfound= falsefound=trueAssert.assertTrue("odd, user 7 didn't get user 8's names", found)Patientpatient= Context.getPatientService().getPatientByUuid(uuid)Assert.assertEquals(2, (int) patient.getPatientId())PatientIdentifierpatientIdentifier= Context.getPatientService().getPatientIdentifierByUuid(uuid)Assert.assertEquals(1, (int) patientIdentifier.getPatientIdentifierId())Patientpatient= patientService.getPatient(999)Assert.assertTrue("This patient should be voided", patient.isVoided())((PatientIdentifier) (patient.getIdentifiers().toArray()[0])).isVoided()List<PatientIdentifier>patientIdentifiers= patientService.getPatientIdentifiers(null, null, null, null, null)for (PatientIdentifier patientIdentifier : patientIdentifiers) {			Assert.assertFalse("No voided identifiers should be returned", patientIdentifier.isVoided());			Assert.assertFalse("No identifiers of voided patients should be returned", patientIdentifier.getPatient()			        .isVoided());		}Patientpatient= Context.getPatientService().getPatient(2)patient.addIdentifier(identifier)Context.getPatientService().savePatient(patient)List<Patient>patientList= patientService.getPatients(null, "???", null, false)assertNotNull("an empty list should be returned instead of a null object", patientList)assertEquals(0, patientList.size())Patientpatient= patientService.getPatient(2)assertNotNull("There should be a patient with patient_id of 2", patient)assertTrue("The patient should be listed as male", patient.getGender().equals("M"))patient.setGender("F")patientService.savePatient(patient)Patientpatient2= patientService.getPatient(patient.getPatientId())assertTrue("The updated patient and the orig patient should still be equal", patient.equals(patient2))assertTrue("The gender should be new", patient2.getGender().equals("F"))Patientpatient= new Patient()assertTrue(patient.getIdentifiers().isEmpty())patientService.savePatient(patient)List<Patient>allPatients= patientService.getAllPatients()assertEquals(4, allPatients.size())List<Patient>allPatients= patientService.getAllPatients(false)assertEquals(4, allPatients.size())List<Patient>allPatients= patientService.getAllPatients(true)assertEquals(6, allPatients.size())List<Patient>patients= patientService.getPatients("Jea", null, null, false)assertTrue("getPatients failed to find patient whose first name included partial match", patients		        .contains(patientService.getPatient(4)))assertTrue("getPatients failed to find patient whose family name included partial match", patients		        .contains(patientService.getPatient(5)))assertTrue("getPatients failed to find patient whose family name included partial match", patients		        .contains(patientService.getPatient(6)))assertFalse("getPatients failed to exclude patient whose first name did not include the partial string", patients		        .contains(patientService.getPatient(2)))assertFalse("getPatients failed to exclude patient whose first name did not include the partial string", patients		        .contains(patientService.getPatient(3)))patients=patientService.getPatients("Claud", null, null, false)assertTrue("getPatients failed to find patient whose family name included partial match", patients		        .contains(patientService.getPatient(5)))assertTrue("getPatients failed to find patient whose family name included partial match", patients		        .contains(patientService.getPatient(6)))assertFalse("getPatients failed to exclude patient whose name did not include the partial string", patients		        .contains(patientService.getPatient(2)))assertFalse("getPatients failed to exclude patient whose name did not include the partial string", patients		        .contains(patientService.getPatient(3)))PatientpatientToPurge= patientService.getPatient(2)assertNotNull(patientToPurge)patientService.purgePatient(patientToPurge)List<Patient>patients= patientService.getPatients("I am voided", null, null, false)assertEquals(patients.size(), 0)Patientpatient= Context.getPatientService().getPatient(2)patient.addIdentifier(identifier)Context.getPatientService().savePatient(patient)Patientpatient= patientService.getPatientIdentifier(3).getPatient()intoldActiveIdentifierSize= patient.getActiveIdentifiers().size()PatientIdentifierpatientIdentifierToVoid= patientService.getPatientIdentifier(3)PatientIdentifiervoidedIdentifier= patientService.voidPatientIdentifier(patientIdentifierToVoid, "Testing")Assert.assertEquals("Testing", voidedIdentifier.getVoidReason())Assert.assertEquals(oldActiveIdentifierSize - 1, patient.getActiveIdentifiers().size())PatientIdentifierpatientIdentifier= new PatientIdentifier("677-56-6666", new PatientIdentifierType(4),		        new Location(1))PatientassociatedPatient= patientService.getPatient(2)patientIdentifier.setPatient(associatedPatient)PatientIdentifiercreatedPatientIdentifier= patientService.savePatientIdentifier(patientIdentifier)Assert.assertNotNull(createdPatientIdentifier)Assert.assertNotNull(createdPatientIdentifier.getPatientIdentifierId())PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setIdentifier("NEW-ID")PatientIdentifierupdatedPatientIdentifier= patientService.savePatientIdentifier(patientIdentifier)Assert.assertNotNull(updatedPatientIdentifier)Assert.assertEquals("NEW-ID", updatedPatientIdentifier.getIdentifier())PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientService.purgePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setIdentifier(null)patientService.savePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setIdentifier(" ")patientService.savePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setIdentifier("")patientService.savePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setLocation(null)patientIdentifier.getIdentifierType().setLocationBehavior(PatientIdentifierType.LocationBehavior.NOT_USED)patientService.savePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifier= patientService.getPatientIdentifier(7)patientIdentifier.setLocation(null)patientIdentifier.getIdentifierType().setLocationBehavior(PatientIdentifierType.LocationBehavior.REQUIRED)patientService.savePatientIdentifier(patientIdentifier)PatientIdentifierpatientIdentifierToVoid= patientService.getPatientIdentifier(3)patientService.voidPatientIdentifier(patientIdentifierToVoid, null)PatientIdentifierpatientIdentifierToVoid= patientService.getPatientIdentifier(3)patientService.voidPatientIdentifier(patientIdentifierToVoid, "")PatientIdentifierpatientIdentifierToVoid= patientService.getPatientIdentifier(3)patientService.voidPatientIdentifier(patientIdentifierToVoid, " ")PatientIdentifierpatientIdentifier= new PatientIdentifier()patientIdentifier.setIdentifier("123-0")patientIdentifier.setIdentifierType(patientService.getPatientIdentifierType(5))patientIdentifier.setLocation(new Location(1))notPreferred.addIdentifier(patientIdentifier)StringaddedIdentifierUuid= nullfor (PatientIdentifier id : preferred.getIdentifiers()) {			if (id.getIdentifier().equals(patientIdentifier.getIdentifier())) {				addedIdentifierUuid = id.getUuid();			}		}addedIdentifierUuidAssert.assertTrue("person identifier creation not audited", isValueInList(addedIdentifierUuid, audit		        .getPersonMergeLogData().getCreatedIdentifiers()))PatientIdentifierTypepatientIdentifierType= Context.getPatientService().getPatientIdentifierType(5)Assert.assertNotNull(patientIdentifierType)PatientIdentifierpreferredIdentifier= new PatientIdentifier()preferredIdentifier.setIdentifier("9999-4")preferredIdentifier.setIdentifierType(patientIdentifierType)preferredIdentifier.setLocation(locations.get(0))preferred.addIdentifier(preferredIdentifier)PatientIdentifiernonPreferredIdentifier= new PatientIdentifier()nonPreferredIdentifier.setIdentifier("9999-4")nonPreferredIdentifier.setIdentifierType(patientIdentifierType)nonPreferredIdentifier.setLocation(locations.get(0))notPreferred.addIdentifier(nonPreferredIdentifier)private booleanisValueInList(String value, List<String> list){		return (list != null && list.contains(value));	}Patientpatient= patientService.getPatient(2)PersonAddressaddress= patient.getAddresses().iterator().next()patientService.savePatient(patient)Context.evictFromSession(patient)patient=patientService.getPatient(2)PersonAddresspersonAddress= patient.getAddresses().iterator().next()Patientpatient= patientService.getPatient(2)Assert.assertTrue(patient.getPersonName().getGivenName().startsWith("Horati"))patient.addName(new PersonName("Horatio", "Test", "name"))Context.getPatientService().savePatient(patient)Patientpatient= patientService.getPatientOrPromotePerson(202)Assert.assertNotNull(patient)Assert.assertEquals(202, patient.getId().intValue())Patientpatient= patientService.getPatientOrPromotePerson(-1)Assert.assertNull(patient)Patientpatient= patientService.getPatient(2)patientService.voidPatient(patient, "reason")Assert.assertTrue(patient.isPersonVoided())Patientpatient= patientService.getPatient(2)Useruser= new User(patient)Assert.assertFalse(Context.getUserService().getUsersByPerson(patient, false).isEmpty())patientService.voidPatient(patient, "reason")Assert.assertTrue(Context.getUserService().getUsersByPerson(patient, false).isEmpty())Patientpatient= patientService.getPatient(2)patientService.voidPatient(patient, "reason")Assert.assertTrue(patient.isPersonVoided())patientService.unvoidPatient(patient)Assert.assertFalse(patient.isPersonVoided())Patientpatient= patientService.getPatient(2)Useruser= new User(patient)patientService.voidPatient(patient, "reason")patientService.unvoidPatient(patient)Assert.assertTrue(Context.getUserService().getUsersByPerson(patient, false).isEmpty())List<Patient>patients= patientService.getPatients("", "", null, false)Assert.assertTrue(patients.isEmpty())PatientpreferredPatient= patientService.getPatient(10000)PatientnonPreferredPatient= patientService.getPatient(10001)patientService.mergePatients(preferredPatient, nonPreferredPatient)Set<PersonName>names= preferredPatient.getNames()PatientpreferredPatient= patientService.getPatient(10000)PatientnonPreferredPatient= patientService.getPatient(10001)patientService.mergePatients(preferredPatient, nonPreferredPatient)Set<PersonAddress>addresses= preferredPatient.getAddresses()PatientpreferredPatient= patientService.getPatient(10001)PatientnonPreferredPatient= patientService.getPatient(10000)patientService.mergePatients(preferredPatient, nonPreferredPatient)assertThat(preferredPatient.getAddresses().size(), equalTo(2))Patientpatient= new Patient()patient.setGender("M")patient.addIdentifier(identifier)patient.addName(name)patient.addAddress(address)Context.getPatientService().savePatient(patient)Patientpatient= new Patient()patient.setGender("M")PatientIdentifierpreferredIdentifier= new PatientIdentifier("QWERTY2", patientService.getPatientIdentifierType(2),		        locationService.getLocation(1))preferredIdentifier.setPreferred(true)patient.addIdentifier(identifier)patient.addIdentifier(preferredIdentifier)patient.addName(name)patient.addName(preferredName)patient.addAddress(address)patient.addAddress(preferredAddress)patientService.savePatient(patient)Assert.assertTrue(preferredIdentifier.isPreferred())Patientpatient= new Patient()patient.setGender("M")PatientIdentifierpreferredIdentifier= new PatientIdentifier("QWERTY2", patientService.getPatientIdentifierType(2),		        locationService.getLocation(1))preferredIdentifier.setPreferred(true)preferredIdentifier.setVoided(true)patient.addIdentifier(identifier)patient.addIdentifier(preferredIdentifier)patient.addName(name)patient.addName(preferredName)patient.addAddress(address)patient.addAddress(preferredAddress)patientService.savePatient(patient)Assert.assertFalse(preferredIdentifier.isPreferred())Patientpatient= patientService.getPatientOrPromotePerson(person.getPersonId())PatientIdentifierpatientIdentifier= new PatientIdentifier("some identifier", new PatientIdentifierType(2),		        new Location(1))patientIdentifier.setPreferred(true)patient.addIdentifier(patientIdentifier)patientService.savePatient(patient)Patientpatient= patientService.getPatient(2)patient.addIdentifier(pId)patientService.savePatient(patient)Patientpatient= patientService.getPatient(2)patient.addIdentifier(pId)patientService.savePatient(patient)PatientpreferredPatient= patientService.getPatient(8)PatientnotPreferredPatient= patientService.getPatient(7)patientService.mergePatients(preferredPatient, notPreferredPatient)
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptDescription.java">
{public Integer getId() {		return getConceptDescriptionId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/BaseOpenmrsObjectTest.java">
{@Override		public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseOpenmrsData.java">
{public Boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseOpenmrsObject.java">
{public String getUuid() {		return uuid;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/activelist/ActiveListItem.java">
{@Override	public Integer getId() {		return getActiveListId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/FormResource.java">
{@Override	public Integer getId() {		return getFormResourceId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Cohort.java">
{public int size() {		return getMemberIds() == null ? 0 : getMemberIds().size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/PatientIdentifierException.java">
{public PatientIdentifier getPatientIdentifier() {		return patientIdentifier;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/FieldType.java">
{public Integer getId() {				return getFieldTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/NoteType.java">
{public Integer getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/EncounterType.java">
{public Integer getId() {		return getEncounterTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptProposal.java">
{public Integer getId() {		return getConceptProposalId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/ReportSchema.java">
{public Integer getId() {		return getReportSchemaId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseCustomizableData.java">
{@Override	public Set<A> getAttributes() {		return attributes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Drug.java">
{public Integer getId() {				return getDrugId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Program.java">
{public Integer getId() {				return getProgramId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/ReportObjectWrapper.java">
{public Integer getId() {		return getReportObjectId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Order.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ProgramWorkflow.java">
{public Integer getId() {				return getProgramWorkflowId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonName.java">
{public Integer getId() {		return getPersonNameId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/EncounterProvider.java">
{public Integer getId() {		return getEncounterProviderId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptReferenceTerm.java">
{@Override	public Integer getId() {		return getConceptReferenceTermId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptDatatype.java">
{public Integer getId() {				return getConceptDatatypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/Note.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/person/PersonMergeLog.java">
{public Integer getId() {		return getPersonMergeLogId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/api/handler/OpenmrsObjectSaveHandlerTest.java">
{public Integer getId() {			return id;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/LocationTag.java">
{public Integer getId() {		return getLocationTagId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/util/DatabaseUpdater.java">
{public String getId() {			return id;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptAnswer.java">
{public Integer getId() {		return getConceptAnswerId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderSet.java">
{public Integer getId() {		return getOrderSetId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/LocationAttribute.java">
{@Override	public Integer getId() {		return getLocationAttributeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/VisitType.java">
{@Override	public Integer getId() {		return getVisitTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptClass.java">
{public Integer getId() {		return getConceptClassId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/VisitListItem.java">
{public boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Concept.java">
{@ElementList	public Collection<ConceptName> getNames() {		return getNames(false);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptMap.java">
{public Integer getId() {		return getConceptMapId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/ListItem.java">
{public Integer getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/GlobalProperty.java">
{public Integer getId() {		throw new UnsupportedOperationException();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/export/DataExportReportObject.java">
{public boolean getAllPatients() {		return allPatients;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/Message.java">
{public Integer getId() {		return this.id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Encounter.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ProgramWorkflowState.java">
{public Integer getId() {		return getProgramWorkflowStateId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ProviderAttributeType.java">
{@Override	public Integer getId() {		return getProviderAttributeTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PersonListItem.java">
{public String getUuid() {		if (uuid == null) {			return "";		}		return uuid;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Obs.java">
{public Patient getPatient() {		return (Patient) getPerson();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OpenmrsObject.java">
{public String getUuid();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Location.java">
{public Integer getId() {				return getLocationId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/LocationAttributeType.java">
{@Override	public Integer getId() {		return getLocationAttributeTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/hibernate/HibernatePatientSetDAO.java">
{@SuppressWarnings("unchecked")	public Cohort getAllPatients() {				Query query = sessionFactory.getCurrentSession().createQuery("select patientId from Patient p where p.voided = '0'");				Set<Integer> ids = new HashSet<Integer>();		ids.addAll(query.list());				return new Cohort("All patients", "", ids);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/customdatatype/Customizable.java">
{Collection<A> getAttributes();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ProviderAttribute.java">
{@Override	public Integer getId() {		return getProviderAttributeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/util/ReflectTest.java">
{public String getUuid() {		return null;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Form.java">
{public Integer getId() {				return getFormId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderFrequency.java">
{public String getUuid() {		return uuid;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/FormField.java">
{public Integer getId() {		return getFormFieldId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EncounterListItem.java">
{public boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientIdentifierType.java">
{public Integer getId() {		return getPatientIdentifierTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/RelationshipType.java">
{public Integer getId() {		return getRelationshipTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/arden/ArdenValue.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/EncounterRole.java">
{public Integer getId() {				return getEncounterRoleId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/scheduler/Schedule.java">
{public Integer getId() {		return this.id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptMapType.java">
{public Integer getId() {		return getConceptMapTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/DataSet.java">
{public Iterator<Map<String, T>> iterator();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/ForEachDisplayAttributeTag.java">
{@Override	protected Object next() throws JspTagException {		if (attrTypes == null) {			throw new JspTagException("The attr iterator is null");		}		return attrTypes.next();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptNameTag.java">
{public Boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/DrugIngredient.java">
{public Integer getId() {		throw new UnsupportedOperationException();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/AlertRecipient.java">
{public Integer getId() {		throw new UnsupportedOperationException();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/export/DataExportFunctions.java">
{public Patient getPatient() {		if (patient == null) {			patient = patientService.getPatient(patientId);		}				return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderGroup.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/Template.java">
{public Integer getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/reporting/AbstractReportObject.java">
{public Integer getId() {		return getReportObjectId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Visit.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptReferenceTermMap.java">
{public Integer getId() {		return getConceptReferenceTermMapId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAttribute.java">
{public Integer getId() {				return getPersonAttributeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/PatientSetDAO.java">
{public Cohort getAllPatients();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/DrugReferenceMap.java">
{@Override	public Integer getId() {		return getDrugReferenceMapId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/MimeType.java">
{public Integer getId() {				return getMimeTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/ReportSchemaXml.java">
{public Integer getId() {		return getReportSchemaId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/scheduler/TaskDefinition.java">
{public Integer getId() {		return this.id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/LoginCredential.java">
{public Integer getId() {		return userId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/hl7/HL7InQueue.java">
{public Integer getId() {		return getHL7InQueueId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/SerializedObject.java">
{public Integer getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAddress.java">
{public Integer getId() {				return getPersonAddressId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PersonAttributeType.java">
{public Integer getId() {		return getPersonAttributeTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/RowPerObsDataSet.java">
{public Map<String, Object> next() {			Locale locale = Context.getLocale();			Obs obs = iter.next();			Map<String, Object> ret = new HashMap<String, Object>();			ret.put("patientId", obs.getPersonId());			ret.put("question", obs.getConcept().getName(locale, false));			ret.put("questionConceptId", obs.getConcept().getConceptId());			ret.put("answer", obs.getValueAsString(locale));			if (obs.getValueCoded() != null) {				ret.put("answerConceptId", obs.getValueCoded());			}			ret.put("obsDatetime", obs.getObsDatetime());			if (obs.getEncounter() != null) {				ret.put("encounterId", obs.getEncounter().getEncounterId());			}			if (obs.getObsGroup() != null) {				ret.put("obsGroupId", obs.getObsGroup().getObsId());			}			return ret;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Voidable.java">
{public Boolean isVoided();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{@ElementList(required = false)	public Set<PersonAddress> getAddresses() {		if (addresses == null) {			addresses = new TreeSet<PersonAddress>();		}		return this.addresses;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/CareSetting.java">
{@Override	public Integer getId() {		return getCareSettingId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Privilege.java">
{public Integer getId() {		throw new UnsupportedOperationException();			}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Relationship.java">
{public Integer getId() {				return getRelationshipId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public PatientService getPatientService() {		return getService(PatientService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/impl/PatientServiceImpl.java">
{@Transactional(readOnly = true)	public List<Patient> getAllPatients() throws APIException {		return Context.getPatientService().getAllPatients(false);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/activelist/ActiveListType.java">
{@Override	public Integer getId() {		return getActiveListTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptStopWord.java">
{public Integer getId() {		return getConceptStopWordId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/PatientService.java">
{@Authorized( { PrivilegeConstants.VIEW_PATIENTS })	public List<Patient> getAllPatients() throws APIException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Role.java">
{public Integer getId() {		throw new UnsupportedOperationException();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptSet.java">
{public Integer getId() {		return getConceptSetId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/ProviderListItem.java">
{public String getIdentifier() {		return identifier;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/PatientSetService.java">
{public Cohort getAllPatients() throws DAOException;}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/notification/Alert.java">
{public Integer getId() {		return getAlertId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientIdentifier.java">
{public String getIdentifier() {		return identifier;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/BaseCustomizableMetadata.java">
{@Override	public Set<A> getAttributes() {		return attributes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/module/DaemonToken.java">
{public String getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/PortletTag.java">
{public String getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static PatientService getPatientService() {		return getServiceContext().getPatientService();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientState.java">
{public Integer getId() {		return getPatientStateId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/FieldAnswer.java">
{public Integer getId() {		throw new UnsupportedOperationException();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/taglib/ObsTableWidget.java">
{public String getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/cohort/CohortSearchHistory.java">
{public int size() {		return searchHistory.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/hl7/HL7InError.java">
{public Integer getId() {		return getHL7InErrorId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Field.java">
{public Integer getId() {				return getFieldId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/VisitAttribute.java">
{@Override	public Integer getId() {		return getVisitAttributeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public Set<PersonName> getNames() {		return person.getNames();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/hl7/HL7InArchive.java">
{public Integer getId() {		return getHL7InArchiveId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/hl7/HL7Source.java">
{public Integer getId() {		return getHL7SourceId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/hibernate/HibernateConceptDAO.java">
{public Concept next() {			if (currentConcept != null) {				sessionFactory.getCurrentSession().evict(currentConcept);			}						currentConcept = nextConcept;			nextConcept = getNextConcept(currentConcept);						return currentConcept;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/api/PatientServiceTest.java">
{private Patient createBasicPatient() {		Patient patient = new Patient();				PersonName pName = new PersonName();		pName.setGivenName("Tom");		pName.setMiddleName("E.");		pName.setFamilyName("Patient");		patient.addName(pName);				PersonAddress pAddress = new PersonAddress();		pAddress.setAddress1("123 My street");		pAddress.setAddress2("Apt 402");		pAddress.setCityVillage("Anywhere city");		pAddress.setCountry("Some Country");		Set<PersonAddress> pAddressList = patient.getAddresses();		pAddressList.add(pAddress);		patient.setAddresses(pAddressList);		patient.addAddress(pAddress);		// patient.removeAddress(pAddress);				patient.setDeathDate(new Date());		// patient.setCauseOfDeath("air");		patient.setBirthdate(new Date());		patient.setBirthdateEstimated(true);		patient.setGender("male");		patient.setDeathdateEstimated(true);				return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/db/ClobDatatypeStorage.java">
{@Override	public Integer getId() {		return id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderSetMember.java">
{@Override	public Integer getId() {		return getOrderSetMemberId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return id;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/test/java/org/openmrs/aop/RequiredDataAdviceTest.java">
{@Override		public Integer getId() {			return null;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/notification/web/ForEachAlertTag.java">
{@Override	protected Object next() throws JspTagException {		if (alerts == null) {			throw new JspTagException("The alert iterator is null");		}		return alerts.next();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptStateConversion.java">
{public Integer getId() {		return getConceptStateConversionId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/PatientProgram.java">
{public Patient getPatient() {		return patient;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/CohortDataSet.java">
{public Iterator<Map<String, Cohort>> iterator() {		return Collections.singleton(cohortData).iterator();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public List<PatientIdentifier> getActiveIdentifiers() {		List<PatientIdentifier> ids = new Vector<PatientIdentifier>();		if (getIdentifiers() != null) {			List<PatientIdentifier> nonPreferred = new LinkedList<PatientIdentifier>();			for (PatientIdentifier pi : getIdentifiers()) {				if (pi.isVoided() == false) {					if (pi.isPreferred()) {						ids.add(pi);					} else {						nonPreferred.add(pi);					}				}			}			for (PatientIdentifier pi : nonPreferred) {				ids.add(pi);			}		}		return ids;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Provider.java">
{public String getIdentifier() {		return identifier;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/impl/PatientSetServiceImpl.java">
{public Cohort getAllPatients() throws DAOException {		return getPatientSetDAO().getAllPatients();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/report/RowPerProgramEnrollmentDataSet.java">
{public Map<String, Object> next() {			Locale locale = Context.getLocale();			PatientProgram pp = iter.next();			Map<String, Object> ret = new HashMap<String, Object>();			ret.put("patientId", pp.getPatient().getPatientId());			ret.put("programName", pp.getProgram().getConcept().getName(locale, false).getName());			ret.put("programId", pp.getProgram().getProgramId());			ret.put("enrollmentDate", pp.getDateEnrolled());			ret.put("completionDate", pp.getDateCompleted());			ret.put("patientProgramId", pp.getPatientProgramId());			return ret;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/VisitAttributeType.java">
{@Override	public Integer getId() {		return getVisitAttributeTypeId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptName.java">
{public Boolean isVoided() {		return voided;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PatientListItem.java">
{public String getIdentifier() {		return identifier;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/messagesource/PresentationMessageMap.java">
{public int size() {		return internalMap.size();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/ConceptSource.java">
{@Deprecated	public Boolean isVoided() {		return isRetired();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/OrderType.java">
{public Integer getId() {		return getOrderTypeId();	}}
