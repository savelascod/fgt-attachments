public ShortPatientModel(Patient patient){		if (patient != null) {			this.patient = patient;			this.personName = patient.getPersonName();			this.personAddress = patient.getPersonAddress();			List<PatientIdentifier> activeIdentifiers = patient.getActiveIdentifiers();			if (activeIdentifiers.isEmpty()) {				final  PatientIdentifierType defaultIdentifierType = getDefaultIdentifierType();				activeIdentifiers.add(new PatientIdentifier(null, defaultIdentifierType, (LocationUtility				        .getUserDefaultLocation() != null) ? LocationUtility.getUserDefaultLocation() : LocationUtility				        .getDefaultLocation()));			}						identifiers = ListUtils.lazyList(new ArrayList<PatientIdentifier>(activeIdentifiers), FactoryUtils			        .instantiateFactory(PatientIdentifier.class));						List<PersonAttributeType> viewableAttributeTypes = Context.getPersonService().getPersonAttributeTypes(			    PERSON_TYPE.PATIENT, ATTR_VIEW_TYPE.VIEWING);						personAttributes = new ArrayList<PersonAttribute>();			if (!CollectionUtils.isEmpty(viewableAttributeTypes)) {				for (PersonAttributeType personAttributeType : viewableAttributeTypes) {					PersonAttribute persistedAttribute = patient.getAttribute(personAttributeType);					//This ensures that empty attributes are added for those we want to display 					//in the view, but have no values					PersonAttribute formAttribute = new PersonAttribute(personAttributeType, null);										//send a clone to the form so that we can use the original to track changes in the values					if (persistedAttribute != null) {						BeanUtils.copyProperties(persistedAttribute, formAttribute);					}										personAttributes.add(formAttribute);				}			}		}	}public voidsetIdentifiers(List<PatientIdentifier> identifiers){		this.identifiers = identifiers;	}public voidsetPatient(Patient patient){		this.patient = patient;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/logic/result/EmptyResult.java">
{@Override	public boolean isEmpty() {		return true;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EnteredField.java">
{public boolean isEmpty() {		return value == null || value.length() == 0;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/user/ChangePasswordFormController.java">
{public boolean isEmpty() {			return getPassword().isEmpty();		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/controller/patient/ShortPatientModel.java">
{private PatientIdentifierType getDefaultIdentifierType() {		List<PatientIdentifierType> types = Context.getPatientService().getAllPatientIdentifierTypes();		if (types.isEmpty()) {			return null;		} else {			return types.iterator().next();		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/VisitListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/ObsListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/EncounterListItem.java">
{public String getPersonName() {		return personName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Person.java">
{public PersonAddress getPersonAddress() {		// normally the DAO layer returns these in the correct order, i.e. preferred and non-voided first, but it's possible that someone		// has fetched a Person, changed their addresses around, and then calls this method, so we have to be careful.		if (getAddresses() != null && getAddresses().size() > 0) {			for (PersonAddress addr : getAddresses()) {				if (addr.isPreferred() && !addr.isVoided()) {					return addr;				}			}			for (PersonAddress addr : getAddresses()) {				if (!addr.isVoided()) {					return addr;				}			}						if (isVoided() && !getAddresses().isEmpty()) {				return getAddresses().iterator().next();			}		}		return null;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Cohort.java">
{public boolean isEmpty() {		return size() == 0;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/Context.java">
{public static PersonService getPersonService() {		return getServiceContext().getPersonService();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/Patient.java">
{public List<PatientIdentifier> getActiveIdentifiers() {		List<PatientIdentifier> ids = new Vector<PatientIdentifier>();		if (getIdentifiers() != null) {			List<PatientIdentifier> nonPreferred = new LinkedList<PatientIdentifier>();			for (PatientIdentifier pi : getIdentifiers()) {				if (pi.isVoided() == false) {					if (pi.isPreferred()) {						ids.add(pi);					} else {						nonPreferred.add(pi);					}				}			}			for (PatientIdentifier pi : nonPreferred) {				ids.add(pi);			}		}		return ids;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/dwr/PersonListItem.java">
{public String getPersonName() {		String name = "";				if (!StringUtils.isBlank(givenName)) {			name = givenName;		}				if (!StringUtils.isBlank(middleName)) {			name = name + (name.length() > 0 ? " " : "") + middleName;		}				if (!StringUtils.isBlank(familyName)) {			name = name + (name.length() > 0 ? " " : "") + familyName;		}				return name;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/api/context/ServiceContext.java">
{public PersonService getPersonService() {		return getService(PersonService.class);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/messagesource/PresentationMessageMap.java">
{public boolean isEmpty() {		return internalMap.isEmpty();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//api/src/main/java/org/openmrs/User.java">
{public PersonName getPersonName() {		return getPerson() == null ? null : getPerson().getPersonName();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/openmrs-core//web/src/main/java/org/openmrs/web/OptionsForm.java">
{public PersonName getPersonName() {		return personName;	}}
