public List<DemographicExport>getAllDocumentsForPatient(int demographicNo){        String sql = "SELECT id, document_type, document, de.demographic_no "                + "FROM DemographicExport de, Demographic d "                + "WHERE de.demographic_no = d.demographic_no AND de.demographic_no = ?";        Query query = entityManager.createQuery(sql);        query.setParameter(1, demographicNo);        return query.getResultList();    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/handler/AbstractQueryHandler.java">
{public List<?> getResultList() {		return resultList;	}}
