PrintWriterwriter= res.getWriter()writer.println("<script>alert('Error: No fax number found!');window.close();</script>")StringtempPath= OscarProperties.getInstance().getProperty(								"fax_file_location", System.getProperty("java.io.tmpdir"))StringtempPdf= tempPath + "/prescription_" + req.getParameter("pdfId") + ".pdf"FileUtils.copyFile(new File(path+pdfFile), new File(tempPdf))StringtxtFile= tempPath + "/prescription_" + req.getParameter("pdfId") + ".txt"writer.println("<script>alert('Fax sent to: " + StringEscapeUtils.escapeJavaScript(req.getParameter("pharmaName")) + " (" + faxNo + ")');window.close();</script>")PrintWriterwriter= res.getWriter()writer.println("Exception from: " + this.getClass().getName() + " " + dex.getClass().getName() + "<br>")writer.println("<pre>")writer.println(dex.getMessage())writer.println("</pre>")PrintWriterwriter= res.getWriter()writer.println("<script>alert('Signature not found. Please sign the prescription.');</script>")public EndPage(String clinicName, String clinicTel, String clinicFax, String patientPhone, String patientCityPostal, String patientAddress,                String patientName,String patientDOB, String sigDoctorName, String rxDate,String origPrintDate,String numPrint, String imgPath, String patientHIN, String patientChartNo,String pracNo, Locale locale){			this.clinicName = clinicName==null ? "" : clinicName;			this.clinicTel = clinicTel==null ? "" : clinicTel;			this.clinicFax = clinicFax==null ? "" : clinicFax;			this.patientPhone = patientPhone==null ? "" : patientPhone;			this.patientCityPostal = patientCityPostal==null ? "" : patientCityPostal;			this.patientAddress = patientAddress==null ? "" : patientAddress;			this.patientName = patientName;            this.patientDOB=patientDOB;			this.sigDoctorName = sigDoctorName==null ? "" : sigDoctorName;			this.rxDate = rxDate;			this.promoText = OscarProperties.getInstance().getProperty("FORMS_PROMOTEXT");			this.origPrintDate = origPrintDate;			this.numPrint = numPrint;			if (promoText == null) {				promoText = "";			}			this.imgPath = imgPath;			this.patientHIN = patientHIN==null ? "" : patientHIN;			this.patientChartNo = patientChartNo==null ? "" : patientChartNo;			this.pracNo = pracNo==null ? "" : pracNo;			this.locale = locale;		}public voidonEndPage(PdfWriter writer, Document document){			renderPage(writer, document);		}private Stringgeti18nTagValue(Locale locale, String tag){			return LocaleUtils.getMessage(locale,tag);		}public voidrenderPage(PdfWriter writer, Document document){			Rectangle page = document.getPageSize();			PdfContentByte cb = writer.getDirectContent();			try {				float height = page.getHeight();                                boolean showPatientDOB=false;                                //head.writeSelectedRows(0, 1,document.leftMargin(), page.height() - document.topMargin()+ head.getTotalHeight(),writer.getDirectContent());                                if(this.patientDOB!=null && this.patientDOB.length()>0){                                    showPatientDOB=true;                                }                                //header table for patient's information.                                                PdfPTable head = new PdfPTable(1);                                                String newline = System.getProperty("line.separator");                                StringBuilder hStr = new StringBuilder(this.patientName);                                if(showPatientDOB){                                     hStr.append("   "+geti18nTagValue(locale, "RxPreview.msgDOB")+":").append(this.patientDOB);                                }                                hStr.append(newline).append(this.patientAddress).append(newline).append(this.patientCityPostal).append(newline).append(this.patientPhone);                                                                if (patientHIN != null && patientHIN.trim().length() > 0) {                                     hStr.append(newline).append(geti18nTagValue(locale, "oscar.oscarRx.hin")+" ").append(patientHIN);                                 }                                if (patientChartNo != null && !patientChartNo.isEmpty()) {                                    String chartNoTitle = geti18nTagValue(locale, "oscar.oscarRx.chartNo") ;                                    hStr.append(newline).append(chartNoTitle).append(patientChartNo);                                }                                				BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);				Phrase hPhrase = new Phrase(hStr.toString(), new Font(bf, 10));				head.addCell(hPhrase);				head.setTotalWidth(272f);				head.writeSelectedRows(0, -1, 13f, height - 100f, cb);				bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);				writeDirectContent(cb, bf, 12, PdfContentByte.ALIGN_LEFT, "o s c a r", 21, page.getHeight() - 60, 90);				// draw R				writeDirectContent(cb, bf, 50, PdfContentByte.ALIGN_LEFT, "P", 24, page.getHeight() - 53, 0);				bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);				// draw X				writeDirectContent(cb, bf, 43, PdfContentByte.ALIGN_LEFT, "X", 38, page.getHeight() - 69, 0);				bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);				writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, this.sigDoctorName, 80, (page.getHeight() - 25), 0);				writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, this.rxDate, 188, (page.getHeight() - 90), 0);								bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);				int fontFlags = Font.NORMAL;				Font font = new Font(bf, 10, fontFlags);				ColumnText ct = new ColumnText(cb);				ct.setSimpleColumn(80, (page.getHeight() - 25), 280, (page.getHeight() - 90), 11, Element.ALIGN_LEFT);				// p("value of clinic name", this.clinicName);				ct.setText(new Phrase(12, clinicName+(pracNo.trim().length()>0 ? "\r\n"+geti18nTagValue(locale, "RxPreview.PractNo")+": "+ pracNo : ""), font));ct.go();				// render clnicaTel;				if (this.clinicTel.length() <= 13) {					writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, geti18nTagValue(locale, "RxPreview.msgTel")+":" + this.clinicTel, 188, (page.getHeight() - 70), 0);					// render clinicFax;					writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, geti18nTagValue(locale, "RxPreview.msgFax")+":" + this.clinicFax, 188, (page.getHeight() - 80), 0);				} else {					String str1 = this.clinicTel.substring(0, 13);					String str2 = this.clinicTel.substring(13);					writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, geti18nTagValue(locale, "RxPreview.msgTel")+":" + str1, 188, (page.getHeight() - 70), 0);					writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, str2, 188, (page.getHeight() - 80), 0);					writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, geti18nTagValue(locale, "RxPreview.msgFax")+":" + this.clinicFax, 188, (page.getHeight() - 88), 0);				}				// get the end of paragraph				float endPara = writer.getVerticalPosition(true);				// draw left line				cb.setRGBColorStrokeF(0f, 0f, 0f);				cb.setLineWidth(0.5f);				// cb.moveTo(13f, 20f);				cb.moveTo(13f, endPara - 60);				cb.lineTo(13f, height - 15f);				cb.stroke();				// draw right line 285, 20, 285, 405, 0.5				cb.setRGBColorStrokeF(0f, 0f, 0f);				cb.setLineWidth(0.5f);				// cb.moveTo(285f, 20f);				cb.moveTo(285f, endPara - 60);				cb.lineTo(285f, height - 15f);				cb.stroke();				// draw top line 10, 405, 285, 405, 0.5				cb.setRGBColorStrokeF(0f, 0f, 0f);				cb.setLineWidth(0.5f);				cb.moveTo(13f, height - 15f);				cb.lineTo(285f, height - 15f);				cb.stroke();				// draw bottom line 10, 20, 285, 20, 0.5				cb.setRGBColorStrokeF(0f, 0f, 0f);				cb.setLineWidth(0.5f);				// cb.moveTo(13f, 20f);				// cb.lineTo(285f, 20f);				cb.moveTo(13f, endPara - 60);				cb.lineTo(285f, endPara - 60);				cb.stroke();				// Render "Signature:"				writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, geti18nTagValue(locale, "RxPreview.msgSignature"), 20f, endPara - 30f, 0);// Render line for Signature 75, 55, 280, 55, 0.5				cb.setRGBColorStrokeF(0f, 0f, 0f);				cb.setLineWidth(0.5f);				// cb.moveTo(75f, 50f);				// cb.lineTo(280f, 50f);				cb.moveTo(75f, endPara - 30f);				cb.lineTo(280f, endPara - 30f);				cb.stroke();				if (this.imgPath != null) {					Image img = Image.getInstance(this.imgPath);					// image, image_width, 0, 0, image_height, x, y					//         131, 55, 375, 75, 0					cb.addImage(img, 157, 0, 0, 40, 150f, endPara-30f);				}				// Render doctor name				writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_LEFT, this.sigDoctorName, 90, endPara - 40f, 0);				// public void writeDirectContent(PdfContentByte cb, BaseFont bf, float fontSize, int alignment, String text, float x, float y, float rotation)				// render reprint origPrintDate and numPrint				if (origPrintDate != null && numPrint != null) {					String rePrintStr = geti18nTagValue(locale, "RxPreview.msgReprintBy")+" " + this.sigDoctorName + "; "+geti18nTagValue(locale, "RxPreview.msgOrigPrinted")+": " + origPrintDate + "; "+geti18nTagValue(locale, "RxPreview.msgTimesPrinted") +": " + numPrint;writeDirectContent(cb, bf, 6, PdfContentByte.ALIGN_LEFT, rePrintStr, 50, endPara - 48, 0);				}				// print promoText				writeDirectContent(cb, bf, 6, PdfContentByte.ALIGN_LEFT, this.promoText, 70, endPara - 57, 0);				// print page number				String footer = "" + writer.getPageNumber();				writeDirectContent(cb, bf, 10, PdfContentByte.ALIGN_RIGHT, footer, 280, endPara - 57, 0);			} catch (Exception e) {				logger.error("Error", e);			}		}StringclinicName= lst.get(0) + "\n" + lst.get(1) + "\n" + lst.get(2)logger.debug(clinicName)hm.put("clinicName", clinicName)PdfWriterwriter= nullStringclinicNameStringclinicTelStringclinicFaxclinicName=hm.get("clinicName")clinicTel=hm.get("clinicTel")clinicFax=hm.get("clinicFax")clinicName=req.getParameter("clinicName")logger.debug("clinicName" + "=" + clinicName)clinicTel=req.getParameter("clinicPhone")clinicFax=req.getParameter("clinicFax")StringpatientPhone= req.getParameter("patientPhone")StringpatientCityPostal= req.getParameter("patientCityPostal")StringpatientAddress= req.getParameter("patientAddress")StringpatientName= req.getParameter("patientName")StringpatientDOB=req.getParameter("patientDOB")StringshowPatientDOB=req.getParameter("showPatientDOB")StringpatientHIN=req.getParameter("patientHIN")StringpatientChartNo= req.getParameter("patientChartNo")Localelocale= req.getLocale()if (clinicName==null) clinicName = "";if (clinicTel==null) clinicTel = "";if (clinicFax==null) clinicFax = "";if (patientPhone==null) patientPhone = "";if (patientCityPostal==null) patientCityPostal = "";if (patientAddress==null) patientAddress = "";if (patientHIN==null) patientHIN = "";if (patientChartNo==null) patientChartNo = "";if(showPatientDOB!=null&&showPatientDOB.equalsIgnoreCase("true")){            isShowDemoDOB=true;        }patientDOB=""StringBuildertemp= new StringBuilder()temp=newStringBuilder(e.nextElement().toString())props.setProperty(temp.toString(), req.getParameter(temp.toString()))temp=newStringBuilder(e.nextElement().toString())props.setProperty(temp.toString(), req.getAttribute(temp.toString()).toString())writer=PdfWriterFactory.newInstance(document, baosPDF, FontSettings.HELVETICA_10PT)writer.setPageEvent(new EndPage(clinicName, clinicTel, clinicFax, patientPhone, patientCityPostal, patientAddress, patientName,patientDOB, sigDoctorName, rxDate, origPrintDate, numPrint, imgFile, patientHIN, patientChartNo, pracNo, locale))PdfContentBytecb= writer.getDirectContent()if (writer != null) {				writer.close();			}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hsfo2Visit.java">
{public double getHeight()  {    return Height;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public static OscarProperties getInstance() {		return oscarProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/printing/PrivacyStatementAppendingFilter.java">
{@Override        public PrintWriter getWriter() throws IOException {	        responseWriterObtained = true;	        if (writer == null)	        	writer = new DelegatingWriter(super.getWriter());	        return writer;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/inbox/InboxManagerQuery.java">
{public int getPageSize() {		return pageSize;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/study/HSFO/PatientData.java">
{public double getHeight() {		return Height;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}
