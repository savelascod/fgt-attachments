public static ClinicalDocumentcreateEmrConversionDocument(Integer demographicNo){		ClinicalDocument clinicalDocument = null;		PatientExport patientExport = new PatientExport(demographicNo);		if(patientExport.isActive()) {			CE<String> code = Constants.EMRConversionDocument.CODE;			II templateId = new II(Constants.EMRConversionDocument.TEMPLATE_ID);			EmrExportPopulator emrExportPopulator = new EmrExportPopulator(patientExport, code, templateId);			emrExportPopulator.populate();			clinicalDocument = emrExportPopulator.getClinicalDocument();		}		return clinicalDocument;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/LookupListItem.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/LookupTableDefValue.java">
{public boolean isActive() {	 return active;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicContact.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchRequest.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/TicklerCategory.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/GroupNoteLink.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/DocumentBodyPopulator.java">
{@Override	public void populate() {		Component2 bodyComponent = new Component2();		bodyComponent.setTypeCode(ActRelationshipHasComponent.HasComponent);		bodyComponent.setContextConductionInd(true);		bodyComponent.setBodyChoice(getStructuredBody());		clinicalDocument.setComponent(bodyComponent);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/ClinicallyMeasuredObservationsPopulator.java">
{@Override	public void populate() {		if(measurements != null) {			for(Measurement measurement : measurements) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(measurement)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Dashboard.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/model/PatientExport.java">
{public Boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/LookupCodeValue.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ResourceStorage.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/LookupListItemTo1.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/header/InformationRecipientPopulator.java">
{@Override	public void populate() {		InformationRecipient informationRecipient = new InformationRecipient();		IntendedRecipient intendedRecipient = new IntendedRecipient();		informationRecipient.setIntendedRecipient(intendedRecipient);		informationRecipient.setTypeCode(x_InformationRecipient.PRCP);		intendedRecipient.setNullFlavor(NullFlavor.NoInformation);		clinicalDocument.setInformationRecipient(new ArrayList<InformationRecipient>(Arrays.asList(informationRecipient)));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IndicatorTemplate.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/AbstractDataDisplayBean.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicContactTo1.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/AbstractPopulator.java">
{public ClinicalDocument getClinicalDocument() {		return clinicalDocument;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Demographic.java">
{public boolean isActive() {		return activeCount > 0;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/EncountersPopulator.java">
{@Override	public void populate() {		if(encounters != null) {			for(CaseManagementNote encounter : encounters) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(encounter)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/DashboardBean.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/header/AuthorPopulator.java">
{@Override	public void populate() {		ArrayList<Author> authors = new ArrayList<Author>();		authors.add(getProvider());		authors.add(getSystem());		clinicalDocument.setAuthor(authors);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsentType.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/RiskFactorsPopulator.java">
{@Override	public void populate() {		if(riskFactors != null) {			for(CaseManagementNote riskFactor : riskFactors) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(riskFactor)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/FamilyHistoryPopulator.java">
{@Override	public void populate() {		if(familyHistory != null) {			for(FamilyHistoryEntry familyEntry : familyHistory) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(familyEntry)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/caisi/wl/VacancyDisplayBO.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FaxConfig.java">
{public Boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/LookupList.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/ProblemsPopulator.java">
{@Override	public void populate() {		for(Dxresearch problem : problems) {			if(problem.getCodingSystem().equals("icd9")) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(problem)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/wlmatch/VacancyDisplayBO.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/ImmunizationsPopulator.java">
{@Override	public void populate() {		if(immunizations != null) {			for(Immunization immunization : immunizations) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(immunization)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/EmrExportPopulator.java">
{@Override	public void populate() {		AbstractPopulator.doPopulate(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Bed.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/header/CustodianPopulator.java">
{@Override	public void populate() {		Custodian custodian = new Custodian();		AssignedCustodian assignedCustodian = new AssignedCustodian();		CustodianOrganization custodianOrganization = new CustodianOrganization();		custodian.setAssignedCustodian(assignedCustodian);		assignedCustodian.setRepresentedCustodianOrganization(custodianOrganization);		custodianOrganization.setId(custodianModel.getIds());		custodianOrganization.setName(custodianModel.getName());		clinicalDocument.setCustodian(custodian);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CtlRelationships.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/AlertsPopulator.java">
{@Override	public void populate() {		if(alerts != null) {			for(CaseManagementNote alert : alerts) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(alert)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/AllergiesPopulator.java">
{@Override	public void populate() {		if(allergies != null) {			for(Allergy allergy : allergies) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(allergy)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/Program.java">
{public boolean isActive() {		return PROGRAM_STATUS_ACTIVE.equals(programStatus);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/Secrole.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/study/types/MyMedsStudy.java">
{public boolean isActive() {		StudyDao studyDao = SpringUtils.getBean(StudyDao.class);		org.oscarehr.common.model.Study study = studyDao.findByName(Study.MYMEDS);				if( study == null ) {			return false;		}		else {			studyId = study.getId();			return (study.getCurrent1() == 1);		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/LabsPopulator.java">
{@Override	public void populate() {		if(labs != null) {			for(Lab lab : labs) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entry.setClinicalStatement(populateClinicalStatement(Arrays.asList(lab)));				entries.add(entry);			}		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Room.java">
{public boolean isActive() {		return active;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/AbstractBodyPopulator.java">
{@Override	public void populate() {		Component3 component = makeSectionComponent();		//entries = new ArrayList<Entry>(); // Null Entry Cascade test line override		if(entries.isEmpty() && bodyConstants.SECTION_PRIORITY == SectionPriority.SHALL) {			ClinicalStatement clinicalStatement = populateNullFlavorClinicalStatement();			if(clinicalStatement != null) {				Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true), clinicalStatement);				entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));				entries.add(entry);			}		}		component.getSection().setEntry(entries);		clinicalDocument.getComponent().getBodyChoiceIfStructuredBody().getComponent().add(component);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/body/MedicationsPopulator.java">
{@Override	public void populate() {		for(List<Drug> medication : mapDrugs.values()) {			Entry entry = new Entry(x_ActRelationshipEntry.DRIV, new BL(true));			entry.setTemplateId(Arrays.asList(new II(bodyConstants.ENTRY_TEMPLATE_ID)));			entry.setClinicalStatement(populateClinicalStatement(medication));			entries.add(entry);		}		super.populate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/header/RecordTargetPopulator.java">
{@Override	public void populate() {		RecordTarget recordTarget = new RecordTarget();		PatientRole patientRole = new PatientRole();		Patient patient = new Patient();		recordTarget.setContextControlCode(ContextControl.OverridingPropagating);		recordTarget.setPatientRole(patientRole);		patientRole.setId(recordTargetModel.getIds());		patientRole.setAddr(recordTargetModel.getAddresses());		patientRole.setTelecom(recordTargetModel.getTelecoms());		patientRole.setPatient(patient);		patient.setName(recordTargetModel.getNames());		patient.setAdministrativeGenderCode(recordTargetModel.getGender());		patient.setBirthTime(recordTargetModel.getBirthDate());		patient.setLanguageCommunication(recordTargetModel.getLanguages());		clinicalDocument.setRecordTarget(new ArrayList<RecordTarget>(Arrays.asList(recordTarget)));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/hospitalReportManager/model/HRMDocumentSubClass.java">
{public boolean isActive() {    	return isActive;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/e2e/populator/header/HeaderPopulator.java">
{@Override	public void populate() {		// realmCode		CS<BindingRealm> binding = new CS<BindingRealm>();		binding.setCodeEx(new BindingRealm(Constants.DocumentHeader.E2E_DTC_CLINICAL_DOCUMENT_TYPE_REALM_CODE, null));		clinicalDocument.setRealmCode(new SET<CS<BindingRealm>>(binding));		// typeId		clinicalDocument.setTypeId(new II(				Constants.DocumentHeader.E2E_DTC_CLINICAL_DOCUMENT_TYPE_ID,				Constants.DocumentHeader.E2E_DTC_CLINICAL_DOCUMENT_TYPE_ID_EXTENSION));		// templateId		LIST<II> templateIds = new LIST<II>();		templateIds.add(new II(Constants.DocumentHeader.TEMPLATE_ID));		templateIds.add(templateId);		clinicalDocument.setTemplateId(templateIds);		// id		clinicalDocument.setId(UUID.randomUUID().toString().toUpperCase(), demographic.getDemographicNo().toString());		// code		clinicalDocument.setCode(code);		// title		clinicalDocument.setTitle("E2E-DTC Record of ".concat(demographic.getFirstName()).concat(" ").concat(demographic.getLastName()));		// effectiveTime		clinicalDocument.setEffectiveTime(new GregorianCalendar(), TS.MINUTE);		// confidentialityCode		clinicalDocument.setConfidentialityCode(x_BasicConfidentialityKind.Normal);		// languageCode		clinicalDocument.setLanguageCode(Constants.DocumentHeader.LANGUAGE_ENGLISH_CANADIAN);	}}
