public voidsetDemographicDao(DemographicDao demographicDao){	    this.demographicDao = demographicDao;    }this.preventionDao=preventionDaopublic voidsetDataExportDAO(DataExportDao dataExportDAO){    	this.dataExportDAO = dataExportDAO;    }public voidsetClinicDAO(ClinicDAO clinicDAO){    	this.clinicDAO = clinicDAO;    }Clinicclinic= clinicDAO.getClinic()List<DataExport>dataExportList= dataExportDAO.findAllByType(DataExportDao.CIHI_OMD4)dataExportList.addAll(dataExportDAO.findAllByType(DataExportDao.CIHI_PHC_VRS))Collections.sort(dataExportList)StringpatientSet= frm.getString("patientSet")if( patientSet == null || "".equals(patientSet) || patientSet.equals("-1")) {			frm.set("orgName", clinic.getClinicName());			frm.set("vendorId", properties.getProperty("vendorId", ""));			frm.set("vendorBusinessName", properties.getProperty("vendorBusinessName", ""));			frm.set("vendorCommonName", properties.getProperty("vendorCommonName", ""));			frm.set("vendorSoftware", properties.getProperty("softwareName", ""));			frm.set("vendorSoftwareCommonName", properties.getProperty("softwareCommonName", ""));			frm.set("vendorSoftwareVer", properties.getProperty("version", ""));			frm.set("installDate", properties.getProperty("buildDateTime", ""));			if( dataExportList.size() > 0 ) {				DataExport dataExport = dataExportList.get(dataExportList.size()-1);				frm.set("contactLName",dataExport.getContactLName());				frm.set("contactFName", dataExport.getContactFName());				frm.set("contactPhone", dataExport.getContactPhone());				frm.set("contactEmail", dataExport.getContactEmail());				frm.set("contactUserName", dataExport.getUser());			}			request.setAttribute("dataExportList", dataExportList);			return mapping.findForward("display");		}List<String>patientList= demoSets.getDemographicSet(frm.getString("patientSet"))Stringfilename= this.make(LoggedInInfo.getLoggedInInfoFromSession(request), frm, patientList, tmpDir)DataExportdataExport= new DataExport()dataExport.setContactLName(frm.getString("contactLName"))dataExport.setContactFName(frm.getString("contactFName"))dataExport.setContactPhone(frm.getString("contactPhone"))dataExport.setContactEmail(frm.getString("contactEmail"))dataExport.setUser(frm.getString("contactUserName"))dataExport.setType(frm.getString("extractType"))dataExport.setDaterun(runDate)dataExport.setFile(filename)dataExportDAO.persist(dataExport)dataExportList.add(dataExport)request.setAttribute("dataExportList", dataExportList)private Stringmake(LoggedInInfo loggedInInfo, DynaValidatorForm frm, List patientList, String tmpDir)throws Exception{		 HashMap<String,CiHiCdsDocument> xmlMap = new HashMap<String,CiHiCdsDocument>();		 HashMap<String,String> fileNamesMap = new HashMap<String,String>();		 String demoNo;		 Demographic demo;		 Calendar now = Calendar.getInstance();		 CiHiCds cihicds;		 CiHiCdsDocument cihiCdsDocument;		 int numPatients = patientList.size();		 log.info("Processing " + numPatients + " patients");		 for( int idx = 0; idx < numPatients; ++idx ) {			 demoNo = null;			 cihicds = null;			 Object obj = patientList.get(idx);			 if (obj instanceof String) {				demoNo = (String)obj;			 } else {				ArrayList<String> l2 = (ArrayList<String>)obj;				demoNo = l2.get(0);			 }			 demo = getDemographicDao().getDemographic(demoNo);			 if( demo == null ) {				 continue;			 }			 if( !xmlMap.containsKey(demo.getProviderNo())) {				 cihiCdsDocument = CiHiCdsDocument.Factory.newInstance();				 cihicds =  cihiCdsDocument.addNewCiHiCds();				 this.buildExtractInformation(frm, cihicds.addNewExtractInformation(), now);				 if( !this.buildProvider(demo.getProvider(),cihicds.addNewProvider(), fileNamesMap) ) {					 continue;				 }				 xmlMap.put(demo.getProviderNo(), cihiCdsDocument);			 }			 else {				 cihiCdsDocument = xmlMap.get(demo.getProviderNo());				 cihicds = cihiCdsDocument.getCiHiCds();			 }			 PatientRecord patientRecord = cihicds.addNewPatientRecord();			 if( !this.buildPatientDemographic(demo, patientRecord.addNewDemographics())) {				 continue;			 }			 log.info("Processing Demo " + idx + " of " + numPatients);			 this.buildAppointment(demo, patientRecord);			 this.buildFamilyHistory(demo, patientRecord);			 this.buildOngoingProblems(demo, patientRecord);			 this.buildRiskFactors(demo, patientRecord);			 this.buildAllergies(demo, patientRecord);			 this.buildCareElements(demo, patientRecord);						 this.buildProcedure(demo, patientRecord);			 this.buildLaboratoryResults(loggedInInfo, demo, patientRecord);			 this.buildMedications(demo, patientRecord);			 this.buildImmunizations(demo, patientRecord);		 }		 return this.makeFiles(xmlMap, fileNamesMap, tmpDir);	}private booleanbuildPatientDemographic(Demographic demo, Demographics xmlDemographics){        String seed = demo.getHin();        if( StringUtils.empty(seed) ) {        	seed = demo.getDemographicNo().toString() + demo.getFirstName() + demo.getLastName();        }        String strHash = this.buildHash(seed);        log.info("Demo Seed: " + seed + " HASH: " + strHash);		if( strHash.equals("")) {			return false;		}		HealthCard healthCard = xmlDemographics.addNewHealthCard();		healthCard.setNumber(strHash);		Calendar dob  = demo.getBirthDay();		xmlDemographics.setDateOfBirth(dob);		String sex = demo.getSex();		if( !"F".equalsIgnoreCase(sex) && !"M".equalsIgnoreCase(sex)) {			sex = "U";		}		cdsDtCihiPhcvrs.Gender.Enum enumDemo = cdsDtCihiPhcvrs.Gender.Enum.forString(sex);		xmlDemographics.setGender(enumDemo);		String spokenLanguage = demo.getSpokenLanguage();		if (StringUtils.filled(spokenLanguage) && Util.convertLanguageToCode(spokenLanguage)!=null){			xmlDemographics.setPreferredSpokenLanguage(Util.convertLanguageToCode(spokenLanguage));		}		Date statusDate = demo.getPatientStatusDate();		Calendar statusCal = Calendar.getInstance();		if( statusDate != null ) {			statusCal.setTime(statusDate);		}                PersonStatusCode personStatusCode = xmlDemographics.addNewPersonStatusCode();		String status = demo.getPatientStatus();		if( "AC".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihiPhcvrs.PersonStatus.A);		else if( "IN".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihiPhcvrs.PersonStatus.I);                else if( "DE".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihiPhcvrs.PersonStatus.D);                else {                    if ("MO".equalsIgnoreCase(status)) status = "Moved";                    else if ("FI".equalsIgnoreCase(status)) status = "Fired";                    personStatusCode.setPersonStatusAsPlainText(status);                }                if( statusDate != null ) {                        xmlDemographics.setPersonStatusDate(statusCal);		}                Date rosterDate = demo.getRosterDate();                Calendar rosterCal = Calendar.getInstance();                if( rosterDate != null ) {                        rosterCal.setTime(rosterDate);                        xmlDemographics.setEnrollmentDate(rosterCal);                }                Date rosterTermDate = demo.getRosterTerminationDate();                Calendar rosterTermCal = Calendar.getInstance();                if( rosterTermDate != null ) {                        rosterTermCal.setTime(rosterTermDate);                        xmlDemographics.setEnrollmentTerminationDate(rosterTermCal);                }                if (StringUtils.filled(demo.getPostal())) {                    PostalZipCode postalZipCode = xmlDemographics.addNewPostalAddress();                    postalZipCode.setPostalCode(StringUtils.noNull(demo.getPostal()).replace(" ",""));                }                return true;	}private voidbuildAppointment(Demographic demo, PatientRecord patientRecord){		//some weird values for appointment time preventing query from executing so just skip bad records		try {			List<Appointment> appointmentList = oscarAppointmentDao.getAppointmentHistory(demo.getDemographicNo());			Calendar cal = Calendar.getInstance();			Calendar startTime = Calendar.getInstance();			Date startDate;			for( Appointment appt: appointmentList) {				if( appt.getAppointmentDate() == null ) {					continue;				}				Appointments appointments = patientRecord.addNewAppointments();	            if (StringUtils.filled(appt.getReason())) appointments.setAppointmentPurpose(appt.getReason());				DateFullOrPartial dateFullorPartial = appointments.addNewAppointmentDate();				cal.setTime(appt.getAppointmentDate());				startDate = appt.getStartTime();				startTime.setTime(startDate);				cal.set(Calendar.HOUR_OF_DAY, startTime.get(Calendar.HOUR_OF_DAY));				cal.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));				dateFullorPartial.setFullDate(cal);			}		} catch( Exception e ) {			//empty		}	}private voidbuildFamilyHistory(Demographic demo, PatientRecord patientRecord ){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"FamHistory","MedHistory"});		String[] famHistory = new String[issueList.size()];		for( int idx = 0; idx < famHistory.length; ++idx ) {			famHistory[idx] = issueList.get(idx).getId().toString();		}		String intervention;	    String relationship;	    String age;	    Date startDate;	    String startDateFormat;	    List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), famHistory);		for( CaseManagementNote caseManagementNote: notesList) {			intervention = "";			relationship = "";			age = "";			startDateFormat = null;			startDate = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.TREATMENT)) {                    intervention = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RELATIONSHIP)) {                    relationship = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.AGEATONSET)) {                    age = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }            }            Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();            if( noteIssueList != null && noteIssueList.size() > 0 ) {                Iterator<CaseManagementIssue> i = noteIssueList.iterator();                CaseManagementIssue cIssue;                FamilyHistory familyHistory = patientRecord.addNewFamilyHistory();                while ( i.hasNext() ) {                	cIssue = i.next();                    if (cIssue.getIssue().getType().equals("system")) continue;                	StandardCoding standardCoding = familyHistory.addNewDiagnosisProcedureCode();                	standardCoding.setStandardCodingSystem(cIssue.getIssue().getType());                    String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                	standardCoding.setStandardCode(code);                	standardCoding.setStandardCodeDescription(cIssue.getIssue().getDescription());                    break;                }                if( startDate != null ) {                	this.putPartialDate(familyHistory.addNewStartDate(), startDate, startDateFormat);                }                if( !"".equalsIgnoreCase(age) ) {                	try {                		familyHistory.setAgeAtOnset(BigInteger.valueOf(Long.parseLong(age)));                	}catch(NumberFormatException e) {                		//empty                	}                }                //if( !hasIssue ) {  //Commenting this out.  I don't see why it's not there if it has an issue                    familyHistory.setProblemDiagnosisProcedureDescription(caseManagementNote.getNote());                //}                if( !"".equalsIgnoreCase(intervention)) {                    familyHistory.setTreatment(intervention);                }                if( !"".equalsIgnoreCase(relationship)) {                    familyHistory.setRelationship(relationship);                }            }        }    }PartialDatepd= partialDateDao.getPartialDate(tableName, tableId, fieldName)if( pd != null ) {    		putPartialDate(dfp, dateValue, pd.getFormat());    	}private voidbuildOngoingProblems(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"Concerns"});		String[] onGoingConcerns = new String[] {issueList.get(0).getId().toString()};		Date startDate;	    Date endDate;	    String startDateFormat;	    String endDateFormat;	    List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), onGoingConcerns);		for( CaseManagementNote caseManagementNote: notesList) {			startDate = null;			endDate = null;		    startDateFormat = null;		    endDateFormat = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {                    endDate = caseManagementNoteExt.getDateValue();                    endDateFormat = caseManagementNoteExt.getValue();                }            }            Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();            if( noteIssueList != null && noteIssueList.size() > 0 ) {                Iterator<CaseManagementIssue> i = noteIssueList.iterator();                CaseManagementIssue cIssue;                ProblemList problemList = patientRecord.addNewProblemList();                while ( i.hasNext() ) {                	cIssue = i.next();                    if (cIssue.getIssue().getType().equals("system")) continue;                	StandardCoding standardCoding = problemList.addNewDiagnosisCode();                	standardCoding.setStandardCodingSystem(cIssue.getIssue().getType());                    String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                	standardCoding.setStandardCode(code);                	standardCoding.setStandardCodeDescription(cIssue.getIssue().getDescription());                    break;                }                if( startDate != null ) {                	this.putPartialDate(problemList.addNewOnsetDate(), startDate, startDateFormat);                }                if( endDate != null ) {                	this.putPartialDate(problemList.addNewResolutionDate(), endDate, endDateFormat);                }                //if( !hasIssue ) {  //Commenting out another one                    problemList.setProblemDiagnosisDescription(caseManagementNote.getNote());                //}            }        }		this.buildOngoingproblemsDiseaseRegistry(demo, patientRecord);    }private voidbuildOngoingproblemsDiseaseRegistry(Demographic demo, PatientRecord patientRecord){		List<Dxresearch> dxResearchList = dxresearchDAO.getDxResearchItemsByPatient(demo.getDemographicNo());		String description;		String dateFormat = "yyyy-MM-dd";		Date date;		Character status;		for( Dxresearch dxResearch : dxResearchList ) {			status = dxResearch.getStatus();			if( status.compareTo('D') == 0 ) {				continue;			}			ProblemList problemList = patientRecord.addNewProblemList();			StandardCoding standardCoding = problemList.addNewDiagnosisCode();			standardCoding.setStandardCodingSystem(dxResearch.getCodingSystem());			standardCoding.setStandardCode(dxResearch.getDxresearchCode());			if( "icd9".equalsIgnoreCase(dxResearch.getCodingSystem()) ) {				List<Icd9>icd9Code = icd9Dao.getIcd9Code(standardCoding.getStandardCode());				if( !icd9Code.isEmpty() ) {					description = icd9Code.get(0).getDescription();					standardCoding.setStandardCodeDescription(description);				}			}			date = dxResearch.getStartDate();			if( date != null ) {				this.putPartialDate(problemList.addNewOnsetDate(), date, dateFormat);			}			if( status.compareTo('C') == 0 ) {				date = dxResearch.getUpdateDate();				if( date != null ) {					this.putPartialDate(problemList.addNewResolutionDate(), date, dateFormat);				}			}		}	}private voidbuildRiskFactors(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"RiskFactors"});		String[] riskFactor = new String[] {issueList.get(0).getId().toString()};		Date startDate;	    Date endDate;	    String startDateFormat;	    String endDateFormat;		List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), riskFactor);		for( CaseManagementNote caseManagementNote: notesList) {			startDate = null;			endDate = null;		    startDateFormat = null;		    endDateFormat = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {                    endDate = caseManagementNoteExt.getDateValue();                    endDateFormat = caseManagementNoteExt.getValue();                }            }            RiskFactors riskFactors = patientRecord.addNewRiskFactors();            if( startDate != null ) {            	this.putPartialDate(riskFactors.addNewStartDate(), startDate, startDateFormat);            }            if( endDate != null ) {            	this.putPartialDate(riskFactors.addNewEndDate(), endDate, endDateFormat);            }            riskFactors.setRiskFactor(caseManagementNote.getNote());		}	}private voidbuildAllergies(Demographic demo, PatientRecord patientRecord){		String[] severity = new String[] {"MI","MO","LT","NO"};		List<Allergy> allergies = allergyDAO.findAllergies(demo.getDemographicNo());		int index;		Date date;        for( Allergy allergy: allergies ) {            	AllergiesAndAdverseReactions xmlAllergies = patientRecord.addNewAllergiesAndAdverseReactions();                Integer typeCode = allergy.getTypeCode();                if (typeCode == null) {                    xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihiPhcvrs.PropertyOfOffendingAgent.UK);                } else {                    if (typeCode == 13) xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihiPhcvrs.PropertyOfOffendingAgent.DR);                    else if(typeCode == 0) xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihiPhcvrs.PropertyOfOffendingAgent.ND);                    else xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihiPhcvrs.PropertyOfOffendingAgent.UK);                }        	xmlAllergies.setOffendingAgentDescription(allergy.getDescription());        	try {                index = Integer.parseInt(allergy.getSeverityOfReaction());            }catch(Exception e ) {                index = 4;            }        	if( index > 0 ) {        		index -= 1;        	}        	xmlAllergies.setSeverity(cdsDtCihiPhcvrs.AdverseReactionSeverity.Enum.forString(severity[index]));        	date = allergy.getStartDate();        	if( date != null ) {        		this.putPartialDate(xmlAllergies.addNewStartDate(), date, PartialDate.ALLERGIES, (int)allergy.getId(), PartialDate.ALLERGIES_STARTDATE);        	}        }	}private voidbuildCareElements(Demographic demo, PatientRecord patientRecord){		List<Measurements> measList = ImportExportMeasurements.getMeasurements(demo.getDemographicNo().toString());		CareElements careElements = patientRecord.addNewCareElements();		Calendar cal = Calendar.getInstance();		Date date = null;		for (Measurements measurement : measList) {			if( measurement.getType().equalsIgnoreCase("BP")) {				String[] sysdiabBp = measurement.getDataField().split("/");				if( sysdiabBp.length == 2 ) {					cdsDtCihiPhcvrs.BloodPressure bloodpressure = careElements.addNewBloodPressure();					bloodpressure.setSystolicBP(sysdiabBp[0]);					bloodpressure.setDiastolicBP(sysdiabBp[1]);					date = measurement.getDateObserved();					if( date == null ) {						date = measurement.getDateEntered();					}					cal.setTime(date);					bloodpressure.setDate(cal);				}			}			else if( measurement.getType().equalsIgnoreCase("HT")) {				cdsDtCihiPhcvrs.Height height = careElements.addNewHeight();				height.setHeight(measurement.getDataField());				height.setHeightUnit("cm");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				height.setDate(cal);			}			else if( measurement.getType().equalsIgnoreCase("WT")) {				cdsDtCihiPhcvrs.Weight weight = careElements.addNewWeight();				weight.setWeight(measurement.getDataField());				weight.setWeightUnit("Kg");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				weight.setDate(cal);			}			else if( measurement.getType().equalsIgnoreCase("WAIS") || measurement.getType().equalsIgnoreCase("WC") ) {				cdsDtCihiPhcvrs.WaistCircumference waist = careElements.addNewWaistCircumference();				waist.setWaistCircumference(measurement.getDataField());				waist.setWaistCircumferenceUnit("cm");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				waist.setDate(cal);			}			else if( StringUtils.filled(measurement.getType()) ) {				cdsDtCihiPhcvrs.Observation observation = careElements.addNewObservation();				observation.setObservationName(measurement.getType());				observation.setObservationData(measurement.getDataField());				observation.setObservationUnit(measurement.getMeasuringInstruction());				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				observation.setDate(cal);			}		}	}private voidbuildProcedure(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"MedHistory"});		String[] medhistory = new String[] {issueList.get(0).getId().toString()};		Procedure procedure;		ProblemList problemlist;		List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), medhistory);		for( CaseManagementNote caseManagementNote: notesList) {                    procedure = null;                    problemlist = null;                    List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());                    String keyval;                    for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                        if (procedure!=null && procedure.getProcedureDate()!=null) break;                        if (problemlist!=null && problemlist.getOnsetDate()!=null && problemlist.getResolutionDate()!=null) break;                        keyval = caseManagementNoteExt.getKeyVal();                        if( caseManagementNoteExt.getDateValue() != null ) {	                        if( keyval.equals(CaseManagementNoteExt.PROCEDUREDATE)) {	                            procedure = patientRecord.addNewProcedure();	                            this.putPartialDate(procedure.addNewProcedureDate(), caseManagementNoteExt);	                        } else	                        if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {	                            if (problemlist==null) problemlist = patientRecord.addNewProblemList();	                            this.putPartialDate(problemlist.addNewOnsetDate(), caseManagementNoteExt);	                        } else	                        if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {	                            if (problemlist==null) problemlist = patientRecord.addNewProblemList();	                            this.putPartialDate(problemlist.addNewResolutionDate(), caseManagementNoteExt);	                        }                        }                    }                    if (procedure==null && problemlist==null) continue;                    Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();                    if( noteIssueList != null && noteIssueList.size() > 0 ) {                        Iterator<CaseManagementIssue> i = noteIssueList.iterator();                        CaseManagementIssue cIssue;                        while ( i.hasNext() ) {                            cIssue = i.next();                            if (cIssue.getIssue().getType().equals("system")) continue;                            String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                            if (procedure!=null) {                                StandardCoding procedureCode = procedure.addNewProcedureCode();                                procedureCode.setStandardCodingSystem(cIssue.getIssue().getType());                                procedureCode.setStandardCode(code);                                procedureCode.setStandardCodeDescription(cIssue.getIssue().getDescription());                                break;                            }                            if (problemlist!=null) {                                StandardCoding diagnosisCode = problemlist.addNewDiagnosisCode();                                diagnosisCode.setStandardCodingSystem(cIssue.getIssue().getType());                                diagnosisCode.setStandardCode(code);                                diagnosisCode.setStandardCodeDescription(cIssue.getIssue().getDescription());                                break;                            }                        }                    }                    if (procedure!=null) procedure.setProcedureInterventionDescription(caseManagementNote.getNote());                    if (problemlist!=null) problemlist.setProblemDiagnosisDescription(caseManagementNote.getNote());		}	}private voidbuildLaboratoryResults(LoggedInInfo loggedInInfo, Demographic demo, PatientRecord patientRecord){		CommonLabResultData comLab = new CommonLabResultData();		ArrayList<LabResultData> labdocs = comLab.populateLabsData(loggedInInfo, demo.getProviderNo(), String.valueOf(demo.getDemographicNo()), demo.getFirstName(), demo.getLastName(), demo.getHin(), "", "");		MessageHandler handler;		int i;		int j;		int k;		int OBRCount;		int obxCount;		String obxName, identifier, obxResult;		Date dateTime;		log.info("Building lab results for " + demo.getDemographicNo() + " " + demo.getFormattedName());		for( LabResultData labData : labdocs ) {			try {				handler = Factory.getHandler(labData.segmentID);				ArrayList<String> headers = handler.getHeaders();				OBRCount = handler.getOBRCount();	            for(i=0;i<headers.size();i++){	            	for ( j=0; j < OBRCount; j++){	            		obxCount = handler.getOBXCount(j);	                    for (k=0; k < obxCount; k++){	                    	if( handler.getOBXResultStatus(j, k) == null || handler.getObservationHeader(j, k) == null ) {	                    		continue;	                    	}	                    	if ( !handler.getOBXResultStatus(j, k).equals("DNS") /*&& !obxName.equals("")*/ && handler.getObservationHeader(j, k).equals(headers.get(i))){ // <<--  DNS only needed for MDS messages	                    		LaboratoryResults labResults = patientRecord.addNewLaboratoryResults();	                        	obxName = handler.getOBXName(j, k);	                            identifier = "";	                            if( StringUtils.filled(obxName)) {	                            	labResults.setTestNameReportedByLab(obxName);	                            }	                            if(handler.getOBXValueType(j,k) != null &&  handler.getOBXValueType(j,k).equalsIgnoreCase("NAR")) {	                            	obxResult = handler.getOBXResult( j, k);	                            	if( StringUtils.filled(obxResult)) {	                            		LaboratoryResults.Result result = labResults.addNewResult();	                            		result.setValue(obxResult);	                            	}	                            }else if(handler.getOBXValueType(j,k) != null &&  handler.getOBXValueType(j,k).equalsIgnoreCase("FT")){	                                String[] dividedString  =divideStringAtFirstNewline(handler.getOBXResult( j, k));	                                obxResult = dividedString[0];	                                identifier = handler.getOBXIdentifier(j, k);	                            }	                            else {	                            	obxResult = handler.getOBXResult( j, k);	                            	identifier = handler.getOBXIdentifier(j,k);	                            }	                            if( StringUtils.filled(identifier)) {	                            	labResults.setLabTestCode(identifier);	                            }	                            LaboratoryResults.Result result = labResults.addNewResult();	                    		result.setValue(obxResult);	                    		result.setUnitOfMeasure(handler.getOBXUnits(j, k));	                    		String range = handler.getOBXReferenceRange(j, k);	                    		if( StringUtils.filled(range)) {	                    			LaboratoryResults.ReferenceRange refRange = labResults.addNewReferenceRange();	                    			String rangeLimits[] = range.split("-");	                    			if( rangeLimits.length == 2 ) {	                    				refRange.setLowLimit(rangeLimits[0]);	                        			refRange.setHighLimit(rangeLimits[1]);	                    			}	                    			else {	                    				refRange.setLowLimit(range);	                    				refRange.setHighLimit(range);	                    			}	                    		}	                    		dateTime = UtilDateUtilities.StringToDate(handler.getTimeStamp(j, k),"yyyy-MM-dd HH:mm:ss");	                			if (dateTime==null) {	                            	dateTime = UtilDateUtilities.StringToDate(handler.getServiceDate(),"yyyy-MM-dd");	                            }	                    		cdsDtCihiPhcvrs.DateFullOrDateTime collDate = labResults.addNewCollectionDateTime();	                            if (dateTime!=null) {	                            	collDate.setFullDate(Util.calDate(dateTime));	                            }	                            else {	                            	collDate.setFullDate(Util.calDate("0001-01-01"));	                            }	                        }	                    }	            	}	            }			}catch( Exception e ) {				log.error("Skipping Lab " + labData.segmentID);			}		}	}private voidbuildMedications(Demographic demo, PatientRecord patientRecord){		MedicationsAndTreatments medications;		RxPrescriptionData.Prescription[] pa = new RxPrescriptionData().getPrescriptionsByPatient(Integer.parseInt(demo.getDemographicNo().toString()));		String drugname;		String customname;		String dosage;		String[] strength;		int sep;		for(int p = 0; p<pa.length; ++p) {			drugname = pa[p].getBrandName();			customname = pa[p].getCustomName();			if (StringUtils.empty(drugname) && StringUtils.empty(customname)) continue;			medications = patientRecord.addNewMedicationsAndTreatments();			if (StringUtils.filled(drugname)) medications.setDrugName(drugname);			else medications.setDrugDescription(customname);			Date writtenDate = pa[p].getWrittenDate();			if (writtenDate!=null) {	        	String dateFormat = partialDateDao.getFormat(PartialDate.DRUGS, pa[p].getDrugId(), PartialDate.DRUGS_WRITTENDATE);	        	this.putPartialDate(medications.addNewPrescriptionWrittenDate(), writtenDate, dateFormat);			}			if (StringUtils.filled(pa[p].getDosage())) {            	strength = pa[p].getDosage().split(" ");            	cdsDtCihiPhcvrs.DrugMeasure drugM = medications.addNewStrength();            	if (Util.leadingNum(strength[0]).equals(strength[0])) {//amount & unit separated by space            		drugM.setAmount(strength[0]);            		if (strength.length>1) drugM.setUnitOfMeasure(strength[1]);            		else drugM.setUnitOfMeasure("unit"); //UnitOfMeasure cannot be null            	} else {//amount & unit not separated, probably e.g. 50mg / 2tablet            		if (strength.length>1 && strength[1].equals("/")) {            			if (strength.length>2) {            				String unit1 = Util.leadingNum(strength[2]).equals("") ? "1" : Util.leadingNum(strength[2]);            				String unit2 = Util.trailingTxt(strength[2]).equals("") ? "unit" : Util.trailingTxt(strength[2]);                    		drugM.setAmount(Util.leadingNum(strength[0])+"/"+Util.leadingNum(strength[2]));                    		drugM.setUnitOfMeasure(Util.trailingTxt(strength[0])+"/"+unit2);            			}            		} else {                		drugM.setAmount(Util.leadingNum(strength[0]));                		drugM.setUnitOfMeasure(Util.trailingTxt(strength[0]));            		}            	}			}	        dosage = StringUtils.noNull(pa[p].getDosageDisplay());	        medications.setDosage(dosage);	        medications.setDosageUnitOfMeasure(StringUtils.noNull(pa[p].getUnit()));	        medications.setForm(StringUtils.noNull(pa[p].getDrugForm()));	        medications.setFrequency(StringUtils.noNull(pa[p].getFreqDisplay()));	        medications.setRoute(StringUtils.noNull(pa[p].getRoute()));	        medications.setNumberOfRefills(String.valueOf(pa[p].getRepeat()));	        YnIndicatorAndBlank patientCompliance = medications.addNewPatientCompliance();	        if (pa[p].getPatientCompliance()==null) {	        	patientCompliance.setBlank(cdsDtCihiPhcvrs.Blank.X);	        } else {	        	patientCompliance.setBoolean(pa[p].getPatientCompliance());	        }		}	}private voidbuildImmunizations(Demographic demo, PatientRecord patientRecord){    	List<Prevention> preventionsList = getPreventionDao().findNotDeletedByDemographicId(demo.getDemographicNo());         for( Prevention prevention: preventionsList ) {             List<PreventionExt> listPreventionExt = preventionExtDao.findByPreventionId(prevention.getId());        	 Immunizations immunizations = patientRecord.addNewImmunizations();        	 for( PreventionExt preventionExt : listPreventionExt ) {	        	 if (preventionExt.getkeyval().equalsIgnoreCase("name") &&  StringUtils.filled(preventionExt.getVal())) {	        		immunizations.setImmunizationName(preventionExt.getVal());	        	 }else{	        	    immunizations.setImmunizationName(prevention.getPreventionType());	        	 }	        	 if (preventionExt.getkeyval().equalsIgnoreCase("lot") &&  StringUtils.filled(preventionExt.getVal())) {	        		 immunizations.setLotNumber(preventionExt.getVal());	        	 }	         }        	 if (prevention.getPreventionDate()!=null) {        		 DateFullOrPartial dateFullorPartial = immunizations.addNewDate();        		 dateFullorPartial.setFullDate(Util.calDate(prevention.getPreventionDate()));        	 }        	 YnIndicator refusedIndicator = immunizations.addNewRefusedFlag();             if( prevention.isRefused() ) {            	 refusedIndicator.setBoolean(true);             }             else {                 refusedIndicator.setBoolean(false);             }         }    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Lab.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/query/Column.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/Intake.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getNode()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/web/PatientListApptItemBean.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Encounter.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/caisi/model/BaseObject.java">
{public String toString() {        return ToStringBuilder.reflectionToString(this,                ToStringStyle.MULTI_LINE_STYLE);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxWriteScriptForm.java">
{public int getDemographicNo() {        return this.demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxProviderData.java">
{public String getFirstName(){            return this.firstName;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/conditionValue/DSValueString.java">
{@Override    public String toString() {        StringBuilder result = new StringBuilder();        if (this.getValueType() != null) result.append(this.getValueType() + ":");        if (this.getValueUnit() == null) {  //shouldn't be a unit anyways, otherwise must be a number, so should do statement (i.e. =5 kg)            result.append("'" + this.getValue() + "'");        } else {            result.append(this.getValue());            result.append(" " + this.getValueUnit());        }        return result.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/CihiExportAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/PreventionTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RourkeExportAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/phr/model/PHRDocument.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RoomDemographicPK.java">
{@Override	public String toString() {		return ("demographicNo=" + demographicNo + ", roomId=" + roomId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/data/EFormBase.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/common/OscarDemographicLinkTag.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicContactFewTo1.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Prescribe.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/phr/util/PHRVerificationTag.java">
{public String getDemographicNo()    {        return demoNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicExtArchive.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetDrug.java">
{public int getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/consultations/ConsultationData.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/inbox/InboxManagerQuery.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Episode.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/ICLHandler.java">
{public String getSex(){				try {            return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));				} catch (Exception e) {				    logger.error("Exception finding sex", e);						return("");				}    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/ProgramClientRestriction.java">
{public int getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/quickbilling/QuickBillingBCFormBean.java">
{@Override	public String toString() {		return (				" PROVIDER="+billingProvider+				" PROVIDER NUMBER="+billingProviderNo+				" SERVICE DATE="+serviceDate+				" VISIT LOCATION="+visitLocation+ 				" IS HEADER SET="+isHeaderSet+				" CREATOR="+creator+				" BILL DATA="+billingData.size()+" ENTRY(S)"		);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/pharmaForms/formBPMH/bean/BpmhDrug.java">
{@Override	public String toString() {		return ToStringBuilder.reflectionToString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OcanClientForm.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/bc/PathNet/PathnetLabTest.java">
{public String getDemographicNo(){        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/query/RangeUpperLimit.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxDrugData.java">
{@Override			public String toString() {				return ToStringBuilder.reflectionToString(this);			}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxDrugData.java">
{@Override		public String toString() {			return ToStringBuilder.reflectionToString(this);		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxDrugData.java">
{@Override		public String toString() {			return ToStringBuilder.reflectionToString(this);		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxDrugData.java">
{@Override		public String toString() {			return ToStringBuilder.reflectionToString(this);		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxDrugData.java">
{@Override		public String toString() {			return ToStringBuilder.reflectionToString(this);		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CasemgmtNoteLock.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/WaitingList.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/match/vacancy/VacancyData.java">
{@Override	public String toString() {		final int maxLen = 16;		return "VacancyData [vacancy_id="				+ vacancy_id				+ ", program_id="				+ program_id				+ ", vacancyData="				+ (vacancyData != null ? toString(vacancyData.entrySet(),						maxLen) : null) + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FaxJob.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/appt/tld/NextApptTag.java">
{public String getDemographicNo() {		return demoNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProviderPropertyTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchResult.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/TicklerTo1.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicMergedTo1.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MessageHandler.java">
{public String getSex();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicContact.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/indivica/olis/queries/Query.java">
{public String getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Billingmaster.java">
{public String toString() {        return ToStringBuilder.reflectionToString(this,                ToStringStyle.MULTI_LINE_STYLE);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BatchBilling.java">
{public int getDemographicNo() {		return this.demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SurveyData.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Drug.java">
{@Override	public String toString() {		return ToStringBuilder.reflectionToString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/IndicatorBean.java">
{@Override	public String toString() {		return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AppointmentTo1.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxPrescriptionData.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/query/Parameter.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Hl7Link.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/FlowSheetItem.java">
{@Override    public String toString(){        return " MEASUREMENT TYPE :"+measurementType +" PREV TYPE :"+preventionType+" dsRulesFileName :"+dsRulesFileName+" ruleBASE :"+ruleBase;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/PhrVerificationTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/ON/model/BillingONCHeader2.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/Echart.java">
{public String getDemographicNo(){          return this.demographicNo;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/FormTo1.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Admission.java">
{@Override    public String toString() {    	return super.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/util/TargetColour.java">
{public String toString(){         return "indicationColor "+getIndicationColor() ;//+" strength "+strength+" text "+text+" ruleName "+ruleName+" measurement "+measurement;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMDS/data/PatientData.java">
{public String getSex() {			return this.sex;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/User.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/managers/DayWorkSchedule.java">
{@Override	public String toString()	{		StringBuilder sb=new StringBuilder();				sb.append("isHoliday=");		sb.append(isHoliday);		sb.append(", timeSlotDurationMin=");		sb.append(timeSlotDurationMin);		sb.append(", timeBlocks=(");		for (Entry<Calendar, Character> entry : timeSlots.entrySet())		{			sb.append('[');			sb.append(entry.getKey().getTime());			sb.append('=');			sb.append(entry.getValue());			sb.append(']');		}		sb.append(")");				return(sb.toString());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/util/Recommendation.java">
{public String toString(){         return " strength "+strength+" text "+text+" ruleName "+ruleName+" measurement "+measurement;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/AbstractModel.java">
{@Override    public String toString()	{		return(ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/match/client/ClientData.java">
{@Override	public String toString() {		final int maxLen = 20;		return "ClientData [clientId="				+ clientId				+ ", formId="				+ formId				+ ", clientData="				+ (clientData != null ? toString(clientData.entrySet(), maxLen)						: null) + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/wlmatch/MatchBO.java">
{@Override    public String toString() {	    return "MatchBO [clientID=" + clientID + ", clientName=" + clientName + 	    ", daysInWaitList=" + daysInWaitList + ", daysSinceLastContact=" + daysSinceLastContact + 	    ", contactAttempts=" + contactAttempts + ", percentageMatch=" + percentageMatch + ",proportion=" + proportion + 	    ", formDataID=" + formDataID + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/model/BillingInr.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/DSDemographicAccess.java">
{public String getSex() {        return getDemographicData(loggedInInfo).getSex();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/dms/EDoc.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeNodeType.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getType()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Billingreferral.java">
{public String getLastName() {        return this.lastName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/service/NoteSelectionResult.java">
{@Override    public String toString() {	    return "NoteSelectionResult [moreNotes=" + moreNotes + ", notes=" + notes + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/web/ConsultationReportFormBean.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/ClientListsReportFormBean.java">
{public String toString() {		StringBuilder sb = new StringBuilder();		try {			for (Method method : getClass().getDeclaredMethods()) {				String methodName = method.getName();				if (method.getParameterTypes().length == 0 && (methodName.startsWith("get") || methodName.startsWith("is") || methodName.startsWith("has"))) {					if (sb.length() > 0) sb.append(", ");					sb.append(methodName);					sb.append("()=");					sb.append(method.invoke(this));				}			}		}		catch (Exception e) {			sb.append(e.getMessage());		}		return(sb.toString());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/pageUtil/PatientEndYearStatementBean.java">
{public String getHin() {		return hin;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/DemographicSearchResultTransformer.java">
{public DemographicDao getDemographicDao() {		return demographicDao;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformTestBook.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/common/PassIntakeFormVars.java">
{public String getDemographicNo()	{		return this.demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/DefaultGenericHandler.java">
{public String getSex(){        try{            return(getString(terser.get("/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FacilityDemographicPrimaryKey.java">
{@Override	public String toString() {		return ("facilityId=" + facilityId + ", demographicId=" + demographicId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AppointmentArchiveTransfer.java">
{public int getDemographicNo() {		return (demographicNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaseManagementTmpSave.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/AppointmentArchive.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DemographicTransfer.java">
{public Date getRosterTerminationDate() {    	return (rosterTerminationDate);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/dms/EDocFactory.java">
{@Override        public String toString() { return statusCharacter + ""; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OcanStaffForm.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicQueryFavourite.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeAnswerValidation.java">
{@Override    public String toString() {        return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getType()).append(")").toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProviderTransfer.java">
{public String getSex() {		return (sex);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OscarToOscarHl7V2Handler.java">
{public String getSex() {	    return chainnedMessageAdapter.getSex();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/PFHTHandler.java">
{public String getSex(){	        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MeasurementsDeleted.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/tld/DemographicNameAgeTag.java">
{public String getDemographicNo()    {        return demoNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/PanelBean.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Consent.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/Agency.java">
{public String toString() {		return super.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MyGroupPrimaryKey.java">
{@Override	public String toString() {		return ("MyGroupNo=" + myGroupNo + ", providerNo=" + providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public static OscarProperties getInstance() {		return oscarProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/mchcv/HCValidationResult.java">
{public String getLastName() {        return lastName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RoomDemographic.java">
{@Override	public String toString() {		return ToStringBuilder.reflectionToString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CDLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/InstitutionDepartmentPK.java">
{@Override	public String toString() {		return ("institutionId=" + institutionId + ", departmentId=" + departmentId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Demographic.java">
{public Date getRosterTerminationDate() {        return rosterTerminationDate;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MDSHandler.java">
{public String getSex(){        try{            return(getString(DynamicHapiLoaderUtils.terserGet(terser,"/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CtlDocumentPK.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/sharingcenter/model/ExportedDocument.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/EChart.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceAnswerForm.java">
{public java.lang.String getDemographicNo() {      return demographicNo;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OscarAnnotation.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hl7TextInfo.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/TDISHandler.java">
{public String getSex() {		return getString(pat_25.getPID().getAdministrativeSex().getValue());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeNodeTemplate.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getType()).append(", ").append(getLabel()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/EChartNoteEntry.java">
{public String toString() {		return "NoteEntry:" + getType() + ":" + getId() + ":" + getDate();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONCHeader1.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformOcularProcedure.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Tickler.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONEAReport.java">
{public String getHin() {        return this.hin;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportTempPK.java">
{@Override	public String toString() {		return ("demographicNo=" + demographicNo + ", edb=" + edb);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/MSP/TeleplanSubmission.java">
{public String toString(){        StringBuilder sb = new StringBuilder();        sb.append("msp file content\n");        sb.append(this.getMspFile());        sb.append("\n");        sb.append("html file content\n");        sb.append(this.getHtmlFile());        sb.append("\n");        sb.append("billing to be marked\n");        sb.append(StringUtils.getCSV(this.billingToBeMarkedAsBilled));        sb.append("\n");        sb.append("billingmaster");        sb.append(StringUtils.getCSV(this.billingmasterToBeMarkedAsBilled));        sb.append("\n");        sb.append("Ending Seq: "+sequenceNum+" total Claims: "+this.getNumClaims()+ " BigTotal: "+this.getBigTotal()+" # in Log: "+this.logList.size());        return sb.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/GraphPlot.java">
{@Override	public String toString() {		return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicExtTo1.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/bean/WLPatientWaitingListBean.java">
{public String getDemographicNo(){           return demographicNo;       }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Form.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingErrorRepData.java">
{public String getHin() {		return hin;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxPatientData.java">
{public String getSex() {			if (demographic != null) return demographic.getSex();			else return "";		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/myoscar/MyOscarMeasurement.java">
{@Override    public String toString() {		Element root = getRoot();		if (root == null) {			return getMeasurement().getDataField();		}	    return toReadableString(root);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/FacilityTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PatientLabRouting.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/event/DemographicUpdateEvent.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ticklers/web/TicklerQuery.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicTo1.java">
{public Date getRosterTerminationDate() {		return rosterTerminationDate;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MEDITECHHandler.java">
{@Override	public String getSex(){		return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxSessionBean.java">
{public int getDemographicNo() {        return this.demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/GroupNoteLink.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProfessionalSpecialist.java">
{public String getLastName() {    	return lastName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/SpireHandler.java">
{public String getSex(){		String sex = getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue());				if (sex.length() > 0)			sex = sex.substring(0, 1);		        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/DemographicExportForm.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/GDMLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementSearchBean.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMDS/data/PatientInfo.java">
{@Override	public String toString() {		// TODO Auto-generated method stub		return lastName + ("".equals(lastName) ? "" : ", ") + firstName;				}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/on/Spire/SpireLabTest.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/consultations/ConsultationResponseSearchFilter.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultationServices.java">
{@Override    public String toString() {        return "org.oscarehr.common.model.ConsultationServices[serviceId=" + serviceId + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderFacilityPK.java">
{@Override	public String toString() {		return ("providerNo=" + providerNo + ", facilityId=" + facilityId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/service/NoteSelectionCriteria.java">
{@Override    public String toString() {	    return "NoteSelectionCriteria [maxResults=" + maxResults + ", firstResult=" + firstResult + ", demographicId=" + demographicId + ", userRole=" + userRole + ", userName=" + userName + ", noteSort=" + noteSort + ", programId=" + programId + ", roles=" + roles + ", providers=" + providers + ", issues=" + issues + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/MSPBill.java">
{public String getHin() {    return hin;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicContactTo1.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/bc/PathNet/HL7/Message.java">
{public String toString() {		return pid.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicAccessory.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FunctionalCentreAdmission.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/hl7/handlers/PhsStarHandler.java">
{public String getSex() {		try {			String var = this.extractOrEmpty("PID-8");			return var;		}catch(Exception e) {			return "";		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/search/pageUtil/EctDemographicSearchForm.java">
{public String getSex()    {        if(sex == null)            sex = new String();        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderArchive.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OscarJobType.java">
{public String toString() {		return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/pageUtil/EctIncomingConsultationForm.java">
{public String getDemographicNo()    {        if(demographicNo == null)            demographicNo = new String();        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingClaimHeader1Data.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/CheckSurveillanceForm.java">
{public java.lang.String getDemographicNo() {      log.debug("CheckSurveillanceForm.getDemographicNo called :"+demographicNo);      return demographicNo;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/hl7/model/PatientId.java">
{public String toString() {		return "Patient Identifier: " + id + " " + authority + " " + typeId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/match/vacancy/VacancyTemplateData.java">
{@Override	public String toString() {		final int maxLen = 20;		return "VacancyTemlateData [weight="				+ weight				+ ", param="				+ param				+ ", values="				+ (values != null ? values.subList(0,						Math.min(values.size(), maxLen)) : null)				+ ", ranges="				+ (ranges != null ? ranges.subList(0,						Math.min(ranges.size(), maxLen)) : null) + ", option="				+ option + ", range=" + range + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Prescription.java">
{public String getDemographicNo() {    return demographicNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicPharmacy.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AgeTo1.java">
{@Override	public String toString() {		return years + " Years, " + months + " Months, " + days + " Days";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SentToPHRTracking.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/pharmaForms/formBPMH/bean/BpmhFormBean.java">
{@Override	public String toString() {		return ToStringBuilder.reflectionToString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicCustArchive.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/event/DemographicCreateEvent.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/spireHapiExt/v23/segment/ZDS.java">
{public String toString() {		String text = "";		try {			text += super.getField(1, 0).encode() + " | ";			text += super.getField(2, 0).encode() + " | ";			text += super.getField(3, 0).encode() + " | ";			text += super.getField(4, 0).encode() + " | ";		} catch (Exception e) {			logger.error("Error converting ZDS segment to string: " + e.toString());		}		return text;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RaDetail.java">
{public String getHin() {    	return hin;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RemoteIntegratedDataCopy.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformFollowUp.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/hospitalReportManager/model/HRMDocumentToDemographic.java">
{public String getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/on/LabResultData.java">
{public String getSex(){		return this.sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeAnswer.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getValue()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FormLabReq07.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/DrilldownBean.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FormBPMH.java">
{@Override	public String toString() {		return ToStringBuilder.reflectionToString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RoomBedPK.java">
{@Override	public String toString() {		return ("bedId=" + bedId + ", roomId=" + roomId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Clinic.java">
{public String toString(){       return "clinicName " +clinicName +    " clinicAddress  " +clinicAddress+    " clinicCity " +clinicCity+    " clinicPostal " +clinicPostal+    " clinicPhone " +clinicPhone+    "  clinicFax " +clinicFax+    " clinicLocationCode " +clinicLocationCode+    " status " +status+    " clinicProvince " +clinicProvince+    " clinicDelimPhone " +clinicDelimPhone+    " clinicDelimFax " +clinicDelimFax;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/IHAHandler.java">
{@Override    public String getSex(){        try{            return(getString(terser.get("/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/StaffForm.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Relationships.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/PrescriptionTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Appointment.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RehabStudy2004.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IntegratorProgress.java">
{public String toString() {		return "IntegratorProgress: (id="+getId()+", status="+getStatus()+")";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MdsMSH.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/TableModification.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderSitePK.java">
{public String toString() {		return ("ProviderNo=" + providerNo + ", siteId=" + siteId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Dxresearch.java">
{public Integer getDemographicNo() {        return this.demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/Measurements.java">
{public Long getDemographicNo() {	return this.demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarPrevention/tld/PreventionTag.java">
{public String getDemographicNo()    {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/QueryAppender.java">
{@Override	public String toString() {		return getQuery();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/Appender.java">
{@Override	public String toString() {		return getBuffer().toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/PreIntakeForm.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/bean/BillingClaimsErrorReportBean.java">
{public String getHin(){           return hin;           }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MsgDemoMapPK.java">
{@Override	public String toString() {		return ("MessageId=" + messageId + ", demographicNo=" + demographicNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/InboxTo1.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ServiceSpecialistsPK.java">
{@Override	public String toString() {		return ("ServiceSpecialistsPK:" + serviceId + "," + specId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/pageUtil/EctSessionBean.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/consultations/ConsultationRequestSearchFilter.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformProcedureBook.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/EctProviderData.java">
{public String getFirstName() {			return firstName;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/myoscar/utils/MyOscarLoggedInInfo.java">
{@Override	public String toString()	{		return("loggedInPersonId=" + loggedInPersonId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicMerged.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformSpecsHistory.java">
{@Override	public String toString() {		return toString3("<br/>");	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/FunctionalCentreAdmissionDisplay.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/sharingcenter/model/PatientPolicyConsent.java">
{public Integer getDemographicNo()    {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DrugReason.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/IndicatorPanelBean.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AllergyTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWorkflow/WorkFlowInfo.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarResearch/oscarDxResearch/pageUtil/dxResearchForm.java">
{public String getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/config/pageUtil/EctConAddSpecialistForm.java">
{public String getLastName() {		if (lName == null) lName = new String();		return lName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BedDemographicHistoricalPK.java">
{@Override	public String toString() {		return ("bedId=" + bedId + ", demographicNo=" + demographicNo + ",usageStart=" + usageStart);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/CihiExportPHC_VRSAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/BillingDataBean.java">
{public String getHin()   {      return hin;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IntegratorProgressItem.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/model/FormRourke2009.java">
{@Column(name="demographic_no")	public int getDemographicNo() {		return this.demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/Age.java">
{@Override	   public String toString()	   {	      return years + " Years, " + months + " Months, " + days + " Days";	   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/Program.java">
{public String toString() {		return super.toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/TRUENORTHHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/DemographicDao.java">
{public String getPatientStatus() {			return patientStatus;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportAgeSex.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformConsultationReport.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/PATHL7Handler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IntegratorConsentComplexExitInterview.java">
{public String getSpokenLanguage() {		return spokenLanguage;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Patient.java">
{public String getBirthDay(){   return this.yearOfBirth + "-" + this.monthOfBirth + "-" + this.dateOfBirth;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingRAData.java">
{public String getHin() {		return hin;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OLISHL7Handler.java">
{@Override	public String getSex() {		try {			return (getString(terser.get("/.PID-8-1")));		} catch (Exception e) {			return ("");		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/query/RangeLowerLimit.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/HRMXMLHandler.java">
{public String getSex() {		return pr.getDemographics().getGender().value();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/MSP/ServiceCodeValidationLogic.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Provider.java">
{public String getSex() {    return (sex != null ? sex : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderData.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/web/PatientDetailBean.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SpecialtyPK.java">
{public String toString() {		return getRegion()+getSpecialty();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/web/admin/UserSearchFormBean.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementViewFormBean.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/EctPatientData.java">
{public String getSex() {            return sex;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/web/reports/ocan/beans/OcanDomainConsumerStaffBean.java">
{public String toString() {		return(ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MyGroup.java">
{public String getLastName() {        return this.lastName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/DashboardBean.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Provider.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicSets.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultationResponse.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/pageUtil/RptDemographicReportForm.java">
{public String[] getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecObjPrivilegePrimaryKey.java">
{@Override	public String toString() {		return ("roleUserGroup=" + roleUserGroup + ", objectName=" + objectName);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/NewAppointmentTo1.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicArchive.java">
{public Date getRosterTerminationDate() {		    return this.rosterTerminationDate;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/study/HSFO/PatientData.java">
{public String getSex() {		return Sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/event/AppointmentStatusChangeEvent.java">
{@Override    public String toString() {        return "AppointmentChangeEvent [appointment_no=" + this.appointment_no                + ", provider_no=" + this.provider_no + ", status="                + this.status + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/util/DosingRecomendation.java">
{public String toString(){        return "name: "+name+" atccode: "+atccode;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderStudyPK.java">
{@Override	public String toString() {		return ("providerNo=" + providerNo + ", studyNo=" + studyNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/MessageTo1.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicArchiveMeta.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hsfo2Patient.java">
{public String getSex()  {    return Sex;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MdsPID.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Hl7Link.java">
{public int getDemographicNo() {    return demographicNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/TeleplanResponse.java">
{public String toString(){        return "#TID="+getTransactionNo()+";Result="+getResult()+";Filename="+getFilename()+";Msgs="+getMsgs()+"; NUM LINES "+lineCount+" REALFILNAME ="+realFilename;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/WorkFlow.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/MeasurementTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Billing.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingProviderData.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchResultItem.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Allergy.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/on/CML/CMLLabTest.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicStudyPK.java">
{@Override	public String toString() {		return ("demographicNo=" + demographicNo + ", studyNo=" + studyNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AllergyTo1.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CLSHandler.java">
{public String getSex() {		return (getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProgramProviderTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProfessionalContact.java">
{public String toString() {		return "ProfessionalRelationship - id:"+ getId();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeAnswerElement.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getElement()).append(", ").append(getValidation()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DocumentTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProgramTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Desaprisk.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/PrescriptionTo1.java">
{public Integer getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PHRVerification.java">
{public Integer getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/MeasurementDao.java">
{public Integer getDemographicNo() {			return demographicNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ProfessionalSpecialistTo1.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/ClinicDAO.java">
{public Clinic getClinic(){    	Query query = entityManager.createQuery("select c from Clinic c");        @SuppressWarnings("unchecked")        List<Clinic> codeList = query.getResultList();        if(codeList.size()>0) {        	return codeList.get(0);        }        return null;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Immunizations.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/conditionValue/DSValueStatement.java">
{@Override    public String toString() {        StringBuilder result = new StringBuilder();        if (this.getValueType() != null) result.append(this.getValueType() + ":");        if (this.getOperator() != null) result.append(this.getOperator());        result.append(this.getValue());        if (this.getValueUnit() != null) result.append(" " + this.getValueUnit());        return result.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DesAnnualReviewPlan.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/bean/EctMeasurementsDataBean.java">
{public String toString(){        return " id " +id+ " type: " +type+              " typeDisplayName: "+typeDisplayName+            "typeDescription: "+typeDescription+            "demo: "+demo+            "providerFirstName: "+providerFirstName+            "providerLastName: "+providerLastName +            "dataField: "+dataField +            "measuringInstrc: "+measuringInstrc +            "comments: "+comments +            "dateObserved: "+dateObserved +            "dateEntered: "+dateEntered +            "canPlot: "+canPlot  +            "dateObservedAsDate: "+dateObservedAsDate  +            "dateEnteredAsDate: "+dateEnteredAsDate  +            "indicationColour: "+indicationColour  ;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/MetricOwner.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/HealthSafety.java">
{public Long getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeNodeLabel.java">
{@Override	public String toString() {		return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getLabel()).append(")").toString();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/SummaryTo1.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyData.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/match/VacancyClientMatch.java">
{@Override	public String toString() {		return "VacancyClientMatch [client_id=" + client_id + ", vacancy_id=" + vacancy_id + ", form_id=" + form_id + ", contactAttempts=" + contactAttempts + ", last_contact_date=" + last_contact_date + ", status=" + status + ", rejectionReason=" + rejectionReason + ", matchPercentage=" + matchPercentage + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxChoosePatientForm.java">
{public String getDemographicNo() {        return (this.demographicNo);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AppointmentTransfer.java">
{public int getDemographicNo() {		return (demographicNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/IntakeNode.java">
{@Override    public String toString() {        return new StringBuilder(REF).append("(").append(getId()).append(", ").append(getLabel()).append(", ").append(getNodeTemplate()).append(")").toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarPrevention/Prevention.java">
{public String getSex(){ return sex; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/ClientSearchFormBean.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RemoteAttachments.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/event/AppointmentCreatedEvent.java">
{@Override    public String toString() {        return "AppointmentChangeEvent [appointment_no=" + this.appointment_no                + ", provider_no=" + this.provider_no + "]";    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/MeasurementMapTransfer.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/sharingcenter/model/PatientSharingNetworkDataObject.java">
{public Integer getDemographicNo() {	    return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/NoteTo1.java">
{@Override	public String toString() {		return (ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Hl7Pid.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/ClientReferral.java">
{public String toString() {        return super.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/upload/PatientLabRoutingResult.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/pageUtil/OruR01UploadForm.java">
{@Override    public String toString()	{		return(ToStringBuilder.reflectionToString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxSearchDrugForm.java">
{public String getDemographicNo() {        return (this.demographicNo);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ScheduleTemplatePrimaryKey.java">
{@Override	public String toString() {		return ("name=" + name + ", providerNo=" + providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/VacancyClientMatch.java">
{@Override	public String toString() {		return "VacancyClientMatch [client_id=" + client_id + ", vacancy_id=" + vacancy_id + ", form_id=" + form_id + ", contactAttempts=" + contactAttempts + ", last_contact_date=" + last_contact_date + ", status=" + status + ", rejectionReason=" + rejectionReason + ", matchPercentage=" + matchPercentage + "]";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Diseases.java">
{public int getDemographicNo() {    	return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/pageUtil/EctConsultationFormRequestForm.java">
{public String getDemographicNo() {		return (StringUtils.trimToEmpty(demographicNo));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RoomBedHistoricalPK.java">
{@Override	public String toString() {		return ("bedId=" + bedId + ", roomId=" + roomId + ",containStart=" + containStart);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarProvider/data/ProviderData.java">
{public java.lang.String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/inbox/InboxManagerResponse.java">
{public Integer getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MEDVUEHandler.java">
{public String getSex() {				try {			return getString(pat_23.getPID().getSex().getValue());		} catch (Exception e) {			logger.error("Exception getting the sex of the patient" , e);		}				return "";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetCustomization.java">
{public String toString(){        return   " id:"+id+ " flowsheet:" +flowsheet+ " measurement:" + measurement + " payload:" + payload +  " action:"  + action +  " providerNo:"  +providerNo +" demographicNo:"+ demographicNo+" createDate:"+createDate+" archived:"+archived;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/CaseManagementViewAction.java">
{public String toString() {			return (ReflectionToStringBuilder.toString(this));		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/pharmaForms/formBPMH/business/BpmhFormHandler.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/filters/CodeFilter.java">
{@Override    public String toString(){		return(ReflectionToStringBuilder.toString(this));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Immunizations.java">
{public int getDemographicNo() {    return demographicNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/StudentInfo.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementEntryFormBean.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CustomFilter.java">
{public String getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/SecProvider.java">
{public String getSex() {		return this.sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/WCB.java">
{public String toString() {        return ReflectionToStringBuilder.toString(this);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementIssue.java">
{public String toString() {		return "CaseManagementIssue: id=" + id;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/conditionValue/DSValue.java">
{@Override    public String toString() {        StringBuilder result = new StringBuilder();        if (this.getValueType() != null) result.append(this.getValueType() + ":");        result.append(this.getValue());        if (this.getValueUnit() != null) result.append(" " + this.getValueUnit());        return result.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/BillingPatientDataBean.java">
{public String getSex()   {      return sex;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Wcb.java">
{public int getDemographicNo() {		return demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OscarToOscarHl7V2/ChainnedMessageAdapter.java">
{public String getSex() {		String hl7Gender = getPid().getAdministrativeSex().getValue();		Gender oscarGender = DataTypeUtils.getOscarGenderFromHl7Gender(hl7Gender);		if (oscarGender==null) return(null);		return (oscarGender.name());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/sharingcenter/dao/ClinicInfoDao.java">
{public ClinicInfoDataObject getClinic() {        Query query = entityManager.createQuery("FROM ClinicInfoDataObject c");        query.setMaxResults(1);        ClinicInfoDataObject retVal = getSingleResultOrNull(query);        if (retVal != null) {            return retVal;        }        MiscUtils.getLogger().warn("Please update the 'Administration/Clinic Info' page");        ClinicInfoDataObject failsafe = new ClinicInfoDataObject();        failsafe.setFacilityName("facility");        failsafe.setLocalAppName("app");        failsafe.setName("name");        failsafe.setNamespaceId("namespace");        failsafe.setOid("organization oid");        failsafe.setUniversalId("universal id");        failsafe.setSourceId("source id");        return failsafe;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BedDemographicPK.java">
{@Override	public String toString() {		return ("demographicNo=" + demographicNo + ", bedId=" + bedId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingOnTransaction.java">
{public int getDemographicNo() {		return this.demographicNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/EChart.java">
{public int getDemographicNo() {    return demographicNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONExt.java">
{public int getDemographicNo() {            return demographicNo;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Contact.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/mchcv/HCMagneticStripe.java">
{public String getSex() {        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/handler/IndicatorTemplateXML.java">
{@Override	public String toString() {	   return ReflectionToStringBuilder.toString(this);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/administration/TeleplanCorrectionFormWCB.java">
{public String getLastName() {        return this.lastName;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/GenericIntakeSearchFormBean.java">
{public String getLastName() {		return lastName;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ProviderTo1.java">
{public Sex1 getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DrugTo1.java">
{public Integer getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CMLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/bean/BillingEDTOBECOutputSpecificationBean.java">
{public String getSex(){           return sex;       }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetDx.java">
{public int getDemographicNo() {        return demographicNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/BioTestHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}
