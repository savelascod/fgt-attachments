public voidsetDemographicDao(DemographicDao demographicDao){	    this.demographicDao = demographicDao;    }this.preventionDao=preventionDaopublic voidsetDataExportDAO(DataExportDao dataExportDAO){    	this.dataExportDAO = dataExportDAO;    }public voidsetClinicDAO(ClinicDAO clinicDAO){    	this.clinicDAO = clinicDAO;    }Clinicclinic= clinicDAO.getClinic()List<DataExport>dataExportList= dataExportDAO.findAllByType(DataExportDao.CIHI_OMD4)dataExportList.addAll(dataExportDAO.findAllByType(DataExportDao.CIHI_PHC_VRS))Collections.sort(dataExportList)StringpatientSet= frm.getString("patientSet")if( patientSet == null || "".equals(patientSet) || patientSet.equals("-1")) {			frm.set("orgName", clinic.getClinicName());			frm.set("vendorId", properties.getProperty("vendorId", ""));			frm.set("vendorBusinessName", properties.getProperty("vendorBusinessName", ""));			frm.set("vendorCommonName", properties.getProperty("vendorCommonName", ""));			frm.set("vendorSoftware", properties.getProperty("softwareName", ""));			frm.set("vendorSoftwareCommonName", properties.getProperty("softwareCommonName", ""));			frm.set("vendorSoftwareVer", properties.getProperty("version", ""));			frm.set("installDate", properties.getProperty("buildDateTime", ""));			if( dataExportList.size() > 0 ) {				DataExport dataExport = dataExportList.get(dataExportList.size()-1);				frm.set("contactLName",dataExport.getContactLName());				frm.set("contactFName", dataExport.getContactFName());				frm.set("contactPhone", dataExport.getContactPhone());				frm.set("contactEmail", dataExport.getContactEmail());				frm.set("contactUserName", dataExport.getUser());			}			request.setAttribute("dataExportList", dataExportList);			return mapping.findForward("display");		}List<String>patientList= demoSets.getDemographicSet(frm.getString("patientSet"))Stringfilename= this.make(LoggedInInfo.getLoggedInInfoFromSession(request), frm, patientList, tmpDir)DataExportdataExport= new DataExport()dataExport.setContactLName(frm.getString("contactLName"))dataExport.setContactFName(frm.getString("contactFName"))dataExport.setContactPhone(frm.getString("contactPhone"))dataExport.setContactEmail(frm.getString("contactEmail"))dataExport.setUser(frm.getString("contactUserName"))dataExport.setType(frm.getString("extractType"))dataExport.setDaterun(runDate)dataExport.setFile(filename)dataExportDAO.persist(dataExport)dataExportList.add(dataExport)request.setAttribute("dataExportList", dataExportList)private Stringmake(LoggedInInfo loggedInInfo, DynaValidatorForm frm, List patientList, String tmpDir)throws Exception{		 HashMap<String,CiHiCdsDocument> xmlMap = new HashMap<String,CiHiCdsDocument>();		 HashMap<String,String> fileNamesMap = new HashMap<String,String>();		 String demoNo;		 Demographic demo;		 Calendar now = Calendar.getInstance();		 CiHiCds cihicds;		 CiHiCdsDocument cihiCdsDocument;		 for( int idx = 0; idx < patientList.size(); ++idx ) {			 demoNo = null;			 Object obj = patientList.get(idx);			 if (obj instanceof String) {				demoNo = (String)obj;			 } else {				ArrayList<String> l2 = (ArrayList<String>)obj;				demoNo = l2.get(0);			 }			 demo = getDemographicDao().getDemographic(demoNo);			 if( demo == null ) {				 continue;			 }			 if( !xmlMap.containsKey(demo.getProviderNo())) {				 cihiCdsDocument = CiHiCdsDocument.Factory.newInstance();				 cihicds =  cihiCdsDocument.addNewCiHiCds();				 this.buildExtractInformation(frm, cihicds.addNewExtractInformation(), now);				 this.buildProvider(demo.getProvider(),cihicds.addNewProvider(), fileNamesMap);				 xmlMap.put(demo.getProviderNo(), cihiCdsDocument);			 }			 else {				 cihiCdsDocument = xmlMap.get(demo.getProviderNo());				 cihicds = cihiCdsDocument.getCiHiCds();			 }			 PatientRecord patientRecord = cihicds.addNewPatientRecord();			 this.buildPatientDemographic(demo, patientRecord.addNewDemographics());			 this.buildAppointment(demo, patientRecord);			 this.buildFamilyHistory(demo, patientRecord);			 this.buildOngoingProblems(demo, patientRecord);			 this.buildRiskFactors(demo, patientRecord);			 this.buildAllergies(demo, patientRecord);			 			 this.buildCareElements(demo, patientRecord);			 			 this.buildProcedure(demo, patientRecord);			 this.buildLaboratoryResults(demo, patientRecord);			 this.buildMedications(demo, patientRecord);			 this.buildImmunizations(loggedInInfo, demo, patientRecord);		 }		 return this.makeFiles(xmlMap, fileNamesMap, tmpDir);	}private voidbuildPatientDemographic(Demographic demo, Demographics xmlDemographics){		HealthCard healthCard = xmlDemographics.addNewHealthCard();		healthCard.setNumber(demo.getHin());                if (StringUtils.empty(healthCard.getNumber())) healthCard.setNumber("0");		healthCard.setType("HCN");		healthCard.setJurisdiction(cdsDtCihi.HealthCardProvinceCode.CA_ON);		Calendar dob  = demo.getBirthDay();		xmlDemographics.setDateOfBirth(dob);		String sex = demo.getSex();		if( !"F".equalsIgnoreCase(sex) && !"M".equalsIgnoreCase(sex)) {			sex = "U";		}		cdsDtCihi.Gender.Enum enumDemo = cdsDtCihi.Gender.Enum.forString(sex);		xmlDemographics.setGender(enumDemo);		String spokenLanguage = demo.getSpokenLanguage();		if (StringUtils.filled(spokenLanguage) && Util.convertLanguageToCode(spokenLanguage)!=null){			xmlDemographics.setPreferredSpokenLanguage(Util.convertLanguageToCode(spokenLanguage));		}		Date statusDate = demo.getPatientStatusDate();		Calendar statusCal = Calendar.getInstance();		if( statusDate != null ) {			statusCal.setTime(statusDate);		}                PersonStatusCode personStatusCode = xmlDemographics.addNewPersonStatusCode();		String status = demo.getPatientStatus();		if( "AC".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihi.PersonStatus.A);		else if( "IN".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihi.PersonStatus.I);                else if( "DE".equalsIgnoreCase(status)) personStatusCode.setPersonStatusAsEnum(cdsDtCihi.PersonStatus.D);                else {                    if ("MO".equalsIgnoreCase(status)) status = "Moved";                    else if ("FI".equalsIgnoreCase(status)) status = "Fired";                    personStatusCode.setPersonStatusAsPlainText(status);                }                if( statusDate != null ) {                        xmlDemographics.setPersonStatusDate(statusCal);		}                Date rosterDate = demo.getRosterDate();                Calendar rosterCal = Calendar.getInstance();                if( rosterDate != null ) {                        rosterCal.setTime(rosterDate);                        xmlDemographics.setEnrollmentDate(rosterCal);                }                Date rosterTermDate = demo.getRosterTerminationDate();                Calendar rosterTermCal = Calendar.getInstance();                if( rosterTermDate != null ) {                        rosterTermCal.setTime(rosterTermDate);                        xmlDemographics.setEnrollmentTerminationDate(rosterTermCal);                }                if (StringUtils.filled(demo.getPostal())) {                    PostalZipCode postalZipCode = xmlDemographics.addNewPostalAddress();                    postalZipCode.setPostalCode(StringUtils.noNull(demo.getPostal()).replace(" ",""));                }	}private voidbuildAppointment(Demographic demo, PatientRecord patientRecord){		//some weird values for appointment time preventing query from executing so just skip bad records		try {			List<Appointment> appointmentList = oscarAppointmentDao.getAppointmentHistory(demo.getDemographicNo());			Calendar cal = Calendar.getInstance();			Calendar startTime = Calendar.getInstance();			Date startDate;			for( Appointment appt: appointmentList) {				if( appt.getAppointmentDate() == null ) {					continue;				}				Appointments appointments = patientRecord.addNewAppointments();	            if (StringUtils.filled(appt.getReason())) appointments.setAppointmentPurpose(appt.getReason());				DateFullOrPartial dateFullorPartial = appointments.addNewAppointmentDate();				cal.setTime(appt.getAppointmentDate());				startDate = appt.getStartTime();				startTime.setTime(startDate);				cal.set(Calendar.HOUR_OF_DAY, startTime.get(Calendar.HOUR_OF_DAY));				cal.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));				dateFullorPartial.setFullDate(cal);			}		} catch( Exception e ) {			MiscUtils.getLogger().error("Error",e);		}	}private voidbuildFamilyHistory(Demographic demo, PatientRecord patientRecord ){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"FamHistory"});		String[] famHistory = new String[] {issueList.get(0).getId().toString()};		String intervention;	    String relationship;	    String age;	    Date startDate;	    String startDateFormat;	    List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), famHistory);		for( CaseManagementNote caseManagementNote: notesList) {			intervention = "";			relationship = "";			age = "";			startDateFormat = null;			startDate = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.TREATMENT)) {                    intervention = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RELATIONSHIP)) {                    relationship = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.AGEATONSET)) {                    age = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }            }            Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();            if( noteIssueList != null && noteIssueList.size() > 0 ) {                Iterator<CaseManagementIssue> i = noteIssueList.iterator();                CaseManagementIssue cIssue;                FamilyHistory familyHistory = patientRecord.addNewFamilyHistory();                while ( i.hasNext() ) {                	cIssue = i.next();                    if (cIssue.getIssue().getType().equals("system")) continue;                	StandardCoding standardCoding = familyHistory.addNewDiagnosisProcedureCode();                	standardCoding.setStandardCodingSystem(cIssue.getIssue().getType());                    String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                	standardCoding.setStandardCode(code);                	standardCoding.setStandardCodeDescription(cIssue.getIssue().getDescription());                    break;                }                if( startDate != null ) {                	Util.putPartialDate(familyHistory.addNewStartDate(), startDate, startDateFormat);                }                if( !"".equalsIgnoreCase(age) ) {                	try {                		familyHistory.setAgeAtOnset(BigInteger.valueOf(Long.parseLong(age)));                	}catch(NumberFormatException e) {                		//ignore                	}                }                //if( !hasIssue ) {  //Commenting this out.  I don't see why it's not there if it has an issue                    familyHistory.setProblemDiagnosisProcedureDescription(caseManagementNote.getNote());                //}                if( !"".equalsIgnoreCase(intervention)) {                    familyHistory.setTreatment(intervention);                }                if( !"".equalsIgnoreCase(relationship)) {                    familyHistory.setRelationship(relationship);                }            }        }    }private voidbuildOngoingProblems(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"Concerns"});		String[] onGoingConcerns = new String[] {issueList.get(0).getId().toString()};		Date startDate;	    Date endDate;	    String startDateFormat;	    String endDateFormat;	    List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), onGoingConcerns);		for( CaseManagementNote caseManagementNote: notesList) {			startDate = null;			endDate = null;		    startDateFormat = null;		    endDateFormat = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {                    endDate = caseManagementNoteExt.getDateValue();                    endDateFormat = caseManagementNoteExt.getValue();                }            }            Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();            if( noteIssueList != null && noteIssueList.size() > 0 ) {                Iterator<CaseManagementIssue> i = noteIssueList.iterator();                CaseManagementIssue cIssue;                ProblemList problemList = patientRecord.addNewProblemList();                while ( i.hasNext() ) {                	cIssue = i.next();                    if (cIssue.getIssue().getType().equals("system")) continue;                	StandardCoding standardCoding = problemList.addNewDiagnosisCode();                	standardCoding.setStandardCodingSystem(cIssue.getIssue().getType());                    String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                	standardCoding.setStandardCode(code);                	standardCoding.setStandardCodeDescription(cIssue.getIssue().getDescription());                    break;                }                if( startDate != null ) {                	Util.putPartialDate(problemList.addNewOnsetDate(), startDate, startDateFormat);                }                if( endDate != null ) {                	Util.putPartialDate(problemList.addNewResolutionDate(), endDate, endDateFormat);                }                //if( !hasIssue ) {  //Commenting out another one                    problemList.setProblemDiagnosisDescription(caseManagementNote.getNote());                //}            }        }    }private voidbuildRiskFactors(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"RiskFactors"});		String[] riskFactor = new String[] {issueList.get(0).getId().toString()};		Date startDate;	    Date endDate;	    String startDateFormat;	    String endDateFormat;		List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), riskFactor);		for( CaseManagementNote caseManagementNote: notesList) {			startDate = null;			endDate = null;		    startDateFormat = null;		    endDateFormat = null;			List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());            String keyval;            for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                keyval = caseManagementNoteExt.getKeyVal();                if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {                    startDate = caseManagementNoteExt.getDateValue();                    startDateFormat = caseManagementNoteExt.getValue();                }                else if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {                    endDate = caseManagementNoteExt.getDateValue();                    endDateFormat = caseManagementNoteExt.getValue();                }            }            RiskFactors riskFactors = patientRecord.addNewRiskFactors();            if( startDate != null ) {            	Util.putPartialDate(riskFactors.addNewStartDate(), startDate, startDateFormat);            }            if( endDate != null ) {            	Util.putPartialDate(riskFactors.addNewEndDate(), endDate, endDateFormat);            }            String riskFactorContent = caseManagementNote.getNote();            if (riskFactorContent.length()>120) riskFactorContent = riskFactorContent.substring(0, 120);            riskFactors.setRiskFactor(riskFactorContent);		}	}private voidbuildAllergies(Demographic demo, PatientRecord patientRecord){		String[] severity = new String[] {"MI","MO","LT","NO"};		AllergyDao allergyDao=(AllergyDao)SpringUtils.getBean("allergyDao");		List<Allergy> allergies = allergyDao.findActiveAllergies(demo.getDemographicNo());		int index;		Date date;        for( Allergy allergy: allergies ) {            	AllergiesAndAdverseReactions xmlAllergies = patientRecord.addNewAllergiesAndAdverseReactions();                Integer typeCode = allergy.getTypeCode();                if (typeCode == null) {                    xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihi.PropertyOfOffendingAgent.UK);                } else {                    if (typeCode == 13) xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihi.PropertyOfOffendingAgent.DR);                    else if(typeCode == 0) xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihi.PropertyOfOffendingAgent.ND);                    else xmlAllergies.setPropertyOfOffendingAgent(cdsDtCihi.PropertyOfOffendingAgent.UK);                }        	xmlAllergies.setOffendingAgentDescription(allergy.getDescription());        	try {                index = Integer.parseInt(allergy.getSeverityOfReaction());            }catch(Exception e ) {                index = 4;            }        	if( index > 0 ) {        		index -= 1;        	}        	xmlAllergies.setSeverity(cdsDtCihi.AdverseReactionSeverity.Enum.forString(severity[index]));        	date = allergy.getStartDate();        	if( date != null ) {        		Util.putPartialDate(xmlAllergies.addNewStartDate(), date, PartialDate.ALLERGIES, allergy.getAllergyId(), PartialDate.ALLERGIES_STARTDATE);        	}        }	}private voidbuildCareElements(Demographic demo, PatientRecord patientRecord){		List<Measurements> measList = ImportExportMeasurements.getMeasurements(demo.getDemographicNo().toString());		CareElements careElements = patientRecord.addNewCareElements();		Calendar cal = Calendar.getInstance();		Date date = null;		for (Measurements measurement : measList) {			if( measurement.getType().equalsIgnoreCase("BP")) {				String[] sysdiabBp = measurement.getDataField().split("/");				if( sysdiabBp.length == 2 ) {					cdsDtCihi.BloodPressure bloodpressure = careElements.addNewBloodPressure();					bloodpressure.setSystolicBP(sysdiabBp[0]);					bloodpressure.setDiastolicBP(sysdiabBp[1]);					date = measurement.getDateObserved();					if( date == null ) {						date = measurement.getDateEntered();					}					cal.setTime(date);					bloodpressure.setDate(cal);				}			}			else if( measurement.getType().equalsIgnoreCase("HT")) {				cdsDtCihi.Height height = careElements.addNewHeight();				height.setHeight(measurement.getDataField());				height.setHeightUnit("cm");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				height.setDate(cal);			}			else if( measurement.getType().equalsIgnoreCase("WT")) {				cdsDtCihi.Weight weight = careElements.addNewWeight();				weight.setWeight(measurement.getDataField());				weight.setWeightUnit("Kg");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				weight.setDate(cal);			}			else if( measurement.getType().equalsIgnoreCase("WAIS") || measurement.getType().equalsIgnoreCase("WC") ) {				cdsDtCihi.WaistCircumference waist = careElements.addNewWaistCircumference();				waist.setWaistCircumference(measurement.getDataField());				waist.setWaistCircumferenceUnit("cm");				date = measurement.getDateObserved();				if( date == null ) {					date = measurement.getDateEntered();				}				cal.setTime(date);				waist.setDate(cal);			}		}	}private voidbuildProcedure(Demographic demo, PatientRecord patientRecord){		List<Issue> issueList = issueDAO.findIssueByCode(new String[] {"MedHistory"});		String[] medhistory = new String[] {issueList.get(0).getId().toString()};		Procedure procedure;		ProblemList problemlist;		List<CaseManagementNote> notesList = getCaseManagementNoteDAO().getActiveNotesByDemographic(demo.getDemographicNo().toString(), medhistory);		for( CaseManagementNote caseManagementNote: notesList) {                    procedure = null;                    problemlist = null;                    List<CaseManagementNoteExt> caseManagementNoteExtList = getCaseManagementNoteExtDAO().getExtByNote(caseManagementNote.getId());                    String keyval;                    for( CaseManagementNoteExt caseManagementNoteExt: caseManagementNoteExtList ) {                        if (procedure!=null && procedure.getProcedureDate()!=null) break;                        if (problemlist!=null && problemlist.getOnsetDate()!=null && problemlist.getResolutionDate()!=null) break;                        keyval = caseManagementNoteExt.getKeyVal();                        if( caseManagementNoteExt.getDateValue() != null ) {	                        if( keyval.equals(CaseManagementNoteExt.PROCEDUREDATE)) {	                            procedure = patientRecord.addNewProcedure();	                            Util.putPartialDate(procedure.addNewProcedureDate(), caseManagementNoteExt);	                        } else	                        if( keyval.equals(CaseManagementNoteExt.STARTDATE)) {	                            if (problemlist==null) problemlist = patientRecord.addNewProblemList();	                            Util.putPartialDate(problemlist.addNewOnsetDate(), caseManagementNoteExt);	                        } else	                        if( keyval.equals(CaseManagementNoteExt.RESOLUTIONDATE)) {	                            if (problemlist==null) problemlist = patientRecord.addNewProblemList();	                            Util.putPartialDate(problemlist.addNewResolutionDate(), caseManagementNoteExt);	                        }                        }                    }                    if (procedure==null && problemlist==null) continue;                    Set<CaseManagementIssue> noteIssueList = caseManagementNote.getIssues();                    if( noteIssueList != null && noteIssueList.size() > 0 ) {                        Iterator<CaseManagementIssue> i = noteIssueList.iterator();                        CaseManagementIssue cIssue;                        while ( i.hasNext() ) {                            cIssue = i.next();                            if (cIssue.getIssue().getType().equals("system")) continue;                            String code = cIssue.getIssue().getType().equalsIgnoreCase("icd9") ? Util.formatIcd9(cIssue.getIssue().getCode()) : cIssue.getIssue().getCode();                            if (procedure!=null) {                                StandardCoding procedureCode = procedure.addNewProcedureCode();                                procedureCode.setStandardCodingSystem(cIssue.getIssue().getType());                                procedureCode.setStandardCode(code);                                procedureCode.setStandardCodeDescription(cIssue.getIssue().getDescription());                                break;                            }                            if (problemlist!=null) {                                StandardCoding diagnosisCode = problemlist.addNewDiagnosisCode();                                diagnosisCode.setStandardCodingSystem(cIssue.getIssue().getType());                                diagnosisCode.setStandardCode(code);                                diagnosisCode.setStandardCodeDescription(cIssue.getIssue().getDescription());                                break;                            }                        }                    }                    if (procedure!=null) procedure.setProcedureInterventionDescription(caseManagementNote.getNote());                    if (problemlist!=null) problemlist.setProblemDiagnosisDescription(caseManagementNote.getNote());		}	}private voidbuildLaboratoryResults(Demographic demo, PatientRecord patientRecord){				log.debug("Building lab results for " + demo.getDemographicNo() + " " + demo.getFormattedName());                List<LabMeasurements> labMeaList = ImportExportMeasurements.getLabMeasurements(demo.getDemographicNo().toString());                for (LabMeasurements labMea : labMeaList) {                	String data = StringUtils.noNull(labMea.getExtVal("identifier"));                	log.debug("Measurement search for identifier '" + data + "'");                    Date dateTime = UtilDateUtilities.StringToDate(labMea.getExtVal("datetime"),"yyyy-MM-dd HH:mm:ss");                    if (dateTime==null) {                    	dateTime = UtilDateUtilities.StringToDate(labMea.getExtVal("datetime"),"yyyy-MM-dd");                    	if (dateTime==null) {                    		log.debug("dateTime is null...continuing");                    	}                    }                    LaboratoryResults labResults = patientRecord.addNewLaboratoryResults();                    if (StringUtils.filled(data)) labResults.setLabTestCode(data);                    cdsDtCihi.DateFullOrDateTime collDate = labResults.addNewCollectionDateTime();                    if (dateTime!=null) collDate.setFullDate(Util.calDate(dateTime));                    else collDate.setFullDate(Util.calDate("0001-01-01"));                    data = labMea.getExtVal("name");                    if (StringUtils.filled(data)) {                    	log.debug("Adding " + data);                    	labResults.setTestNameReportedByLab(data);                    }                    data = labMea.getMeasure().getDataField();                    if (StringUtils.filled(data)) {                        LaboratoryResults.Result result = labResults.addNewResult();                        result.setValue(data);                        data = labMea.getExtVal("unit");                        if (StringUtils.filled(data)) result.setUnitOfMeasure(data);                    }                    String min = labMea.getExtVal("minimum");                    String max = labMea.getExtVal("maximum");                    LaboratoryResults.ReferenceRange refRange = labResults.addNewReferenceRange();                    if (StringUtils.filled(min)) refRange.setLowLimit(min);                    if (StringUtils.filled(max)) refRange.setHighLimit(max);		}	}private voidbuildMedications(Demographic demo, PatientRecord patientRecord){		MedicationsAndTreatments medications;		RxPrescriptionData.Prescription[] pa = new RxPrescriptionData().getPrescriptionsByPatient(Integer.parseInt(demo.getDemographicNo().toString()));		String drugname;		String customname;		String dosage;		String[] strength;		int sep;		for(int p = 0; p<pa.length; ++p) {			drugname = pa[p].getBrandName();			customname = pa[p].getCustomName();			if (StringUtils.empty(drugname) && StringUtils.empty(customname)) continue;			medications = patientRecord.addNewMedicationsAndTreatments();			if (StringUtils.filled(drugname)) medications.setDrugName(drugname);			else medications.setDrugDescription(customname);			Date writtenDate = pa[p].getWrittenDate();			if (writtenDate!=null) {	        	String dateFormat = partialDateDao.getFormat(PartialDate.DRUGS, pa[p].getDrugId(), PartialDate.DRUGS_WRITTENDATE);	        	Util.putPartialDate(medications.addNewPrescriptionWrittenDate(), writtenDate, dateFormat);			}			if (StringUtils.filled(pa[p].getDosage())) {            	strength = pa[p].getDosage().split(" ");            	cdsDtCihi.DrugMeasure drugM = medications.addNewStrength();            	if (Util.leadingNum(strength[0]).equals(strength[0])) {//amount & unit separated by space            		drugM.setAmount(strength[0]);            		if (strength.length>1) drugM.setUnitOfMeasure(strength[1]);            		else drugM.setUnitOfMeasure("unit"); //UnitOfMeasure cannot be null            	} else {//amount & unit not separated, probably e.g. 50mg / 2tablet            		if (strength.length>1 && strength[1].equals("/")) {            			if (strength.length>2) {            				String unit1 = Util.leadingNum(strength[2]).equals("") ? "1" : Util.leadingNum(strength[2]);            				String unit2 = Util.trailingTxt(strength[2]).equals("") ? "unit" : Util.trailingTxt(strength[2]);                    		drugM.setAmount(Util.leadingNum(strength[0])+"/"+Util.leadingNum(strength[2]));                    		drugM.setUnitOfMeasure(Util.trailingTxt(strength[0])+"/"+unit2);            			}            		} else {                		drugM.setAmount(Util.leadingNum(strength[0]));                		drugM.setUnitOfMeasure(Util.trailingTxt(strength[0]));            		}            	}			}	        dosage = StringUtils.noNull(pa[p].getDosageDisplay());	        medications.setDosage(dosage);	        medications.setDosageUnitOfMeasure(StringUtils.noNull(pa[p].getUnit()));	        medications.setForm(StringUtils.noNull(pa[p].getDrugForm()));	        medications.setFrequency(StringUtils.noNull(pa[p].getFreqDisplay()));	        medications.setRoute(StringUtils.noNull(pa[p].getRoute()));	        medications.setNumberOfRefills(String.valueOf(pa[p].getRepeat()));	        YnIndicatorAndBlank patientCompliance = medications.addNewPatientCompliance();	        if (pa[p].getPatientCompliance()==null) {	        	patientCompliance.setBlank(cdsDtCihi.Blank.X);	        } else {	        	patientCompliance.setBoolean(pa[p].getPatientCompliance());	        }		}	}private voidbuildImmunizations(LoggedInInfo loggedInInfo, Demographic demo, PatientRecord patientRecord){    	HashMap<String,String> preventionMap;    	List<Prevention> preventionsList = getPreventionDao().findNotDeletedByDemographicId(demo.getDemographicNo());    	Map<String,Object> prevTypes = Util.getPreventionTypes(loggedInInfo);    	         for( Prevention prevention: preventionsList ) {             preventionMap = getPreventionExtDao().getPreventionExt(prevention.getId());        	 Immunizations immunizations = patientRecord.addNewImmunizations();        	 if (StringUtils.filled(preventionMap.get("name"))) {        		immunizations.setImmunizationName(preventionMap.get("name"));        	 }else{        		String preventionType = Util.getImmunizationType(loggedInInfo, prevention.getPreventionType(),prevTypes);        		if (cdsDt.ImmunizationType.Enum.forString(preventionType)!=null) {        			immunizations.setImmunizationName(preventionType);        		} else {        			immunizations.setImmunizationName("");        		}        	 }        	 if (StringUtils.filled(preventionMap.get("lot")))        		 immunizations.setLotNumber(preventionMap.get("lot"));        	 if (prevention.getPreventionDate()!=null) {        		 DateFullOrPartial dateFullorPartial = immunizations.addNewDate();        		 dateFullorPartial.setFullDate(Util.calDate(prevention.getPreventionDate()));        	 }        	 YnIndicator refusedIndicator = immunizations.addNewRefusedFlag();             if( prevention.isRefused() ) {            	 refusedIndicator.setBoolean(true);             }             else {                 refusedIndicator.setBoolean(false);             }         }    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Lab.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/IHAHandler.java">
{@Override    public String getSex(){        try{            return(getString(terser.get("/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/CihiExportAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RourkeExportAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/ICLHandler.java">
{public String getSex(){				try {            return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));				} catch (Exception e) {				    logger.error("Exception finding sex", e);						return("");				}    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/CihiExportPHC_VRSAction.java">
{public DemographicDao getDemographicDao() {	    return demographicDao;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/TRUENORTHHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/DemographicDao.java">
{public String getPatientStatus() {			return patientStatus;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportAgeSex.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchResult.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/PATHL7Handler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IntegratorConsentComplexExitInterview.java">
{public String getSpokenLanguage() {		return spokenLanguage;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MessageHandler.java">
{public String getSex();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Patient.java">
{public String getBirthDay(){   return this.yearOfBirth + "-" + this.monthOfBirth + "-" + this.dateOfBirth;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OLISHL7Handler.java">
{@Override	public String getSex() {		try {			return (getString(terser.get("/.PID-8-1")));		} catch (Exception e) {			return ("");		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/HRMXMLHandler.java">
{public String getSex() {		return pr.getDemographics().getGender().value();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Provider.java">
{public String getSex() {    return (sex != null ? sex : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderData.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/EctPatientData.java">
{public String getSex() {            return sex;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/ON/model/BillingONCHeader2.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Provider.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMDS/data/PatientData.java">
{public String getSex() {			return this.sex;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/pageUtil/RptDemographicReportForm.java">
{public String[] getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicArchive.java">
{public Date getRosterTerminationDate() {		    return this.rosterTerminationDate;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/study/HSFO/PatientData.java">
{public String getSex() {		return Sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/DSDemographicAccess.java">
{public String getSex() {        return getDemographicData(loggedInInfo).getSex();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hsfo2Patient.java">
{public String getSex()  {    return Sex;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MdsPID.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/DemographicSearchResultTransformer.java">
{public DemographicDao getDemographicDao() {		return demographicDao;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchResultItem.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/DefaultGenericHandler.java">
{public String getSex(){        try{            return(getString(terser.get("/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CLSHandler.java">
{public String getSex() {		return (getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DemographicTransfer.java">
{public Date getRosterTerminationDate() {    	return (rosterTerminationDate);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicQueryFavourite.java">
{public String getPatientStatus() {		return patientStatus;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProviderTransfer.java">
{public String getSex() {		return (sex);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OscarToOscarHl7V2Handler.java">
{public String getSex() {	    return chainnedMessageAdapter.getSex();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/PFHTHandler.java">
{public String getSex(){	        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/ClinicDAO.java">
{public Clinic getClinic(){    	Query query = entityManager.createQuery("select c from Clinic c");        @SuppressWarnings("unchecked")        List<Clinic> codeList = query.getResultList();        if(codeList.size()>0) {        	return codeList.get(0);        }        return null;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public static OscarProperties getInstance() {		return oscarProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CDLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Demographic.java">
{public Date getRosterTerminationDate() {        return rosterTerminationDate;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MDSHandler.java">
{public String getSex(){        try{            return(getString(DynamicHapiLoaderUtils.terserGet(terser,"/.PID-8-1")));        }catch(Exception e){            return("");        }    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hl7TextInfo.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/TDISHandler.java">
{public String getSex() {		return getString(pat_25.getPID().getAdministrativeSex().getValue());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarPrevention/Prevention.java">
{public String getSex(){ return sex; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONCHeader1.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Hl7Pid.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxPatientData.java">
{public String getSex() {			if (demographic != null) return demographic.getSex();			else return "";		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarProvider/data/ProviderData.java">
{public java.lang.String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicTo1.java">
{public Date getRosterTerminationDate() {		return rosterTerminationDate;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MEDITECHHandler.java">
{@Override	public String getSex(){		return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/MEDVUEHandler.java">
{public String getSex() {				try {			return getString(pat_23.getPID().getSex().getValue());		} catch (Exception e) {			logger.error("Exception getting the sex of the patient" , e);		}				return "";	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/pharmaForms/formBPMH/business/BpmhFormHandler.java">
{public DemographicDao getDemographicDao() {		return demographicDao;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/SpireHandler.java">
{public String getSex(){		String sex = getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue());				if (sex.length() > 0)			sex = sex.substring(0, 1);		        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/GDMLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/SecProvider.java">
{public String getSex() {		return this.sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/BillingPatientDataBean.java">
{public String getSex()   {      return sex;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/hl7/handlers/PhsStarHandler.java">
{public String getSex() {		try {			String var = this.extractOrEmpty("PID-8");			return var;		}catch(Exception e) {			return "";		}	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/OscarToOscarHl7V2/ChainnedMessageAdapter.java">
{public String getSex() {		String hl7Gender = getPid().getAdministrativeSex().getValue();		Gender oscarGender = DataTypeUtils.getOscarGenderFromHl7Gender(hl7Gender);		if (oscarGender==null) return(null);		return (oscarGender.name());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/search/pageUtil/EctDemographicSearchForm.java">
{public String getSex()    {        if(sex == null)            sex = new String();        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/sharingcenter/dao/ClinicInfoDao.java">
{public ClinicInfoDataObject getClinic() {        Query query = entityManager.createQuery("FROM ClinicInfoDataObject c");        query.setMaxResults(1);        ClinicInfoDataObject retVal = getSingleResultOrNull(query);        if (retVal != null) {            return retVal;        }        MiscUtils.getLogger().warn("Please update the 'Administration/Clinic Info' page");        ClinicInfoDataObject failsafe = new ClinicInfoDataObject();        failsafe.setFacilityName("facility");        failsafe.setLocalAppName("app");        failsafe.setName("name");        failsafe.setNamespaceId("namespace");        failsafe.setOid("organization oid");        failsafe.setUniversalId("universal id");        failsafe.setSourceId("source id");        return failsafe;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderArchive.java">
{public String getSex() {    	return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingOnTransaction.java">
{public String getClinic() {		return this.clinic;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingClaimHeader1Data.java">
{public String getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/mchcv/HCMagneticStripe.java">
{public String getSex() {        return sex;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ProviderTo1.java">
{public Sex1 getSex() {		return sex;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/CMLHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/bean/BillingEDTOBECOutputSpecificationBean.java">
{public String getSex(){           return sex;       }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/pharmaForms/formBPMH/bean/BpmhFormBean.java">
{public Clinic getClinic() {		return clinic;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/parsers/BioTestHandler.java">
{public String getSex(){        return(getString(msg.getRESPONSE().getPATIENT().getPID().getSex().getValue()));    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/on/LabResultData.java">
{public String getSex(){		return this.sex;	}}
