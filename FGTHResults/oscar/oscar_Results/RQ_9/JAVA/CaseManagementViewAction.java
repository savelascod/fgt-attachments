for (Iterator<CaseManagementNote> iter = notes.listIterator(); iter.hasNext();) {			CaseManagementNote note = iter.next();			if (Arrays.binarySearch(roleId, note.getReporter_caisi_role()) >= 0) filteredNotes.add(note);		}for (Iterator<CaseManagementNote> iter = notes.listIterator(); iter.hasNext();) {			CaseManagementNote note = iter.next();			List<CaseManagementIssue> issues = cmeIssueNotesDao.getNoteIssues((Integer.valueOf(note.getId().toString())));			for (CaseManagementIssue issue : issues) {				if (Arrays.binarySearch(issueId, String.valueOf(issue.getId())) >= 0) {					filteredNotes.add(note);					break;				}			}		}private List<CaseManagementNote>manageLockedNotes(List<CaseManagementNote> notes, boolean removeLockedNotes, Map<Long, Boolean> unlockedNotesMap){		List<CaseManagementNote> notesNoLocked = new ArrayList<CaseManagementNote>();		for (CaseManagementNote note : notes) {			if (note.isLocked()) {				if (unlockedNotesMap.get(note.getId()) != null) {					note.setLocked(false);				}			}			if (removeLockedNotes && !note.isLocked()) {				notesNoLocked.add(note);			}		}		if (removeLockedNotes) {			return notesNoLocked;		}		return notes;	}Map<Long, Boolean>unlockedNoteMap= this.getUnlockedNotesMap(request)unlockedNoteMap.put(new Long(noteId), new Boolean(success))request.getSession().setAttribute("unlockedNoteMap", unlockedNoteMap)Map<Long, Boolean>unlockedNoteMap= this.getUnlockedNotesMap(request)unlockedNoteMap.put(new Long(noteId), new Boolean(success))request.getSession().setAttribute("unlockedNoteMap", unlockedNoteMap)private Collection<CaseManagementNote>manageLockedNotes(Collection<CaseManagementNote> notes, boolean removeLockedNotes, Map<Long, Boolean> unlockedNotesMap){		List<CaseManagementNote> notesNoLocked = new ArrayList<CaseManagementNote>();		for (CaseManagementNote note : notes) {			if (note.isLocked()) {				if (unlockedNotesMap.get(note.getId()) != null) {					note.setLocked(false);				}			}			if (removeLockedNotes && !note.isLocked()) {				notesNoLocked.add(note);			}		}		if (removeLockedNotes) {			return notesNoLocked;		}		return notes;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/DashboardBean.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/ProblemCheckFilter.java">
{public HttpSession getSession()		{			HttpSession parentSession = super.getSession();			if (parentSession == null)				return null;			return new SessionChecker(super.getSession());		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayLocal.java">
{public boolean isLocked() {		return (caseManagementNote.isLocked());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Dashboard.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayIntegrator.java">
{public boolean isLocked() {	    return(false);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplay.java">
{public boolean isLocked();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Hsfo2Visit.java">
{public boolean isLocked()  {    return locked;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CasemgmtNoteLock.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayNonNote.java">
{public boolean isLocked() {		return false;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/SshHandler.java">
{public Session getSession() {		return session;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IndicatorTemplate.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PageMonitor.java">
{public boolean isLocked() {    	return locked;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/display/beans/AbstractDataDisplayBean.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{public HttpSession getSession() {		return (session);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementNote.java">
{public boolean isLocked() {		return locked;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/study/HSFO/VisitData.java">
{public boolean isLocked() {		return locked;	}}
