StringuserName= ""Stringpassword= ""booleanforcedpasswordchange= trueuserName=(String)request.getSession().getAttribute("userName")password=(String)request.getSession().getAttribute("password")StringerrorStr= errorHandling(password, newPassword, confirmPassword, encodePassword(oldPassword), oldPassword)persistNewPasswordAndPin(loggedInInfo, userName, newPassword, newPin)password=newPasswordforcedpasswordchange=falseuserName=((LoginForm)form).getUsername()password=((LoginForm)form).getPassword()Stringusername=(String)request.getSession().getAttribute("user")LogAction.addLog(username, LogConst.LOGIN, LogConst.CON_LOGIN, "facilityId="+facilityIdString, ip)if (cl.isBlock(ip, userName)) {	        	logger.info(LOG_PRE + " Blocked: " + userName);	            // return mapping.findForward(where); //go to block page	            // change to block page	            String newURL = mapping.findForward("error").getPath();	            newURL = newURL + "?errormsg=Your account is locked. Please contact your administrator to unlock.";	            	            if(ajaxResponse) {	            	JSONObject json = new JSONObject();	            	json.put("success", false);	            	json.put("error", "Your account is locked. Please contact your administrator to unlock.");	            	response.setContentType("text/x-json");	            	json.write(response.getWriter());	            	return null;	            }	            	            return(new ActionForward(newURL));	        }strAuth=cl.auth(userName, password, pin, ip)logger.info(LOG_PRE + " Inactive: " + userName)booleanisForcePasswordReset= securityManager.getPasswordResetFlag(userName)&&securityManager.isRequireUpgradeToStorage(userName)isForcePasswordReset&&forcedpasswordchange)||requiresUpgrade {            	            	String newURL = mapping.findForward("forcepasswordreset").getPath();            	            	try{            	   setUserInfoToSession( request, userName,  password,  pin, nextPage);            	}              	catch (Exception e) {            		logger.error("Error", e);                    newURL = mapping.findForward("error").getPath();                    newURL = newURL + "?errormsg=Setting values to the session.";            		            	}                return(new ActionForward(newURL));            	            }MyOscarUtils.setDeterministicallyMangledPasswordSecretKeyIntoSession(session, password)CRHelper.recordLoginSuccess(userName, strAuth[0], request)Stringusername= (String) session.getAttribute("user")Providerprovider= providerManager.getProvider(username)cl.updateLoginList(ip, userName)cl.updateLoginList(ip, userName)CRHelper.recordLoginFailure(userName, request)private voidsetUserInfoToSession(HttpServletRequest request,String userName, String password, String pin,String nextPage)throws Exception{    	request.getSession().setAttribute("userName", userName);    	request.getSession().setAttribute("password", encodePassword(password));    	request.getSession().setAttribute("pin", pin);    	request.getSession().setAttribute("nextPage", nextPage);        }private StringerrorHandling(String password, String  newPassword, String  confirmPassword, String  encodedOldPassword, String  oldPassword){	        	String newURL = "";	    if (!encodedOldPassword.equals(password)) {     	   newURL = newURL + "?errormsg=Your old password, does NOT match the password in the system. Please enter your old password.";       	} else if(StringUtils.isEmpty(newPassword)) { 	       newURL = newURL + "?errormsg=Your new password is empty.";   	    } else if (!newPassword.equals(confirmPassword)) {      	   newURL = newURL + "?errormsg=Your new password does NOT match the confirmed password. Please try again.";        	} else if (!Boolean.parseBoolean(OscarProperties.getInstance().getProperty("IGNORE_PASSWORD_REQUIREMENTS")) && newPassword.equals(oldPassword)) {       	   newURL = newURL + "?errormsg=Your new password is the same as your old password. Please choose a new password.";         	} 	    	        	    	    return newURL;     }private StringencodePassword(String password)throws Exception{    	MessageDigest md = MessageDigest.getInstance("SHA");    	    	StringBuilder sbTemp = new StringBuilder();	    byte[] btNewPasswd= md.digest(password.getBytes());	    for(int i=0; i<btNewPasswd.length; i++) sbTemp = sbTemp.append(btNewPasswd[i]);		    return sbTemp.toString();	        }private SecuritygetSecurity(LoggedInInfo loggedInInfo, String username){		List<Security> results = securityManager.findByUserName(loggedInInfo, username);		Security security = null;		if (results.size() > 0) security = results.get(0);		if (security == null) {			return null;		} else if (OscarProperties.isLdapAuthenticationEnabled()) {			security = new LdapSecurity(security);		}				return security;    }private voidpersistNewPasswordAndPin(LoggedInInfo loggedInInfo, String userName, String newPassword, String newPin)throws Exception{    	    Security security = getSecurity(loggedInInfo, userName);	    security.setPassword(PasswordHash.createHash(newPassword));	    security.setForcePasswordReset(Boolean.FALSE);	    security.setStorageVersion(Security.STORAGE_VERSION_2);	    	    if(!StringUtils.isEmpty(newPin)) {	    	security.setPin(PasswordHash.createHash(newPin));	    }	    	    securityManager.updateSecurityRecord(loggedInInfo, security); 		    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/ProblemCheckFilter.java">
{public HttpSession getSession()		{			HttpSession parentSession = super.getSession();			if (parentSession == null)				return null;			return new SessionChecker(super.getSession());		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstanceTmpSave.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/User.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/EaapsServiceClient.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxDoctorPreferences.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstance.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyLogin.java">
{public String getPassword() {    	return password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/StudentInfo.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/controller/ERxCommunicator.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/login/LoginForm.java">
{public String getPassword() {    return password;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Security.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/login/jaas/OscarCallbackHandler.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/MetricOwner.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/SshHandler.java">
{public Session getSession() {		return session;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PageMonitor.java">
{public String getSession() {    	return session;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{public HttpSession getSession() {		return (session);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FaxJob.java">
{public String getPassword() {        return password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementNote.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementViewFormBean.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/request/SetPatientImmediate3.java">
{public String getPassword() {		return this.password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecurityArchive.java">
{public String getPassword() {		return password;	}}
