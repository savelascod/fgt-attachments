StringnewURL= mapping.findForward("error").getPath()newURL=newURL+"?errormsg=Application Error. See Log."return new ActionForward(newURL);StringuserName= ""Stringpassword= ""booleanforcedpasswordchange= trueuserName=(String)request.getSession().getAttribute("userName")password=(String)request.getSession().getAttribute("password")StringnewPassword= ((LoginForm) form).getNewPassword()StringoldPassword= ((LoginForm) form).getOldPassword()StringnewPin= ((LoginForm) form).getNewPin()StringoldPin= ((LoginForm) form).getOldPin()StringerrorStr= errorHandling(password, newPassword, confirmPassword, encodePassword(oldPassword), oldPassword)StringnewURL= mapping.findForward("forcepasswordreset").getPath()newURL=newURL+errorStrreturn(new ActionForward(newURL));StringerrorStr2= errorHandling2(pin, newPin, confirmPin, oldPin)StringnewURL= mapping.findForward("forcepasswordreset").getPath()newURL=newURL+errorStr2return(new ActionForward(newURL));persistNewPasswordAndPin(loggedInInfo, userName, newPassword, newPin)password=newPasswordpin=newPinStringnewURL= mapping.findForward("error").getPath()newURL=newURL+"?errormsg=Setting values to the session."return(new ActionForward(newURL));forcedpasswordchange=falseuserName=((LoginForm)form).getUsername()password=((LoginForm)form).getPassword()Stringusername=(String)request.getSession().getAttribute("user")LogAction.addLog(username, LogConst.LOGIN, LogConst.CON_LOGIN, "facilityId="+facilityIdString, ip)if (cl.isBlock(ip, userName)) {	        	logger.info(LOG_PRE + " Blocked: " + userName);	            // return mapping.findForward(where); //go to block page	            // change to block page	            String newURL = mapping.findForward("error").getPath();	            newURL = newURL + "?errormsg=Your account is locked. Please contact your administrator to unlock.";	            	            if(ajaxResponse) {	            	JSONObject json = new JSONObject();	            	json.put("success", false);	            	json.put("error", "Your account is locked. Please contact your administrator to unlock.");	            	response.setContentType("text/x-json");	            	json.write(response.getWriter());	            	return null;	            }	            	            return(new ActionForward(newURL));	        }strAuth=cl.auth(userName, password, pin, ip)StringnewURL= mapping.findForward("error").getPath()newURL=newURL+"?errormsg=Database driver "+e.getMessage().substring(e.getMessage().indexOf(':') + 2)+" not found."newURL=newURL+"?errormsg=Database connection error: "+e.getMessage()+"."return(new ActionForward(newURL));ProviderDaoproviderDao= SpringUtils.getBean(ProviderDao.class)Providerp= providerDao.getProvider(strAuth[0])logger.info(LOG_PRE + " Inactive: " + userName)StringnewURL= mapping.findForward("error").getPath()newURL=newURL+"?errormsg=Your account is inactive. Please contact your administrator to activate."return(new ActionForward(newURL));booleanisForcePasswordReset= securityManager.getPasswordResetFlag(userName)&&securityManager.isRequireUpgradeToStorage(userName)isForcePasswordReset&&forcedpasswordchange)||requiresUpgrade {            	            	String newURL = mapping.findForward("forcepasswordreset").getPath();            	            	try{            	   setUserInfoToSession( request, userName,  password,  pin, nextPage);            	}              	catch (Exception e) {            		logger.error("Error", e);                    newURL = mapping.findForward("error").getPath();                    newURL = newURL + "?errormsg=Setting values to the session.";            		            	}                return(new ActionForward(newURL));            	            }MyOscarUtils.setDeterministicallyMangledPasswordSecretKeyIntoSession(session, password)StringproviderNo= strAuth[0]ProviderPreferenceproviderPreference=providerPreferenceDao.find(providerNo)if (providerPreference==null) providerPreference=new ProviderPreference();session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE, providerPreference)UserPropertyprop= propDao.getProp(providerNo, UserProperty.PROVIDER_FOR_TICKLER_WARNING)tklerProviderNo=providerNosession.setAttribute("newticklerwarningwindow", providerPreference.getNewTicklerWarningWindow())session.setAttribute("default_pmm", providerPreference.getDefaultCaisiPmm())session.setAttribute("caisiBillingPreferenceNotDelete", String.valueOf(providerPreference.getDefaultDoNotDeleteBilling()))default_pmm=providerPreference.getDefaultCaisiPmm()ArrayList<String>newDocArr= (ArrayList<String>)request.getSession().getServletContext().getAttribute("CaseMgmtUsers")if("enabled".equals(providerPreference.getDefaultNewOscarCme())) {                	newDocArr.add(providerNo);                	session.setAttribute("CaseMgmtUsers", newDocArr);                }session.setAttribute("starthour", providerPreference.getStartHour().toString())session.setAttribute("endhour", providerPreference.getEndHour().toString())session.setAttribute("everymin", providerPreference.getEveryMin().toString())session.setAttribute("groupno", providerPreference.getMyGroupNo())CRHelper.recordLoginSuccess(userName, strAuth[0], request)Stringusername= (String) session.getAttribute("user")Providerprovider= providerManager.getProvider(username)session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER, provider)List<Integer>facilityIds= providerDao.getFacilityIds(provider.getProviderNo())request.getSession().setAttribute("cbiReminderWindow", CBIUtil.getCbiSubmissionFailureWarningMessage(facility.getId(),provider.getProviderNo() ))ProviderDao.addProviderToFacility(providerNo, first_id)UserPropertyprop= propDao.getProp(provider.getProviderNo(), UserProperty.COBALT)cl.updateLoginList(ip, userName)StringnewURL= mapping.findForward("error").getPath()newURL=newURL+"?errormsg=Your account is expired. Please contact your administrator."return(new ActionForward(newURL));cl.updateLoginList(ip, userName)CRHelper.recordLoginFailure(userName, request)Providerprov= providerDao.getProvider((String)request.getSession().getAttribute("user"))private voidsetUserInfoToSession(HttpServletRequest request,String userName, String password, String pin,String nextPage)throws Exception{    	request.getSession().setAttribute("userName", userName);    	request.getSession().setAttribute("password", encodePassword(password));    	request.getSession().setAttribute("pin", pin);    	request.getSession().setAttribute("nextPage", nextPage);        }private StringerrorHandling(String password, String  newPassword, String  confirmPassword, String  encodedOldPassword, String  oldPassword){	        	String newURL = "";	    if (!encodedOldPassword.equals(password)) {     	   newURL = newURL + "?errormsg=Your old password, does NOT match the password in the system. Please enter your old password.";       	} else if(StringUtils.isEmpty(newPassword)) { 	       newURL = newURL + "?errormsg=Your new password is empty.";   	    } else if (!newPassword.equals(confirmPassword)) {      	   newURL = newURL + "?errormsg=Your new password does NOT match the confirmed password. Please try again.";        	} else if (!Boolean.parseBoolean(OscarProperties.getInstance().getProperty("IGNORE_PASSWORD_REQUIREMENTS")) && newPassword.equals(oldPassword)) {       	   newURL = newURL + "?errormsg=Your new password is the same as your old password. Please choose a new password.";         	} 	    	        	    	    return newURL;     }private StringerrorHandling2(String pin, String  newPin, String  confirmPin, String  oldPin){	        	String newURL = "";	    if (!oldPin.equals(pin)) {     	   newURL = newURL + "?errormsg=Your old PIN, does NOT match the PIN in the system. Please enter your old PIN.";       	} else if(StringUtils.isEmpty(newPin)) {  	       newURL = newURL + "?errormsg=Your new PIN is empty.";    	    } else if (!newPin.equals(confirmPin)) {      	   newURL = newURL + "?errormsg=Your new PIN does NOT match the confirmed PIN. Please try again.";        	} else if (!Boolean.parseBoolean(OscarProperties.getInstance().getProperty("IGNORE_PASSWORD_REQUIREMENTS")) && newPin.equals(oldPin)) {       	   newURL = newURL + "?errormsg=Your new PIN is the same as your old PIN. Please choose a new PIN.";         	}     	    	    return newURL;     }private StringencodePassword(String password)throws Exception{    	MessageDigest md = MessageDigest.getInstance("SHA");    	    	StringBuilder sbTemp = new StringBuilder();	    byte[] btNewPasswd= md.digest(password.getBytes());	    for(int i=0; i<btNewPasswd.length; i++) sbTemp = sbTemp.append(btNewPasswd[i]);		    return sbTemp.toString();	        }private SecuritygetSecurity(LoggedInInfo loggedInInfo, String username){		List<Security> results = securityManager.findByUserName(loggedInInfo, username);		Security security = null;		if (results.size() > 0) security = results.get(0);		if (security == null) {			return null;		} else if (OscarProperties.isLdapAuthenticationEnabled()) {			security = new LdapSecurity(security);		}				return security;    }private voidpersistNewPasswordAndPin(LoggedInInfo loggedInInfo, String userName, String newPassword, String newPin)throws Exception{    	    Security security = getSecurity(loggedInInfo, userName);	    security.setPassword(PasswordHash.createHash(newPassword));	    security.setForcePasswordReset(Boolean.FALSE);	    security.setStorageVersion(Security.STORAGE_VERSION_2);	    	    if(!StringUtils.isEmpty(newPin)) {	    	security.setPin(PasswordHash.createHash(newPin));	    }	    	    securityManager.updateSecurityRecord(loggedInInfo, security); 		    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/ProblemCheckFilter.java">
{public HttpSession getSession()		{			HttpSession parentSession = super.getSession();			if (parentSession == null)				return null;			return new SessionChecker(super.getSession());		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstanceTmpSave.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/User.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/ON/util/EDTFolder.java">
{public String getPath() { return path; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/EaapsServiceClient.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxDoctorPreferences.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstance.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyLogin.java">
{public String getPassword() {    	return password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/StudentInfo.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/login/LoginForm.java">
{public String getPassword() {    return password;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/controller/ERxCommunicator.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public String getPassword() {        return this.password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Security.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/login/jaas/OscarCallbackHandler.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/ParameterActionForward.java">
{public String getPath() {        StringBuilder sb = new StringBuilder();        sb.append(path);        boolean firstTimeThrough = true;        if (parameters != null && !parameters.isEmpty()) {            sb.append(questionMark);            Iterator it = parameters.keySet().iterator();            while (it.hasNext()) {                String paramName = (String)it.next();                String paramValue = (String)parameters.get(paramName);                if (firstTimeThrough) {                    firstTimeThrough = false;                } else {                    sb.append(ampersand);                }                sb.append(paramName);                sb.append(equals);                sb.append(paramValue);            }        }        return sb.toString();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/dashboard/model/MetricOwner.java">
{public String getUsername() {		return username;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/SshHandler.java">
{public Session getSession() {		return session;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PageMonitor.java">
{public String getSession() {    	return session;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{public HttpSession getSession() {		return (session);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FaxJob.java">
{public String getPassword() {        return password;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementNote.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementViewFormBean.java">
{public String getPassword() {		return password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/request/SetPatientImmediate3.java">
{public String getPassword() {		return this.password;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecurityArchive.java">
{public String getPassword() {		return password;	}}
