public SshHandler(String userName, String password, String connectionIP, String knownHostsFileName, int port, int timeout)throws Exception{		OscarProperties props = OscarProperties.getInstance();		if (userName == null) {			userName = props.getProperty("eaaps.user");		}		if (password == null) {			password = props.getProperty("eaaps.pass");		}		if (connectionIP == null) {			connectionIP = props.getProperty("eaaps.host");		}		if (knownHostsFileName == null) {			knownHostsFileName = props.getProperty("eaaps.knownHosts");		}		try {			jsch.setKnownHosts(knownHostsFileName);		} catch (JSchException e) {			throw new Exception("Unable to set known hosts", e);		}		setUserName(userName);		setPassword(password);		setHost(connectionIP);		setPort(port);		setTimeOut(timeout);	}public SshHandler(String userName, String password, String connectionIP)throws Exception{		this(userName, password, connectionIP, "", 22, 60000);	}public voidsetPassword(String password){		this.password = password;	}public voidsetTimeOut(int timeOut){		this.timeOut = timeOut;	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public static OscarProperties getInstance() {		return oscarProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}
