StringproviderNo= loggedInInfo.getLoggedInProviderNo()caseManagementMgr.saveCPP(cpp, providerNo)StringproviderNo= loggedInInfo.getLoggedInProviderNo()caseManagementMgr.saveCPP(cpp, providerNo)StringproviderNo= loggedInInfo.getLoggedInProviderNo()Stringroles= (String) se.getAttribute("userrole")if (OscarProperties.getInstance().isOscarLearning() && roles != null && roles.indexOf("moderator") != -1) {			logger.info("skipping domain check..provider is a moderator");		} else if (!caseManagementMgr.isClientInProgramDomain(providerNo, demoNo) && !caseManagementMgr.isClientReferredInProgramDomain(providerNo, demoNo)) {			return mapping.findForward("domain-error");		}Stringroles= (String) se.getAttribute("userrole")if (OscarProperties.getInstance().isOscarLearning() && roles != null && roles.indexOf("moderator") != -1) {			logger.info("skipping domain check..provider is a moderator");		} else if (!caseManagementMgr.isClientInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo) && !caseManagementMgr.isClientReferredInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo)) {			return mapping.findForward("domain-error");		}this.writeAccess=writeAccesspublic voidsetCertain(String certain){			this.certain = certain;		}public voidsetRole(String role){			this.role = role;		}StringproviderNo= loggedInInfo.getLoggedInProviderNo()addLocalIssues(providerNo, checkBoxBeanList, demographicNo, hideInactiveIssues, Integer.valueOf(programId))UserPropertyuserProp= caseManagementMgr.getUserProperty(providerNo, UserProperty.STALE_NOTEDATE)UserPropertyuserProp2= caseManagementMgr.getUserProperty(providerNo, UserProperty.STALE_FORMAT)List<Secrole>roles= roleMgr.getRoles()request.setAttribute("roles", roles)String[]roleId= caseForm.getFilter_roles()notesToDisplay=applyRoleFilter(notesToDisplay, roleId)HashSet<LabelValueBean>providers= new HashSet<LabelValueBean>()providers.add(new LabelValueBean(tempProvider, tempProvider))request.setAttribute("providers", providers)String[]roleId= caseForm.getFilter_roles()if (roleId != null && roleId.length > 0) notes = applyRoleFilter(notes, roleId);StringroleName= (String) request.getSession().getAttribute("userrole") + "," + (String) request.getSession().getAttribute("user")ArrayList<HashMap<String, ? extends Object>>eForms= EFormUtil.listPatientEForms(loggedInInfo, EFormUtil.DATE, EFormUtil.CURRENT, demoNo, roleName)StringproviderNo= loggedInInfo.getLoggedInProviderNo()UserPropertyuserProp= caseManagementMgr.getUserProperty(providerNo, UserProperty.STALE_NOTEDATE)UserPropertyuserProp2= caseManagementMgr.getUserProperty(providerNo, UserProperty.STALE_FORMAT)addLocalIssues(providerNo, checkBoxBeanList, demographicId, false, Integer.valueOf(programId))Set<Provider>providers= new HashSet<Provider>(caseManagementMgr.getAllEditors(demoNo))request.setAttribute("providers", providers)List<Secrole>roles= roleMgr.getRoles()request.setAttribute("roles", roles)private List<CaseManagementNote>applyRoleFilter(List<CaseManagementNote> notes, String[] roleId){		// if no filter return everything		if (Arrays.binarySearch(roleId, "a") >= 0) return notes;		List<CaseManagementNote> filteredNotes = new ArrayList<CaseManagementNote>();		for (Iterator<CaseManagementNote> iter = notes.listIterator(); iter.hasNext();) {			CaseManagementNote note = iter.next();			if (Arrays.binarySearch(roleId, note.getReporter_caisi_role()) >= 0) filteredNotes.add(note);		}		return filteredNotes;	}private List<CaseManagementNote>applyProviderFilters(List<CaseManagementNote> notes, String[] providerNo){		boolean filter = false;		List<CaseManagementNote> filteredNotes = new ArrayList<CaseManagementNote>();		if (providerNo != null && Arrays.binarySearch(providerNo, "a") < 0) {			filter = true;		}		for (Iterator<CaseManagementNote> iter = notes.iterator(); iter.hasNext();) {			CaseManagementNote note = iter.next();			if (!filter) {				// no filter, add all				filteredNotes.add(note);			} else {				if (Arrays.binarySearch(providerNo, note.getProviderNo()) >= 0)				// correct provider				    filteredNotes.add(note);			}		}		return filteredNotes;	}private static booleanhasRole(List<SecUserRole> roles, String role){		if (roles == null) return (false);		logger.debug("Note Role : " + role);		for (SecUserRole roleTmp : roles) {			logger.debug("Provider Roles : " + roleTmp.getRoleName());			if (roleTmp.getRoleName().equals(role)) return (true);		}		return (false);	}List<SecUserRole>roles= secUserRoleDao.getUserRoles(loggedInInfo.getLoggedInProviderNo())StringroleName= this.roleMgr.getRole(note.getReporter_caisi_role()).getRoleName()if (hasRole(roles, roleName)) {					String originaldemo = note.getDemographic_no();					note.setDemographic_no(String.valueOf(demographicNo));					NoteDisplayLocal disp = new NoteDisplayLocal(loggedInInfo, note);					disp.setReadOnly(true);					disp.setGroupNote(true);					Demographic origDemographic = demographicDao.getDemographic(originaldemo);					disp.setLocation(String.valueOf(origDemographic.getDemographicNo()));					notesToDisplay.add(disp);				}protected voidaddLocalIssues(String providerNo, ArrayList<CheckBoxBean> checkBoxBeanList, Integer demographicNo, boolean hideInactiveIssues, Integer programId){		List<CaseManagementIssue> localIssues = caseManagementManager.getIssues(demographicNo, hideInactiveIssues ? false : null);		for (CaseManagementIssue cmi : localIssues) {			CheckBoxBean checkBoxBean = new CheckBoxBean();			checkBoxBean.setIssue(cmi);			IssueDisplay issueDisplay = getIssueDisplay(providerNo, programId, cmi);			checkBoxBean.setIssueDisplay(issueDisplay);			checkBoxBean.setUsed(caseManagementNoteDao.haveIssue(cmi.getIssue().getCode(), demographicNo));			checkBoxBeanList.add(checkBoxBean);		}	}protected IssueDisplaygetIssueDisplay(String providerNo, Integer programId, CaseManagementIssue cmi){		IssueDisplay issueDisplay = new IssueDisplay();		if (programId != null) issueDisplay.writeAccess = cmi.isWriteAccess(providerNo, programId);		issueDisplay.acute = cmi.isAcute() ? "acute" : "chronic";		issueDisplay.certain = cmi.isCertain() ? "certain" : "uncertain";		long issueId = cmi.getIssue_id();		Issue issue = issueDao.getIssue(issueId);		issueDisplay.code = issue.getCode();		issueDisplay.codeType = OscarProperties.getInstance().getProperty("COMMUNITY_ISSUE_CODETYPE").toUpperCase();		issueDisplay.description = issue.getDescription();		issueDisplay.location = "local";		issueDisplay.major = cmi.isMajor() ? "major" : "not major";		issueDisplay.priority = issue.getPriority();		issueDisplay.resolved = cmi.isResolved() ? "resolved" : "unresolved";		issueDisplay.role = issue.getRole();		issueDisplay.sortOrderId = issue.getSortOrderId();		return issueDisplay;	}StringproviderNo= getProviderNo(request)StringroleName= (String) request.getSession().getAttribute("userrole") + "," + (String) request.getSession().getAttribute("user")a=hasPrivilege("_newCasemgmt.otherMeds", roleName)a=hasPrivilege("_newCasemgmt.riskFactors", roleName)a=hasPrivilege("_newCasemgmt.familyHistory", roleName)a=hasPrivilege("_newCasemgmt.medicalHistory", roleName)List<Issue>issues= caseManagementMgr.getIssueInfoByCode(providerNo, codes)StringaddUrl= request.getContextPath() + "/CaseManagementEntry.do?method=issueNoteSave&providerNo=" + providerNo + "&demographicNo=" + demoNo + "&appointmentNo=" + appointmentNo + "&noteId="notes=caseManagementMgr.filterNotes(loggedInInfo, providerNo, notes, programId)StringproviderNo= loggedInInfo.getLoggedInProviderNo()List<CaseManagementNote>filteredResults= caseManagementMgr.filterNotes(loggedInInfo, providerNo, filtered1, programId)private ArrayList<NoteDisplay>applyRoleFilter(ArrayList<NoteDisplay> notes, String[] roleId){		if (roleId == null || hasRole(roleId, "a")) return (notes);		ArrayList<NoteDisplay> filteredNotes = new ArrayList<NoteDisplay>();		for (NoteDisplay note : notes) {			if (hasRole(roleId, note.getRoleName())) filteredNotes.add(note);		}		return filteredNotes;	}private static booleanhasRole(String[] roleId, String role){		for (String s : roleId) {			if (s.equals(role)) return (true);		}		return (false);	}private ArrayList<NoteDisplay>applyProviderFilter(ArrayList<NoteDisplay> notes, String[] providerName){		ArrayList<NoteDisplay> filteredNotes = new ArrayList<NoteDisplay>();		// no list, or empty list, or list of no providers		if (providerName == null || providerName.length == 0 || providerName[0].length() == 0) return (notes);		for (NoteDisplay note : notes) {			String tempName = note.getProviderName();			for (String temp : providerName) {				if (tempName.equals(temp)) filteredNotes.add(note);			}		}		return filteredNotes;	}protected Map<String, Issue>getCPPIssues(HttpServletRequest request, String providerNo){		@SuppressWarnings("unchecked")		Map<String, Issue> issues = (HashMap<String, Issue>) request.getSession().getAttribute("CPPIssues");		if (issues == null) {			String[] issueCodes = { "SocHistory", "MedHistory", "Concerns", "Reminders", "FamHistory", "RiskFactors" };			issues = new HashMap<String, Issue>();			for (String issue : issueCodes) {				List<Issue> i = caseManagementMgr.getIssueInfoByCode(providerNo, issue);				issues.put(issue, i.get(0));			}			request.getSession().setAttribute("CPPIssues", issues);		}		return issues;	}public booleanhasPrivilege(String objectName, String roleName){		Vector v = OscarRoleObjectPrivilege.getPrivilegeProp(objectName);		return OscarRoleObjectPrivilege.checkPrivilege(roleName, (Properties) v.get(0), (Vector) v.get(1));	}Stringroles= (String) se.getAttribute("userrole")if (OscarProperties.getInstance().isOscarLearning() && roles != null && roles.indexOf("moderator") != -1) {			logger.info("skipping domain check..provider is a moderator");		} else if (!caseManagementMgr.isClientInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo) && !caseManagementMgr.isClientReferredInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo)) {			return mapping.findForward("domain-error");		}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/PersonaRightsResponse.java">
{public List<UserRoleTo1> getRoles() {		return roles;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public boolean isOscarLearning() {		return isPropertyActive("OSCAR_LEARNING");	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/service/NoteSelectionCriteria.java">
{public List<String> getRoles() {		return roles;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/SshHandler.java">
{public Session getSession() {		return session;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementViewFormBean.java">
{public String[] getFilter_roles() {		return filter_roles;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/web/CourseManagerAction.java">
{public static List<SecRole> getRoles()  {		SecRoleDao roleDao = (SecRoleDao)SpringUtils.getBean("secRoleDao");		List<SecRole> roles = roleDao.findAll();		return roles;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/dao/security/SecroleDao.java">
{public List<Secrole> getRoles() {        @SuppressWarnings("unchecked")        List<Secrole> results = this.getHibernateTemplate().find("from Secrole r order by roleName");        logger.debug("getRoles: # of results=" + results.size());        return results;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/ProblemCheckFilter.java">
{public HttpSession getSession()		{			HttpSession parentSession = super.getSession();			if (parentSession == null)				return null;			return new SessionChecker(super.getSession());		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/service/security/RolesManager.java">
{public List<Secrole> getRoles() {		return secroleDao.getRoles();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/dao/SecRoleDao.java">
{public List<SecRole> getRoles() {        @SuppressWarnings("unchecked")        Query q = entityManager.createQuery("select s from SecRole s order by s.name");        return q.getResultList();    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{public String getLoggedInProviderNo()	{		Provider p = getLoggedInProvider();		if( p != null ) {			return(p.getProviderNo());		}				return null;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PageMonitor.java">
{public String getSession() {    	return session;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}
