public booleanhasWriteAccess(String objectName, String roleNames, boolean required){    	boolean result=false;    	    	SecobjprivilegeDao secobjprivilegeDao = (SecobjprivilegeDao)SpringUtils.getBean("secobjprivilegeDao");            	List<String> rl = new ArrayList<String>();        for(String tmp:roleNames.split(",")) {        	rl.add(tmp);        }        List<Secobjprivilege> priv = secobjprivilegeDao.getByObjectNameAndRoles(objectName,rl);               if(!required && priv.size()==0) {        	return true;        }        if(required && priv.size()==0) {        	return false;        }        for(Secobjprivilege p:priv) {			if(p.getPrivilege_code().indexOf("w")!= -1)				result=true;			if(p.getPrivilege_code().indexOf("x")!= -1)				result=true;		}    	return result;    }
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public int size() {                return references.size();            }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/Secobjprivilege.java">
{public String getPrivilege_code() {		return privilege_code;	}}
