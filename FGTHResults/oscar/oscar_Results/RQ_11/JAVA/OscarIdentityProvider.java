public booleanisUserInRole(String roleName)if (roleName == null || roleName.length() == 0) {            throw new IllegalArgumentException("Invalid role name specified.");        }booleanisInRole= falseList<SecUserRole>roles= secUserRoleDao.getUserRoles(providerId)for (SecUserRole userRole : roles) {                        // Check if it is the role we are looking for.            if (userRole.getRoleName().equals(roleName)) {                isInRole = true;                break;            }        }return isInRole;
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/Appender.java">
{public int length() {		return getBuffer().length();	}}
