Stringroles= (String) se.getAttribute("userrole")if (OscarProperties.getInstance().isOscarLearning() && roles != null && roles.indexOf("moderator") != -1) {			logger.info("skipping domain check..provider is a moderator");		} else if (!caseManagementMgr.isClientInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo) && !caseManagementMgr.isClientReferredInProgramDomain(loggedInInfo.getLoggedInProviderNo(), demoNo)) {			logger.error("A domain error needs to be added to the returned result, remove this when fixed");			return returnResult;		}StringproviderNo=loggedInInfo.getLoggedInProvider().getProviderNo()StringprogramId= getProgram(loggedInInfo,providerNo)caseManagementMgr.deleteTmpSave(providerNo, ""+demographicNo, programId)caseManagementMgr.tmpSave(providerNo, ""+demographicNo, programId, noteId, noteStr)private StringgetProgram(LoggedInInfo loggedInInfo,String providerNo){		ProgramProvider pp = programManager2.getCurrentProgramInDomain(loggedInInfo,providerNo);		String programId = null;				if(pp !=null && pp.getProgramId() != null){			programId = ""+pp.getProgramId();		}else{			programId = String.valueOf(programMgr.getProgramIdByProgramName("OSCAR")); //Default to the oscar program if provider hasn't been assigned to a program		}		return programId;	}StringproviderNo=loggedInInfo.getLoggedInProviderNo()Providerprovider= loggedInInfo.getLoggedInProvider()StringuserName= provider != null ? provider.getFullName() : ""caseMangementNote.setProvider(provider)caseMangementNote.setProviderNo(providerNo)caseMangementNote.setSigning_provider_no(providerNo)caseMangementNote.setProviderNo(providerNo)if (provider != null) caseMangementNote.setProvider(provider);StringprogramIdString= getProgram(loggedInInfo,providerNo)//might not to convert it.StringproviderNo=loggedInInfo.getLoggedInProviderNo()Providerprovider= loggedInInfo.getLoggedInProvider()StringuserName= provider != null ? provider.getFullName() : ""StringprogramId= getProgram(loggedInInfo,providerNo)Stringrole= nullrole=String.valueOf((programManager.getProgramProvider(providerNo, programId)).getRole().getId())role="0"caseMangementNote.setReporter_caisi_role(role)caseMangementNote.setSigning_provider_no(providerNo)caseMangementNote.setProviderNo(providerNo)if (provider != null) caseMangementNote.setProvider(provider);List<CaseManagementNote>curCPPNotes2= new ArrayList<CaseManagementNote>()curCPPNotes2.add(cn)curCPPNotes2.add(note.getPosition()-1,xn)for(int x=0;x<curCPPNotes2.size();x++) {				if(curCPPNotes2.get(x).getId() != -1L) {					//update the note					curCPPNotes2.get(x).setPosition(positionToAssign);					caseManagementMgr.updateNote(curCPPNotes2.get(x));				} 				if(curCPPNotes2.get(x).getId() != -1L && curCPPNotes2.get(x).getUuid().equals(note.getUuid())) {					curCPPNotes2.get(x).setPosition(0);					caseManagementMgr.updateNote(curCPPNotes2.get(x));					positionToAssign--;				}				positionToAssign++;			}StringsavedStr= caseManagementMgr.saveNote(cpp, caseMangementNote, providerNo, userName, null, note.getRoleName())caseManagementMgr.saveCPP(cpp, providerNo)StringproviderNo=loggedInInfo.getLoggedInProviderNo()StringprogramIdString= getProgram(loggedInInfo,providerNo)CaseManagementTmpSavetmpsavenote= this.caseManagementMgr.restoreTmpSave(providerNo, ""+demographicNo, programIdString)note.setProviderNo(providerNo)prov.setProviderNo(providerNo)note.setProviderNo(providerNo)prov.setProviderNo(providerNo)if ((note = caseManagementMgr.getLastSaved(""+programId, ""+demographicNo, providerNo,unlockedNotesMap)) == null) {//				session.setAttribute("newNote", "true");//				//session.setAttribute("issueStatusChanged", "false");							//String encType 				String apptDate = getString(jsonobject,"apptDate");				String reason = getString(jsonobject,"reason");				String appointmentNo = getString(jsonobject,"appointmentNo");				note = caseManagementMgr.makeNewNote(providerNo, ""+demographicNo, encType, appointmentNo,loggedInInfo.getLocale());				//note = caseManagementMgr.makeNewNote(providerNo, ""+demographicNo, bean, encType, apptDate, reason,loggedInInfo.locale);//				note = this.makeNewNote(providerNo, demono, request);							}public GenericRESTResponsesetEditingNoteFlag(@QueryParam("noteUUID") String noteUUID, @QueryParam("userId") String providerNo){		GenericRESTResponse resp = new GenericRESTResponse(false, "Parameter error");		if (noteUUID==null || noteUUID.trim().isEmpty() || providerNo==null || providerNo.trim().isEmpty()) return resp;				ConcurrentHashMap<String, Long> noteList = editList.get(noteUUID);		if (noteList==null) {			noteList = new ConcurrentHashMap<String, Long>();			editList.put(noteUUID, noteList);		}		clearDanglingFlags();				resp.setSuccess(true);		resp.setMessage(null);				if (!noteList.containsKey(providerNo)) { // only check for other editing user when initializing flag			for (String key : noteList.keySet()) {				if (key!=providerNo) {					resp.setSuccess(false);					break;				}			}		}		noteList.put(providerNo, new Date().getTime());		editList.put(noteUUID, noteList);		return resp;	}String[]providerNos= noteList.keySet().toArray(new String[noteList.keySet().size()])for (String providerNo : providerNos) {				Long editTime = noteList.get(providerNo);				if (now-editTime>=360000) noteList.remove(providerNo); //remove flag due 6 min (should be renewed/removed within 5 min)			}public GenericRESTResponsecheckEditNoteNew(@QueryParam("noteUUID") String noteUUID, @QueryParam("userId") String providerNo){		GenericRESTResponse resp = new GenericRESTResponse(true, null);		if (noteUUID==null || noteUUID.trim().isEmpty() || providerNo==null || providerNo.trim().isEmpty()) return resp;				ConcurrentHashMap<String, Long> noteList = editList.get(noteUUID);		if (noteList==null) return resp;		if (noteList.size()==1 && noteList.containsKey(providerNo)) return resp;				long myEditTime = 0;		if (noteList.containsKey(providerNo)) myEditTime = noteList.get(providerNo);		for (String key : noteList.keySet()) {			if (key!=providerNo) {				if (noteList.get(key)>myEditTime) {					resp.setSuccess(false);					break;				}			}		}		return resp;  //true = no new edit, false = warn about new edit	}public voidremoveEditingNoteFlag(@QueryParam("noteUUID") String noteUUID, @QueryParam("userId") String providerNo){		if (noteUUID==null || noteUUID.trim().isEmpty() || providerNo==null || providerNo.trim().isEmpty()) return;				ConcurrentHashMap<String, Long> noteList = editList.get(noteUUID);		if (noteList!=null && noteList.containsKey(providerNo)) noteList.remove(providerNo);		if (noteList.isEmpty()) editList.remove(noteUUID);		else editList.put(noteUUID, noteList);	}
--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMessenger/pageUtil/MsgDisplayMessagesBean.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ControllerHandler.java">
{public static ControllerHandler getInstance() {		if(instance == null) {			instance = new ControllerHandler();		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/Intake.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/FormInfo.java">
{public Long getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/prop/EctFormProp.java">
{public static EctFormProp getInstance() {                   return fProp;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Encounter.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MessageList.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/FavoriteTo1.java">
{public Integer getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxProviderData.java">
{public String getProviderNo(){            return this.providerNo;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ORNPreImplementationReportLog.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/PreventionTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecurityToken.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/data/EFormBase.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/DemographicNameAgeString.java">
{public static DemographicNameAgeString getInstance() {      return demographicNameAgeString;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/tld/LabTag.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Prescribe.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicExtArchive.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetDrug.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicStudy.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/consultations/ConsultationData.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/inbox/InboxManagerQuery.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ConsultationResponseTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/UserDSMessagePrefs.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/AdmissionSearchBean.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSurveillance/SurveillanceMaster.java">
{public static SurveillanceMaster getInstance() {      if (!isLoaded()){         initSurvey();      }      return surveillanceMaster;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RecycleBin.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/ScheduleTemplateBean.java">
{public String getProviderNo() {     return (providerNo);  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/ProgramClientRestriction.java">
{public int getProgramId() {        return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OcanClientForm.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/research/eaaps/ExportEntry.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/hospitalReportManager/model/HRMDocumentToProvider.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CasemgmtNoteLock.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BedCheckTime.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Document.java">
{public Integer getProgramId() {        return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/pageUtil/WCBForm.java">
{public String getProviderNo() {    return providerNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IncomingLabRules.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicSearchResult.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/EFormReportToolTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/bean/WLWaitingListNameBean.java">
{public String getProviderNo() {			return providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/AbstractIntakeExporter.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/TicklerTo1.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AdmissionTo1.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMessenger/tld/MsgNewMessagesTag.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/eform/EFormLoader.java">
{static public EFormLoader getInstance() {        if (_instance == null) {            _instance = new EFormLoader();            parseXML();            MiscUtils.getLogger().debug("NumElements ====" + eFormAPs.size());        }        return _instance;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SurveyData.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Drug.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AppointmentTo1.java">
{public int getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxPrescriptionData.java">
{public String getProviderNo() {			return this.providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxPrescriptionData.java">
{public String getProviderNo() {			return this.providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/caisi/core/web/tld/programExclusiveViewTag.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RemoteDataLog.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/appt/ApptData.java">
{public String getProviderNo() {		return provider_no;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/data/BillingFormData.java">
{public String getProviderNo() {			return provider_no;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/ProgramSignature.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Hl7Link.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/ON/model/BillingONFilename.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/ActivityReportFormBean.java">
{public int getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/AutoTickler.java">
{public static AutoTickler getInstance() {      return autoTickler;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/Echart.java">
{public String getProviderNo(){          return this.providerNo;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Admission.java">
{public Integer getProgramId() {    	return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/appointment/web/NextAppointmentSearchBean.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/decisionSupport/model/DSGuidelineProviderMapping.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/data/RxInteractionData.java">
{public static RxInteractionData getInstance() {      return rxInteractionData;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FaxClientLog.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/TicklerComment.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PrintResourceLog.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/model/BillingInr.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/dms/EDoc.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/web/ConsultationReportFormBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PageMonitor.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Prevention.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/formbean/ClientListsReportFormBean.java">
{public String getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/common/PassIntakeFormVars.java">
{public String getProviderNo()	{		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AppointmentArchiveTransfer.java">
{public int getProgramId() {		return (programId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaseManagementTmpSave.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/EncounterWindow.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/AppointmentArchive.java">
{public int getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/exporter/DATISExporterFactory.java">
{public synchronized static DATISExporterFactory getInstance() {		if(factory == null) {			factory = new DATISExporterFactory();		}				return factory;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OcanStaffForm.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DemographicTransfer.java">
{public String getProviderNo() {    	return (providerNo);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/ON/model/BillingONFavourite.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicQueryFavourite.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/GroupMembers.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyLogin.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProviderTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplay.java">
{public String getProviderNo();}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MeasurementsDeleted.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MyGroupPrimaryKey.java">
{public String getProviderNo() {        return this.providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/OscarProperties.java">
{public boolean isOscarLearning() {		return isPropertyActive("OSCAR_LEARNING");	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RoomDemographic.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONPremium.java">
{public String getProviderNo() { return this.providerNo; }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/caisi/tickler/web/TicklerAction.java">
{public String getProviderNo() {			return providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DrugTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Demographic.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/EChart.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OscarAnnotation.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/apache/xml/security/encryption/XMLCipher.java">
{public static XMLCipher getInstance() throws XMLEncryptionException {        if (log.isDebugEnabled()) {            log.debug("Getting XMLCipher with no arguments");        }        return new XMLCipher(null, null, null, null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/EChartNoteEntry.java">
{public int getProgramId() {    	return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingONCHeader1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/SecurityAccess.java">
{public String getProgramId()	{		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/bean/RptByExampleQueryBean.java">
{public String getProviderNo(){           return providerNo;       }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Tickler.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/common/Colour.java">
{public static Colour getInstance() {		Colour c = null;		try {			String colourClass = OscarProperties.getInstance().getProperty("ColourClass", "org.oscarehr.casemgmt.common.Colour");			if(colourClass.length()>0) {				c = (Colour)Class.forName(colourClass).newInstance();			}		}catch(Exception e) {			MiscUtils.getLogger().error("Error",e);		}		if(c == null)			return new Colour();		return c;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/IntegratorConsent.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Security.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicExtTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarResearch/oscarDxResearch/bean/dxResearchBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Form.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FileUploadCheck.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/BillingNotes.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OnCallQuestionnaire.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ticklers/web/TicklerQuery.java">
{public String getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/EFormReportTool.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DemographicTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/ProviderExt.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxSessionBean.java">
{public String getProviderNo() {        return this.providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BedDemographic.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/data/BillingFormData.java">
{public String getProviderNo() {			return provider_no;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/Secobjprivilege.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/caisi/model/DefaultIssue.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayNonNote.java">
{public String getProviderNo() {		if (provider != null) return (provider.getProviderNo());		else return ("");	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ORNCkdScreeningReportLog.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderFacilityPK.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/pageUtil/BillingReProcessBillForm.java">
{public java.lang.String getProviderNo() {      return provider_no;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/service/NoteSelectionCriteria.java">
{public String getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/consultations/ConsultationQuery.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FunctionalCentreAdmission.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ScratchPad.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/PreventionsLotNrs.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecObjPrivilege.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderArchive.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/PreventionTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/pageUtil/EctIncomingConsultationForm.java">
{public String getProviderNo()    {        if(providerNo == null)            providerNo = new String();        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayIntegrator.java">
{public String getProviderNo() {	    return(null);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingClaimHeader1Data.java">
{public String getProviderNo() {		return provider_no;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/QuickListUser.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Prescription.java">
{public String getProviderNo() {    return providerNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderDefaultProgram.java">
{public int getProgramId() {    	return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/OscarAuditLogger.java">
{public static OscarAuditLogger getInstance() {		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementNote.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RemoteIntegratedDataCopy.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecUserRole.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/SuperSiteUtil.java">
{public static SuperSiteUtil getInstance()	{		SuperSiteUtil superSiteUtil = (SuperSiteUtil) SpringUtils.getBean("superSiteUtil");		return superSiteUtil;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarTickler/tld/TicklerTag.java">
{public String getProviderNo()    {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FormBPMH.java">
{public int getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NotePermission.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultDocs.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Property.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/NoteDisplayLocal.java">
{public String getProviderNo() {		return (caseManagementNote.getProviderNo());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportProvider.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DocumentDescriptionTemplate.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/PrescriptionTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConfigImmunization.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Appointment.java">
{public int getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Study.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/TableModification.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderSitePK.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportByExamplesFavorite.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FacilityMessage.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Dxresearch.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/Measurements.java">
{public String getProviderNo() {	return this.providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OscarJob.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/WaitingListNameTo1.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/UserProperty.java">
{public String getProviderNo() {        return this.providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportTemp.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderPreference.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Measurement.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/data/EctProviderData.java">
{public String getProviderNo() {			return providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarWaitingList/WaitingList.java">
{public static WaitingList getInstance() {		return new WaitingList();	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/RosterTermReasonProperties.java">
{public static RosterTermReasonProperties getInstance() {		return rosterTermReasonProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/Secuserrole.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultResponseDoc.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportLetters.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/FunctionalCentreAdmissionDisplay.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DrugReason.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AllergyTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarResearch/oscarDxResearch/pageUtil/dxResearchForm.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ServiceRequestToken.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/BillingDataBean.java">
{public String getProviderNo()   {      return provider_no;   }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/form/model/FormRourke2009.java">
{@Column(name="provider_no")	public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/util/plugin/OscarProperties.java">
{public OscarProperties getInstance() {		return (OscarProperties)properties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportAgeSex.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/WaitingListName.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/eyeform/model/EyeformConsultationReport.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/integration/excelleris/com/colcamex/www/core/ServiceExecuter.java">
{public static ServiceExecuter getInstance() {		if(ServiceExecuter.instance == null) {									instance = new ServiceExecuter();			logger.info("Instantiating Service Executer.");		}		return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/service/myoscar/MeasurementsManager.java">
{public String getProviderNo() {			if (height != null && height.getProviderNo() != null) return (height.getProviderNo());			else if (weight != null && weight.getProviderNo() != null) return (weight.getProviderNo());			else return (null);		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/ProviderService.java">
{@GET    @Path("/provider/me")    @Produces("application/json")    public String getLoggedInProvider() {       Provider provider = getLoggedInInfo().getLoggedInProvider();    	if(provider != null) {    		JsonConfig config = new JsonConfig();        	config.registerJsonBeanProcessor(java.sql.Date.class, new JsDateJsonBeanProcessor());            return JSONObject.fromObject(provider,config).toString();    	}    	return null;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Patient.java">
{public int getProviderNo() {    return providerNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstance.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/UserAccessValue.java">
{public String getProviderNo() {			return providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/pageUtil/BillingPreferencesActionForm.java">
{public String getProviderNo() {    return providerNo;  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/EFormData.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Provider.java">
{public String getProviderNo() {    return (providerNo != null ? providerNo : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ProgramProviderTo1.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/web/PatientDetailBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderBillCenter.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementViewFormBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/SecurityArchive.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/scratch/tld/ScratchTag.java">
{public String getProviderNo()    {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/web/AdmissionForDisplay.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/appointment/web/NextAppointmentSearchResult.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Provider.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/hospitalReportManager/model/HRMDocumentComment.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxApplicationPreferences.java">
{public static ERxApplicationPreferences getInstance() {        // The object we will return        ERxApplicationPreferences answer = new ERxApplicationPreferences();        // The object we will use to get the data        OscarProperties properties = OscarProperties.getInstance();        // Set data in the new object        answer.setERxEnabled(Boolean.parseBoolean(properties                .getProperty("util.erx.enabled")));        answer.setSoftwareName(properties.getProperty("util.erx.software"));        answer.setVendor(properties.getProperty("util.erx.vendor"));        answer.setVersion(properties.getProperty("util.erx.version"));        return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultationResponse.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ConsultationRequestTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/tld/ConsultTag.java">
{public String getProviderNo()    {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/pageUtil/RptDemographicReportForm.java">
{public String[] getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/NewAppointmentTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DemographicArchive.java">
{public String getProviderNo() {		    return this.providerNo;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderStudyPK.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RecycleBinBilling.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CdsClientForm.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/study/types/MyMedsStudy.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/util/LoggedInInfo.java">
{public Provider getLoggedInProvider() {		return (loggedInProvider);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Hl7Link.java">
{public String getProviderNo() {    return (providerNo != null ? providerNo : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/model/CaseManagementCPP.java">
{public String getProviderNo() {            return this.providerNo;        }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/WorkFlow.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/MeasurementTransfer.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Billing.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/on/data/BillingProviderData.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/pageUtil/SpokenLangProperties.java">
{public static SpokenLangProperties getInstance() {		return spokenLangProperties;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarReport/ClinicalReports/ClinicalReportManager.java">
{static public ClinicalReportManager getInstance(){        clinicalReportManager.loadReportsFromFile();        return clinicalReportManager;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Allergy.java">
{public String getProviderNo() {    	return (providerNo);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/AllergyTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/OscarLog.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/TicklerUpdate.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/ProgramProviderTransfer.java">
{public Integer getProgramId() {		return (programId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FavoritesPrivilege.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/LogLetters.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/DocumentTransfer.java">
{public Integer getProgramId() {		return (programId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMessenger/pageUtil/MsgSessionBean.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarDemographic/data/ProvinceNames.java">
{public static ProvinceNames getInstance() {        return pNames;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Desaprisk.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/PrescriptionTo1.java">
{public Integer getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Immunizations.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DesAnnualReviewPlan.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyData.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/data/BillingPreference.java">
{public int getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarRx/pageUtil/RxChoosePatientForm.java">
{public String getProviderNo() {        return (this.providerNo);    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/transfer_objects/AppointmentTransfer.java">
{public int getProgramId() {		return (programId);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/MeasurementTemplateFlowSheetConfig.java">
{static public MeasurementTemplateFlowSheetConfig getInstance() {        if (measurementTemplateFlowSheetConfig.flowsheets == null) {            measurementTemplateFlowSheetConfig.loadFlowsheets();        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarSecurity/SecurityTokenManager.java">
{public static SecurityTokenManager getInstance() {		if(instance != null) {			return instance;		}				String managerName = OscarProperties.getInstance().getProperty("security.token.manager");		if(managerName != null) {			try {				instance = (SecurityTokenManager)Class.forName(managerName).newInstance();			}catch(Exception e) {				MiscUtils.getLogger().error("Unable to load token manager");			}		}				return instance;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ScheduleDate.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ServiceAccessToken.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ConsultationRequest.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/oscarRx/erx/model/ERxFacilityPreferences.java">
{public static ERxFacilityPreferences getInstance()            throws MalformedURLException {        // The object we will return    	    	//logger.info("ERxFacilityPref1");    	        ERxFacilityPreferences answer = new ERxFacilityPreferences();        //logger.info("ERxFacilityPref2");        OscarProperties properties = OscarProperties.getInstance();        //logger.info("ERxFacilityPref3");        // Set data in the new object        answer.setFacilityId(Integer.parseInt(properties                .getProperty("util.erx.clinic_facility_id")));        answer.setRemoteURL(new URL(properties.getProperty("util.erx.webservice_url")));        answer.setUsername(properties.getProperty("util.erx.clinic_username"));        answer.setPassword(properties.getProperty("util.erx.clinic_password"));        answer.setClientNumber(properties.getProperty("util.erx.clinic_facility_id"));        answer.setIsTraining(Boolean.parseBoolean(properties.getProperty("util.erx.clinic_training_mode")));        answer.setLocale(properties.getProperty("util.erx.clinic_locale"));                return answer;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/StudyDetails.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/NoteTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/model/ClientReferral.java">
{public Long getProgramId() {        return _programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Favorites.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarLab/ca/all/upload/PatientLabRoutingResult.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ScheduleTemplatePrimaryKey.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/learning/web/CourseDetailBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarConsultationRequest/pageUtil/EctConsultationFormRequestForm.java">
{public String getProviderNo() {		return (StringUtils.trimToEmpty(providerNo));	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarProvider/data/ProviderData.java">
{public java.lang.String getProviderNo() {		return provider_no;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/data/BillingNote.java">
{public java.lang.String getProviderNo() {			return provider_no;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/TicklerCommentTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/AbstractWs.java">
{protected Provider getLoggedInProvider()	{		LoggedInInfo loggedInInfo=getLoggedInInfo();		return(loggedInInfo.getLoggedInProvider());	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CaisiFormInstanceTmpSave.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/inbox/InboxManagerResponse.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetCustomization.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ReportByExamples.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/Immunizations.java">
{public String getProviderNo() {    return (providerNo != null ? providerNo : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Prescription.java">
{public String getProviderNo() {		return (providerNo);	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/TicklerUpdateTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/com/quatro/model/security/SecProvider.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/casemgmt/web/formbeans/CaseManagementEntryFormBean.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/CustomFilter.java">
{public String getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/RSchedule.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMessenger/tld/MsgNewMessageTag.java">
{public String getProviderNo()    {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/pageUtil/BillingReProcessBillForm.java">
{public java.lang.String getProviderNo() {        return provider_no;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarEncounter/oscarMeasurements/data/MeasurementTypes.java">
{public static synchronized MeasurementTypes getInstance() {		if (measurementTypes == null) {			measurementTypes = new MeasurementTypes();			measurementTypes.reInit();		}		return measurementTypes;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/decisionSupport/BillingGuidelines.java">
{static public BillingGuidelines getInstance() {        String tmpRegion = OscarProperties.getInstance().getProperty("billregion","");        if (measurementTemplateFlowSheetConfig.billingGuideLines == null || !tmpRegion.equals(region)) {                region = tmpRegion;                measurementTemplateFlowSheetConfig.loadGuidelines(region);        }        return measurementTemplateFlowSheetConfig;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProgramEncounterTypePK.java">
{public int getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Favorite.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/AppUser.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/billing/CA/BC/model/Wcb.java">
{public int getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderLabRoutingModel.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/Teleplan/WCBCodes.java">
{public static WCBCodes getInstance() {        return wcbCodes;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/BillingOnTransaction.java">
{public String getProviderNo() {		return this.providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/entities/EChart.java">
{public String getProviderNo() {    return (providerNo != null ? providerNo : "");  }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DrugDispensing.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/DigitalSignature.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/JointAdmission.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/PMmodule/wlmatch/VacancyDisplayBO.java">
{public Integer getProgramId() {    	return programId;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/MSP/MSPBillingNote.java">
{public java.lang.String getProviderNo() {			return provider_no;		}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/dashboard/handler/IndicatorTemplateXML.java">
{public String getLoggedInProvider() {		String providerNo = "";				if( this.loggedInInfo != null ) {			providerNo = this.loggedInInfo.getLoggedInProviderNo(); 		}				return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/ProviderTo1.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarBilling/ca/bc/administration/TeleplanCorrectionFormWCB.java">
{public java.lang.String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/DrugTo1.java">
{public Integer getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/ProviderInboxItem.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/oscar/oscarMDS/data/ReportStatus.java">
{public String getProviderNo() {		return providerNo;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/Room.java">
{public Integer getProgramId() {		return programId;	}}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/FlowSheetDx.java">
{public String getProviderNo() {        return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/common/model/MyGroupAccessRestriction.java">
{public String getProviderNo() {    	return providerNo;    }}

--------------------------------
External Method <"/Users/mordreth/Projects/FG-Traceability/oscarFGTH//src/main/java/org/oscarehr/ws/rest/to/model/OscarJobTo1.java">
{public String getProviderNo() {		return providerNo;	}}
